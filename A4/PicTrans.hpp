#pragma once

#include "Image.hpp"

#include <glm/glm.hpp>


class PicTrans{
  glm::dmat4 T1;
  glm::dmat4 S2;
  glm::dmat4 R3;
  glm::dmat4 T4;
  
  double W;
  double H;
  
  void GenT1();
  void GenS2();
  void GenR3();
  void GenT4();
  
public:
  const glm::dvec3 eye;
  const glm::dvec3 view;
  const glm::dvec3 up;  
  const double fovy;
  const double NX;
  const double NY;
  const double Dist;
  
  PicTrans(const glm::dvec3& E,const glm::dvec3& V,const glm::dvec3& U,double FoV,Image& I);
  
  ~PicTrans();
  glm::dmat4 M;
  void GetM();
};
