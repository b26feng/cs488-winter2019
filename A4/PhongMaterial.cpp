// Winter 2019

#include "PhongMaterial.hpp"
#include <iostream>
#include <glm/glm.hpp>
PhongMaterial::PhongMaterial(
	const glm::vec3& kd, const glm::vec3& ks, double shininess )
  : m_kd(glm::dvec3(kd))
  , m_ks(glm::dvec3(ks))
  , m_shininess(shininess)
{
  Type = MatType::Phong;
}

void PhongMaterial:: GetColor(glm::dvec4& EyeRay,glm::dvec4& Origin,glm::dvec4& Ray,glm::dvec4& Normal,glm::dvec3& color,Light* L,bool Hit,double t){
  if(!Hit){
    double Dist = glm::length(Ray);
    double DIF = glm::dot(glm::normalize(Ray),Normal);
    double Fall = L->falloff[0] +L->falloff[1]*Dist+L->falloff[2]*Dist*Dist;
    //DIF = (DIF + (DIF - 0.0f))/2;
    // difuse
    if(DIF > 0){
      color.x += (DIF* m_kd.x  * (L->colour.x))/Fall;
      color.y += (DIF* m_kd.y  * (L->colour.y))/Fall;
      color.z +=(DIF* m_kd.z * (L->colour.z))/Fall;
    }
    // specular
    
    glm::dvec4 SRay = Origin - glm::dvec4(L->position,1.0f);
    SRay = glm::reflect(SRay,-Normal);
    DIF = -glm::dot(glm::normalize(SRay),glm::normalize(EyeRay));
    DIF = glm::pow(DIF,m_shininess);
    //std::cout<<"Len:"<<glm::length(EyeRay)<<"|"<<glm::length(Normal)<<std::endl;
    //DIF = (DIF + (DIF - 0.0f))/2;
    if(DIF>0){
      color.x += (DIF* m_ks.x  * (L->colour.x));
      color.y += (DIF* m_ks.y  * (L->colour.y));
      color.z += (DIF* m_ks.z  * (L->colour.z));
    }
  }
}

PhongMaterial::~PhongMaterial()
{}
