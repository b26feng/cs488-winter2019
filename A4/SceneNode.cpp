// Winter 2019

#include "SceneNode.hpp"

#include "cs488-framework/MathUtils.hpp"

#include <iostream>
#include <sstream>
using namespace std;

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

using namespace glm;


// Static class variable
unsigned int SceneNode::nodeInstanceCount = 0;


//---------------------------------------------------------------------------------------
SceneNode::SceneNode(const std::string& name)
  : m_name(name),
	m_nodeType(NodeType::SceneNode),
	trans(dmat4()),
	invtrans(dmat4()),
	m_nodeId(nodeInstanceCount++)
{
  TMat = glm::dmat4();
  RMat = glm::dmat4();
  TRMat = glm::dmat4();
}

//---------------------------------------------------------------------------------------
// Deep copy
SceneNode::SceneNode(const SceneNode & other)
	: m_nodeType(other.m_nodeType),
	  m_name(other.m_name),
	  trans(other.trans),
	  invtrans(other.invtrans)
{
	for(SceneNode * child : other.children) {
		this->children.push_front(new SceneNode(*child));
	}
}

//---------------------------------------------------------------------------------------
SceneNode::~SceneNode() {
	for(SceneNode * child : children) {
		delete child;
	}
}

//---------------------------------------------------------------------------------------
void SceneNode::set_transform(const glm::dmat4& m) {
	trans = m;
	invtrans = glm::inverse(m);
}

//---------------------------------------------------------------------------------------
const glm::dmat4& SceneNode::get_transform() const {
	return trans;
}

//---------------------------------------------------------------------------------------
const glm::dmat4& SceneNode::get_inverse() const {
	return invtrans;
}

//---------------------------------------------------------------------------------------
void SceneNode::add_child(SceneNode* child) {
	children.push_back(child);
}

//---------------------------------------------------------------------------------------
void SceneNode::remove_child(SceneNode* child) {
	children.remove(child);
}

//---------------------------------------------------------------------------------------
void SceneNode::rotate(char axis, float angle) {
	dvec3 rot_axis;

	switch (axis) {
		case 'x':
			rot_axis = dvec3(1,0,0);
			break;
		case 'y':
			rot_axis = dvec3(0,1,0);
	        break;
		case 'z':
			rot_axis = dvec3(0,0,1);
	        break;
		default:
			break;
	}
	dmat4 rot_matrix = glm::rotate(degreesToRadians((double)angle), rot_axis);
	RMat = rot_matrix;
	set_transform( rot_matrix * trans );
}

//---------------------------------------------------------------------------------------
void SceneNode::scale(const glm::dvec3 & amount) {
  SMat = glm::scale(amount);
  //set_transform( glm::scale(amount) * trans );
  set_transform( SMat * trans );
}

//---------------------------------------------------------------------------------------
void SceneNode::translate(const glm::dvec3& amount) {
	set_transform( glm::translate(amount) * trans );
	TMat = glm::translate(amount);
}


// cout type
void SceneNode::PrintType(){
  std::cout<<m_name<<": No Primitive"<<std::endl;
  for(SceneNode* i:children){
    i->PrintType();
  }
}

size_t SceneNode::Count(){
  size_t C = 1;
  for(SceneNode* i:children){
    C += i->Count();
  }
  return C;
}

void SceneNode::PreProcess(std::vector<SceneNode*>& R,SceneNode* Parent){
  R.emplace_back(this);
  //std::cout<<m_name<<":"<<m_nodeId<<std::endl;
  /*if(Parent!=nullptr){
    //trans =  TMat * RMat *Parent->trans * glm::inverse(RMat) * glm::inverse(TMat) * trans;
    //trans = TMat*Parent->TMat*RMat*Parent->RMat*glm::inverse(Parent->RMat) * glm::inverse(Parent->TMat) * Parent->trans * glm::inverse(RMat) * glm::inverse(TMat) * trans;
    //TRMat = TMat * RMat;
    //TMat *= Parent->TMat;
    //RMat *= Parent->RMat;
    //TRMat *= Parent->TRMat;
    //trans = Parent->trans * trans;
    //invtrans = glm::inverse(trans);
    }*/
  for(SceneNode* i:children){
    i->PreProcess(R,this);
  }
}

void SceneNode::GetMaterial(glm::dvec4& EYE,glm::dvec4& Ray,glm::dvec4& Normal,bool* init,double* t,SceneNode** Hit){
  glm::dvec4 MyEye = invtrans * EYE;
  glm::dvec4 MyRay = invtrans * Ray;
  SceneNode* First = *Hit;
  for(SceneNode* i:children){
    i->GetMaterial(MyEye,MyRay,Normal,init,t,Hit);
  }
  if(*Hit != First){
    Normal = trans * Normal;
  }
}

//---------------------------------------------------------------------------------------
int SceneNode::totalSceneNodes() const {
	return nodeInstanceCount;
}

//---------------------------------------------------------------------------------------
std::ostream & operator << (std::ostream & os, const SceneNode & node) {

	//os << "SceneNode:[NodeType: ___, name: ____, id: ____, isSelected: ____, transform: ____"
	switch (node.m_nodeType) {
		case NodeType::SceneNode:
			os << "SceneNode";
			break;
		case NodeType::GeometryNode:
			os << "GeometryNode";
			break;
		case NodeType::JointNode:
			os << "JointNode";
			break;
	}
	os << ":[";

	os << "name:" << node.m_name << ", ";
	os << "id:" << node.m_nodeId;

	os << "]\n";
	return os;
}
