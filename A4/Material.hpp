// Winter 2019

#pragma once
#include "Light.hpp"

enum class MatType{
  None,
  Phong,
    };

class Material {
public:
  virtual ~Material();
  virtual void GetColor(glm::dvec4& EyeRay,glm::dvec4& Origin,glm::dvec4& Ray,glm::dvec4& Normal,glm::dvec3& color,Light* L,bool Hit,double t);
  MatType Type;
protected:
  Material();
};
