// Winter 2019

#pragma once

#include <glm/glm.hpp>

#include "Material.hpp"

class PhongMaterial : public Material {
public:
  PhongMaterial(const glm::vec3& kd, const glm::vec3& ks, double shininess);
  virtual ~PhongMaterial();
  virtual void GetColor(glm::dvec4& EyeRay,glm::dvec4& Origin,glm::dvec4& Ray,glm::dvec4& Normal,glm::dvec3& color,Light* L,bool Hit,double t) override;
    glm::dvec3 m_kd;
  glm::dvec3 m_ks;

  double m_shininess;
private:

};
