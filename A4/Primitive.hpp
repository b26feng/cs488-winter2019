// Fall 2018

#pragma once

#include <glm/glm.hpp>

enum class PType{
  Primitive,
    Sphere,
    Cube,
    NSphere,
    NBox
    };
  

class Primitive {
protected:
  glm::dmat4 TMat;
  glm::dmat4 RMat;
  glm::dmat4 SMat;
  glm::dmat4 Trans;
  glm::dmat4 Inv;
  glm::dmat4 InvT;
public:
  PType Type;
  Primitive();
  virtual ~Primitive();
  virtual void Print() const;

 
  // return the t for intersection
  virtual double GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal);

  virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans);
};



class NonhierSphere : public Primitive {
public:
  NonhierSphere(const glm::dvec3& pos, double radius);
  virtual ~NonhierSphere();
  virtual void Print() const override;
  // return the t for intersection
  virtual double GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal) override;
  //virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
protected:
  glm::dvec3 m_pos;
  double m_radius;
};

class NonhierBox : public Primitive {
public:
  NonhierBox(const glm::dvec3& pos, double size);
  virtual ~NonhierBox();
  virtual void Print() const override;
  virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
  virtual double GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal) override;
protected:
  glm::dvec3 m_pos;
  glm::dvec4* Vertices;
  glm::dvec4* Normals;
  double m_size;
};

class Sphere : public NonhierSphere {
public:
  Sphere();
};

class Cube : public NonhierBox {
public:
  Cube();
};
