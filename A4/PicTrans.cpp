#include "PicTrans.hpp"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

PicTrans::PicTrans(const glm::dvec3& E,const glm::dvec3& V,const glm::dvec3& U,double FoV,Image& I):
  eye{E},
  view{V},
  up{U},
  fovy{FoV},
  NX{(double)(I.width())},
  NY{(double)(I.height())},
  Dist{1.0f}
{
  T1 = glm::dmat4();
  S2 =  glm::dmat4();
  R3 =  glm::dmat4();
  T4 =  glm::dmat4();
  M =  glm::dmat4();
}

void PicTrans::GenT1(){
  T1 = glm::translate(glm::dmat4(),glm::dvec3(-NX/2,-NY/2,Dist));
}

void PicTrans::GenS2(){
  double Radians = glm::radians(fovy);
  H = 2*Dist*glm::tan(Radians/2);
  W = NX*H/NY;
  S2 = glm::scale(glm::dmat4(),glm::dvec3(-W/NX,-H/NY,1.0f));
}

void PicTrans::GenR3(){
  glm::dvec3 WV = view/(glm::length(view));
  glm::dvec3 UV = glm::cross(up,WV);
  UV = UV/glm::length(UV);
  glm::dvec3 VV = glm::cross(WV,UV);

  R3[0] = glm::dvec4(UV,0.0f);
  R3[1] = glm::dvec4(VV,0.0f);
  R3[2] = glm::dvec4(WV,0.0f);
}


void PicTrans::GenT4(){
  T4[3] = glm::dvec4(eye,1.0f);
}

void PicTrans::GetM(){
  GenT1();
  GenS2();
  GenR3();
  GenT4();
  M = T4 * R3 * S2 * T1;
}

PicTrans::~PicTrans(){}

