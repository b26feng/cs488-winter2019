#include "Tracer.hpp"
#include "GeometryNode.hpp"
#include "PhongMaterial.hpp"
#include "ColorPrint.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>


Tracer::Tracer(PicTrans* P,SceneNode* root, const glm::dvec3& ambient,const std::list<Light *>& lights):
  PT{P},
  Amb{ambient},
  LGT{lights}
{
  NodeCount = root->Count();
  R = std::vector<SceneNode*>(0);
  root->PreProcess(R,nullptr);
  Root = root;
  Background = glm::dvec3(0.0f,0.0f,0.0f);
}

void Tracer::Trace(){
}


/*
void Tracer::RayTrace(Image& image){
  size_t h = image.height();
  size_t w = image.width();
  glm::dvec4 Ray,Screen,Normal,LastNormal,EYE;
  glm::dvec4 NewRay;
  EYE = glm::dvec4(PT->eye,1.0f);
  double t,last;
  bool Init = false;
  PhongMaterial* P;
  glm::dvec3 color;
  SceneNode* i;
  
  for(uint y = 0;y<w;y++){
    for(uint x =0;x<h;x++){
      // Get the world coordinate of current pixel, and calculate ray
      Screen = glm::dvec4((float)x,(float)y,0.0f,1.0f);
      Ray = (PT->M * Screen) - EYE;
      for(int j = 0;j<NodeCount;j++){
	i = R[j];
	//std::cout<<glm::to_string(Ray)<<std::endl;
	
	//GeometryNode* G = dynamic_cast<GeometryNode*>(i);
	//if(G==nullptr) continue;
	if(i->m_nodeType != NodeType::GeometryNode) continue;
	GeometryNode* G = static_cast<GeometryNode*>(i);
	G->GetPhongColor(EYE,Ray,color,Init,LGT,&t,R,NodeCount);
	  /*
	  t = G->m_primitive->GetIntersect(EYE,Ray,Normal);
	  if(t>0 &&(Init==false || (t<last))){
	    //std::cout<<"Substitute"<<std::endl;
	    Init = true;
	    last = t;
	    LastNormal = Normal;
	    P = (PhongMaterial*)(G->m_material);
	    color = Amb;
	    }
      }
      if(Init==false){
	color = Background;
      }else{
	  color += Amb;
      }
      image(x,y,0) = (double)color.x;
      image(x,y,1) = (double)color.y;
      image(x,y,2) = (double)color.z;
      Init = false;
      t = 0;
      //std::cout<<glm::to_string(color)<<std::endl;
      
    }
    Background.x += 0.002f;
    Background.z += 0.001f;
  }
}
*/

void Tracer::RayTrace(Image& image){
  size_t h = image.height();
  size_t w = image.width();
  glm::dvec4 Ray,Screen,Normal,SecN,EYE,Inter;
  glm::dvec4 NewRay;
  glm::dvec3 color;
  double t;
  bool Init = false;
  SceneNode* HitNode;
  //SceneNode* i;
  EYE = glm::dvec4(PT->eye,1.0f);

  for(uint y = 0;y<w;y++){
    for(uint x =0;x<h;x++){
      color = glm::dvec3(0.0f,0.0f,0.0f);
      // Get the world coordinate of current pixel, and calculate ray
      Screen = glm::dvec4(x,y,0.0f,1.0f);
      Ray = (PT->M * Screen) - EYE;
      // check intersection and get corresponding material
      //RED();
      //std::cout<<glm::to_string(EYE)<<std::endl;
      //DEFAULT();
      Root->GetMaterial(EYE,Ray,Normal,&Init,&t,&HitNode);
      if(Init){
	Normal = glm::normalize(Normal);
	// get Intersection
	Inter = EYE + glm::dvec4((t*Ray.x),(t*Ray.y),(t*Ray.z),0.0f);
	GetColor(Inter,Normal, Ray,color,((GeometryNode*)(HitNode))->m_material);
	color += Amb;
      }else{
	color = Background;
      }
      image(x,y,0) = color.x;
      image(x,y,1) = color.y;
      image(x,y,2) = color.z;
      Init = false;
      t = 0;
      HitNode = nullptr;
    }
    Background.x += 0.002f;
    Background.z += 0.001f;
  }
}

void  Tracer::GetColor(glm::dvec4& Intersect,glm::dvec4& Normal, glm::dvec4& EyeRay,glm::dvec3& color,Material* m){
  bool Init = false;
  double t = 0;
  glm::dvec4 SecNormal,Ray;
  SceneNode* HitNode;
  for(Light* L: LGT){
    Ray = glm::dvec4(L->position,1.0f) - Intersect;
    Root->GetMaterial(Intersect,Ray,SecNormal,&Init,&t,&HitNode);
    m->GetColor(EyeRay,Intersect,Ray,Normal,color,L,Init,t);
    Init = false;
    t = 0;
  }
}

Tracer::~Tracer(){}
