// Fall 2018

#pragma once

#include "Material.hpp"
#include <glm/glm.hpp>

#include <list>
#include <string>
#include <iostream>
#include <vector>
enum class NodeType {
	SceneNode,
	GeometryNode,
	JointNode
};

class SceneNode {
public:
  SceneNode(const std::string & name);

  SceneNode(const SceneNode & other);

  virtual ~SceneNode();
    
  int totalSceneNodes() const;
    
  const glm::dmat4& get_transform() const;
  const glm::dmat4& get_inverse() const;
    
  void set_transform(const glm::dmat4& m);
    
  void add_child(SceneNode* child);
    
  void remove_child(SceneNode* child);

  //-- Transformations:
  void rotate(char axis, float angle);
  void scale(const glm::dvec3& amount);
  void translate(const glm::dvec3& amount);
  virtual void PrintType();
  virtual size_t Count();
  virtual void PreProcess(std::vector<SceneNode*>& R,SceneNode* Parent);
  virtual void GetMaterial(glm::dvec4& EYE,glm::dvec4& Ray,glm::dvec4& Normal,bool* init,double* t,SceneNode** Hit);
  
  friend std::ostream & operator << (std::ostream & os, const SceneNode & node);

  // Transformations
  glm::dmat4 trans;
  glm::dmat4 invtrans;

  glm::dmat4 TMat;
  glm::dmat4 RMat;
  glm::dmat4 TRMat;
  glm::dmat4 SMat;
  
  std::list<SceneNode*> children;
  
  NodeType m_nodeType;
  std::string m_name;
  unsigned int m_nodeId;
  
private:
	// The number of SceneNode instances.
	static unsigned int nodeInstanceCount;
};
