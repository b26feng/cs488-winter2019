#pragma once

#include <glm/glm.hpp>

#include "PicTrans.hpp"
#include "SceneNode.hpp"
#include "Light.hpp"
#include "Image.hpp"

class Tracer{
  PicTrans* PT;
  std::vector<SceneNode*> R;
  SceneNode* Root;
  size_t NodeCount;
  const glm::dvec3 Amb;
  const std::list<Light*> LGT;
  glm::dvec3 Background;
  void GetColor(glm::dvec4& Intersect,glm::dvec4& Normal,glm::dvec4& EyeRay,glm::dvec3& color,Material* m);
public:
  Tracer(PicTrans* P,SceneNode* root,const glm::dvec3& ambient,const std::list<Light *>& lights);
  ~Tracer();
  void Trace();
  void RayTrace(Image& image);
};
