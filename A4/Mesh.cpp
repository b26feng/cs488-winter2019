// Winter 2019

#include <iostream>
#include <fstream>

#include <glm/ext.hpp>

// #include "cs488-framework/ObjFileDecoder.hpp"
#include "Mesh.hpp"

const static double PI = 3.14159265358979323846;
const static double ZERO = 0.0001f;

Mesh::Mesh( const std::string& fname )
	: m_vertices()
	, m_faces()
{
  std::string code;
  double vx, vy, vz;
  size_t s1, s2, s3;

  std::ifstream ifs( fname.c_str() );
  while( ifs >> code ) {
    if( code == "v" ) {
      ifs >> vx >> vy >> vz;
      m_vertices.push_back( glm::dvec3( vx, vy, vz ) );
    } else if( code == "f" ) {
      ifs >> s1 >> s2 >> s3;
      m_faces.push_back( Triangle( s1 - 1, s2 - 1, s3 - 1 ) );
    }
  }
  Normals = new glm::dvec4[m_faces.size()];
}

std::ostream& operator<<(std::ostream& out, const Mesh& mesh)
{
  out << "mesh {";
  /*
  
  for( size_t idx = 0; idx < mesh.m_verts.size(); ++idx ) {
  	const MeshVertex& v = mesh.m_verts[idx];
  	out << glm::to_string( v.m_position );
	if( mesh.m_have_norm ) {
  	  out << " / " << glm::to_string( v.m_normal );
	}
	if( mesh.m_have_uv ) {
  	  out << " / " << glm::to_string( v.m_uv );
	}
  }

*/
  out << "}";
  return out;
}

void Mesh::Print() const{
  std::cout<<"Mesh v:"<<m_vertices.size()<<", F:"<<m_faces.size()<<std::endl;
}


void Mesh::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
  this->TMat = TMat;
  this->RMat = RMat;
  Trans = trans;
  Inv = invtrans;
  /*
  int L = m_vertices.size();
  for(int i = 0; i<L;i++){
    // m_vertices[i] = glm::dvec3(TMat * RMat * glm::dvec4(m_vertices[i],1.0f));
    m_vertices[i] = glm::dvec3(trans * glm::dvec4(m_vertices[i],1.0f));

    }*/
  int L = m_faces.size();
  
  for(int i = 0; i<L;i++){
    Normals[i] = glm::dvec4(glm::cross(m_vertices[m_faces[i].v1]-m_vertices[m_faces[i].v2],m_vertices[m_faces[i].v2]-m_vertices[m_faces[i].v3]),0.0f);
  }
}


double Mesh::GetIntersect(glm::dvec4& Origin,glm::dvec4& Ray,glm::dvec4& Normal){
  double Rad,last;
  Rad = 0;
  double T;
  int Index = 0;
  size_t FaceCount = m_faces.size();
  bool Init = false;
  glm::dvec4 IN,V1,V2,V3;
  Triangle* TRI;
  // get intersection with plane one by one
  for(int i = 0; i< FaceCount; i++){
    TRI =&(m_faces[i]);
    T = glm::dot(-Normals[i],Origin-glm::dvec4(m_vertices[TRI->v1],1.0f))/glm::dot(Normals[i],Ray);
    if(T > ZERO && (T < last || !Init)){
      // determine if that's within the plane
      IN = Origin + glm::dvec4(Ray.x*T,Ray.y*T,Ray.z*T,0.0f);
      V1 = glm::dvec4(m_vertices[TRI->v1],1.0f) - IN;
      V2 = glm::dvec4(m_vertices[TRI->v2],1.0f) - IN;
      V3 = glm::dvec4(m_vertices[TRI->v3],1.0f) - IN;
      Rad += glm::acos(glm::dot(V1,V2)/(glm::length(V1)*glm::length(V2)));
      Rad += glm::acos(glm::dot(V2,V3)/(glm::length(V2)*glm::length(V3)));
      Rad += glm::acos(glm::dot(V3,V1)/(glm::length(V1)*glm::length(V3)));
      if(Rad <= 2*PI+ZERO && Rad >= 2*PI-ZERO){
	last = T;
	Index = i;
	Init = true;
      }
      Rad = 0;
    }
  }
  // keep the closer one
  if(Init){
    Normal = Normals[Index];/*
    Normal.x *= 10000;
    Normal.y *= 10000;
    Normal.z *= 10000;*/
    //Inter = RealIN;
    return last + ZERO;
  }
  return 0;
  
}


Mesh::~Mesh(){
  delete[] Normals;
}
