// Winter 2019

#pragma once

#include <vector>
#include <iosfwd>
#include <string>

#include <glm/glm.hpp>

#include "Primitive.hpp"

struct Triangle
{
	size_t v1;
	size_t v2;
	size_t v3;

	Triangle( size_t pv1, size_t pv2, size_t pv3 )
		: v1( pv1 )
		, v2( pv2 )
		, v3( pv3 )
	{}
};

// A polygonal mesh.
class Mesh : public Primitive {
public:
  Mesh( const std::string& fname );
  ~Mesh();
  virtual double GetIntersect(glm::dvec4& Origin, glm::dvec4&Ray,glm::dvec4& Normal) override;
  virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
  virtual void Print() const override;
private:
  std::vector<glm::dvec3> m_vertices;
  std::vector<Triangle> m_faces;
  glm::dvec4* Normals;
  
  friend std::ostream& operator<<(std::ostream& out, const Mesh& mesh);
};
