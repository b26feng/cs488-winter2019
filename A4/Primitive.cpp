// Winter 2019

#include "Primitive.hpp"
#include "polyroots.hpp"

#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>

static const int Recs[24] = {0,1,2,3,5,1,0,4,5,4,7,6,6,7,3,2,6,2,1,5,0,3,7,4};
static const double PI = 3.14159265358979323846;
static const double ZERO = 0.00001;
Primitive::Primitive():
  TMat{glm::dmat4()},
  RMat{glm::dmat4()}
{}

Primitive::~Primitive(){}

void Primitive::Print() const
{
  std::cout<<"Empty"<<std::endl;
}


double Primitive::GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal){
  return 0;
}

void Primitive::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
  
  this->TMat = TMat;
  this->RMat = glm::inverse(RMat);
  Trans = trans;
  Inv = invtrans;
  SMat = glm::inverse(RMat) * glm::inverse(TMat) * trans;
  return;
}

NonhierSphere::NonhierSphere(const glm::dvec3& pos, double radius)
  : m_pos(pos), m_radius(radius)
{
  Type = PType::NSphere;
}

void NonhierSphere::Print() const{
  std::cout<<glm::to_string(m_pos)<<","<<m_radius<<std::endl;
}

double NonhierSphere::GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal){
  
  // get intersection with plane one by one
  //std::cout<<glm::to_string(Origin)<<std::endl;
  glm::dvec4 AC = Origin - glm::dvec4(m_pos,1.0f);
  glm::dvec4 Longer;
  double A = glm::dot(Ray,Ray);
  double B = 2*glm::dot(Ray,AC);
  double C = glm::dot(AC,AC) - (m_radius*m_radius);
  double Roots[2] = {0,0};
  int n = quadraticRoots(A,B,C,Roots);
  double Result = 0;
  //std::cout<<A<<","<<B<<std::endl;
  if(n){
    if(Roots[0] == Roots[1]){
      Result = Roots[0];
    }else{
      // return the one that's closer
      double Larger = (Roots[0] > Roots[1])? Roots[0]:Roots[1];
      double Smaller = Roots[0]+Roots[1]-Larger;
      Longer = Ray;
      if(Smaller>0){
	Result = Smaller - ZERO;
      }else if(Smaller <= 0 && Larger > ZERO){
	Result = Larger + ZERO;
      }
    }
    if(Result != 0){
      Longer.x *= Result;
      Longer.y *= Result;
      Longer.z *= Result;
      // Calculate Normal
      Normal = (Origin + Longer) - glm::dvec4(m_pos,1.0f);
      /*
      Normal.x *= 10000;
      Normal.y *= 10000;
      Normal.z *= 10000;
      */
      return Result - ZERO;
    }
  }else{
    return 0;
  }
}
/*
void NonhierSphere::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
  /*
  this->TMat = TMat;
  this->RMat = RMat;
  Trans = trans;
  Inv = invtrans;
  //m_pos = glm::dvec3(trans * glm::dvec4(m_pos,1.0f));
}
*/

NonhierSphere::~NonhierSphere()
{
}


NonhierBox::NonhierBox(const glm::dvec3& pos, double size):
  m_pos(pos),
  m_size(size)
{
  Type = PType::NBox;
  Vertices = new glm::dvec4[8];
  Vertices[0] = glm::dvec4(size,0.0f,0.0f,1.0f);
  Vertices[1] = glm::dvec4(size,0.0f,size,1.0f);
  Vertices[2] = glm::dvec4(size,size,size,1.0f);
  Vertices[3] = glm::dvec4(size,size,0.0f,1.0f);
  Vertices[4] = glm::dvec4(0.0f,0.0f,0.0f,1.0f);
  Vertices[5] = glm::dvec4(0.0f,0.0f,size,1.0f);
  Vertices[6] = glm::dvec4(0.0f,size,size,1.0f);
  Vertices[7] = glm::dvec4(0.0f,size,0.0f,1.0f);
  for(int i = 0;i<8;i++){
    Vertices[i] = glm::dvec4(pos,0.0f) + Vertices[i];
  }
  
  Normals = new glm::dvec4[6];
}

void NonhierBox::Print() const{
  std::cout<<glm::to_string(m_pos)<<","<<m_size<<std::endl;
}



void NonhierBox::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
  
  this->TMat = TMat;
  this->RMat = RMat;
  Trans = trans;
  Inv = invtrans;
  //m_pos = glm::dvec3(TMat * RMat * glm::dvec4(m_pos,1.0f));
  /*
  m_pos = glm::dvec3(trans * glm::dvec4(m_pos,1.0f));
  for(int i = 0;i<8;i++){
    //Vertices[i] = TMat * RMat * Vertices[i];
    Vertices[i] = trans * Vertices[i];
    }*/
  for(int i = 0; i<6;i++){
    Normals[i] = glm::dvec4(glm::cross(glm::dvec3(Vertices[Recs[(i<<2)+1]]-Vertices[Recs[(i<<2)+2]]),glm::dvec3(Vertices[Recs[i<<2]]-Vertices[Recs[(i<<2)+1]])),0.0f);
    }
  
}


double NonhierBox::GetIntersect(glm::dvec4& Origin, glm::dvec4& Ray,glm::dvec4& Normal){
  double Rad,last;
  Rad = 0;
  double T;
  int Index = 0;
  bool Init = false;
  glm::dvec4 IN,V1,V2;
  // get intersection with plane one by one
  //std::cout<<glm::to_string(Ray)<<std::endl;
  for(int i = 0; i< 6; i++){

    T = glm::dot(-Normals[i],Origin-Vertices[Recs[i<<2]])/glm::dot(Normals[i],Ray);
    if(T > 0 && (T < last || !Init)){
      // determine if that's within the plane
      IN = Origin + glm::dvec4(Ray.x*T,Ray.y*T,Ray.z*T,0.0f);
      V1 = Vertices[Recs[(i<<2)]] - IN;
      // check inner angle sum
      for(int j = 0;j<4;j++){
	V2 = Vertices[Recs[(i<<2)+((j+1)%4)]]-IN;
	Rad += glm::acos(glm::dot(V1,V2)/(glm::length(V1)*glm::length(V2)));
	V1 = V2;
      }
      
      if(Rad <= 2*PI+ZERO && Rad >= 2*PI-ZERO){
	//std::cout<<Rad<<std::endl;
	last = T;
	Index = i;
	Init = true;
	//RealIN = IN;
	//Store = testCross;
      }
      Rad = 0;
    }
  };
  // keep the closer one
  if(Init){
    Normal =  Normals[Index];/*
    Normal.x *= 10000;
    Normal.y *= 10000;
    Normal.z *= 10000;*/
    //std::cout<<Index<<"|"<<glm::to_string(Normal)<<std::endl;
    //Normal = Store;
    //Inter = RealIN;
    return last - ZERO;
  }
  return 0;
  
}

NonhierBox::~NonhierBox()
{
  delete[] Normals;
  delete[] Vertices;
}

Sphere::Sphere():
  NonhierSphere(glm::dvec3(0.0f),1.0f)
{
  Type = PType::Sphere;
}

Cube::Cube():
  NonhierBox(glm::dvec3(0.0f),1.0f)
{
  Type = PType::Cube;
}
