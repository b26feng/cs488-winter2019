// Winter 2019

#include "A2.hpp"
#include "cs488-framework/GlErrorCheck.hpp"

#include <iostream>
using namespace std;

#include <imgui/imgui.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/string_cast.hpp>
using namespace glm;

const static float ZERO = 0.00001f;
const static float PI = 3.14159265358979323846;

#define ESC 27
#define RED() printf("%c[31m",ESC);
#define YELLOW() printf("%c[33m",ESC);
#define PURPLE() printf("%c[35m",ESC);
#define DEFAULT() printf("%c[39;49m",ESC);

const static int CubeSeq[12] = {0,2,4,6,0,1,4,5,0,1,2,3};
const static mat3 CubeCoordColour = mat3();
const static mat3 WorldColour = mat3(vec3(1.0f),vec3(1.0f),vec3(1.0f));

//----------------------------------------------------------------------------------------
// Constructor
VertexData::VertexData()
  : numVertices(0),
    index(0)
{
  positions.resize(kMaxVertices);
  colours.resize(kMaxVertices);
}
  
//----------------------------------------------------------------------------------------
// Constructor
A2::A2()
  : m_currentLineColour(vec3(0.0f)),
    TheCube(ObjData(8)),
    WorldOrigin(vec3(0.0f)),
    LookFrom(vec3(3.0f,3.0f,4.0f)),
    LookAt(vec3(0.0f)),
    Up(vec3(0.0f,1.0f,0.0f)),
    FVD(30.0f),
    ON(1.0f),
    OF(10.0f),
    VT(mat4()),
    VR(mat4()),
    MT(mat4()),
    MR(mat4()),
    MS(mat4()),
    VPS(mat4()),
    Changed(true)
{
  // init cube
  for(int i = -1;i<=1;i+=2){
    for(int j = -1;j<=1;j+=2){
      for(int k = -1; k<=1;k+=2){
	TheCube.Vertices.emplace_back(vec3((float) i,(float) j,(float) k));
      }
    }
  }

  // init V

  CalculateV();
  //LookFrom = vec3(0.0f);
  //LookAt = vec3(-1.0f,1.0f,-1.0f);
  
  // some how calculate the View coordinate and View Matrix
  
  // init P
  FoVDeg = FVD;
  N = ON;
  F = OF;
  //FoVDeg = 10.0f;
  ASPC = 1.0f;
  // some how calculate the Perspective matrix

  RecordStart = true;
  VStartX = 0.95f;
  VStartY = 0.95f;
  VCurrentX = -0.95f;
  VCurrentY = -0.95f;
  VPS[0][0] = 0.95f;
  VPS[1][1] = 0.95f;
  //Set keyboard code and mouse code to 0
  KeyFlag = MouseFlag = 0;
}

//----------------------------------------------------------------------------------------
// Destructor
A2::~A2()
{

}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A2::init()
{
  // Set the background colour.
  glClearColor(0.3, 0.5, 0.7, 1.0);

  createShaderProgram();

  glGenVertexArrays(1, &m_vao);

  enableVertexAttribIndices();

  generateVertexBuffers();

  mapVboDataToVertexAttributeLocation();

}

//----------------------------------------------------------------------------------------
void A2::createShaderProgram()
{
  m_shader.generateProgramObject();
  m_shader.attachVertexShader( getAssetFilePath("VertexShader.vs").c_str() );
  m_shader.attachFragmentShader( getAssetFilePath("FragmentShader.fs").c_str() );
  m_shader.link();
}

//---------------------------------------------------------------------------------------- Winter 2019
void A2::enableVertexAttribIndices()
{
  glBindVertexArray(m_vao);

  // Enable the attribute index location for "position" when rendering.
  GLint positionAttribLocation = m_shader.getAttribLocation( "position" );
  glEnableVertexAttribArray(positionAttribLocation);

  // Enable the attribute index location for "colour" when rendering.
  GLint colourAttribLocation = m_shader.getAttribLocation( "colour" );
  glEnableVertexAttribArray(colourAttribLocation);

  // Restore defaults
  glBindVertexArray(0);

  CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
void A2::generateVertexBuffers()
{
  // Generate a vertex buffer to store line vertex positions
  {
    glGenBuffers(1, &m_vbo_positions);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);

    // Set to GL_DYNAMIC_DRAW because the data store will be modified frequently.
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * kMaxVertices, nullptr,
		 GL_DYNAMIC_DRAW);


    // Unbind the target GL_ARRAY_BUFFER, now that we are finished using it.
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK_GL_ERRORS;
  }

  // Generate a vertex buffer to store line colors
  {
    glGenBuffers(1, &m_vbo_colours);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);

    // Set to GL_DYNAMIC_DRAW because the data store will be modified frequently.
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * kMaxVertices, nullptr,
		 GL_DYNAMIC_DRAW);


    // Unbind the target GL_ARRAY_BUFFER, now that we are finished using it.
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK_GL_ERRORS;
  }
}

//----------------------------------------------------------------------------------------
void A2::mapVboDataToVertexAttributeLocation()
{
  // Bind VAO in order to record the data mapping.
  glBindVertexArray(m_vao);

  // Tell GL how to map data from the vertex buffer "m_vbo_positions" into the
  // "position" vertex attribute index for any bound shader program.
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);
  GLint positionAttribLocation = m_shader.getAttribLocation( "position" );
  glVertexAttribPointer(positionAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

  // Tell GL how to map data from the vertex buffer "m_vbo_colours" into the
  // "colour" vertex attribute index for any bound shader program.
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);
  GLint colorAttribLocation = m_shader.getAttribLocation( "colour" );
  glVertexAttribPointer(colorAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

  //-- Unbind target, and restore default values:
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  CHECK_GL_ERRORS;
}

//---------------------------------------------------------------------------------------
void A2::initLineData()
{
  m_vertexData.numVertices = 0;
  m_vertexData.index = 0;
}

//---------------------------------------------------------------------------------------
void A2::setLineColour (
			const glm::vec3 & colour
			) {
  m_currentLineColour = colour;
}

//---------------------------------------------------------------------------------------
void A2::drawLine(
		  const glm::vec2 & V0,   // Line Start (NDC coordinate)
		  const glm::vec2 & V1    // Line End (NDC coordinate)
		  ) {

  m_vertexData.positions[m_vertexData.index] = V0;
  m_vertexData.colours[m_vertexData.index] = m_currentLineColour;
  ++m_vertexData.index;
  m_vertexData.positions[m_vertexData.index] = V1;
  m_vertexData.colours[m_vertexData.index] = m_currentLineColour;
  ++m_vertexData.index;

  m_vertexData.numVertices += 2;
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A2::appLogic()
{
  // Place per frame, application logic here ...

  // Call at the beginning of frame, before drawing lines:
  initLineData();
  int Len;
  // get all the lines, Cube, Cube Coordinate
  if(Changed){
    CalculateV();
    CalculateM();
    CalculateNDC();
    vector<glm::vec4> DrawLines;
    vector<glm::vec4> CubeCoordinate;
    vector<glm::vec4> WorldCoord;
    int Next;
    Next = 0;
    for(int i = 0; i<12;i++){
      if(i%4 == 0){
	Next = (Next == 0)? 1: Next<<1;
      }
      DrawLines.emplace_back(VR*VT*V*M*vec4(TheCube.Vertices[CubeSeq[i]],1.0f));
      DrawLines.emplace_back(VR*VT*V*M*vec4(TheCube.Vertices[CubeSeq[i]+Next],1.0f));
    }
    // CubeCoordinate
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(0.0f,0.0f,0.0f,1.0f));
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(1.0f,0.0f,0.0f,1.0f));
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(0.0f,0.0f,0.0f,1.0f));
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(0.0f,1.0f,0.0f,1.0f));
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(0.0f,0.0f,0.0f,1.0f));
    CubeCoordinate.emplace_back(VR*VT*V*M*vec4(0.0f,0.0f,1.0f,1.0f));

    // World Coordinate
    WorldCoord.emplace_back(VR*VT*V*vec4(0.0f,0.0f,0.0f,1.0f));
    WorldCoord.emplace_back(VR*VT*V*vec4(1.0f,0.0f,0.0f,1.0f));
    WorldCoord.emplace_back(VR*VT*V*vec4(0.0f,0.0f,0.0f,1.0f));
    WorldCoord.emplace_back(VR*VT*V*vec4(0.0f,1.0f,0.0f,1.0f));
    WorldCoord.emplace_back(VR*VT*V*vec4(0.0f,0.0f,0.0f,1.0f));
    WorldCoord.emplace_back(VR*VT*V*vec4(0.0f,0.0f,1.0f,1.0f));

    vector<glm::vec4> Backup = DrawLines;
    Clipped = Clip(Backup);
    ClippedCoord = Clip(CubeCoordinate);
    WorldClipped = Clip(WorldCoord);
    Project(Clipped);
    Project(ClippedCoord);
    Project(WorldClipped);
    NDCS(Clipped);
    NDCS(ClippedCoord);
    NDCS(WorldClipped);
    Changed = false;
  }
  
  vector<glm::vec4> DrawBuffer = VPProcess(Clipped);

  Len = Clipped.size();
  setLineColour(vec3(1.0f,1.0f,1.0f));
  vec2 Start,End;
  for(int i = 0; i<Len;i+=2){
    Start = vec2(DrawBuffer[i].x,DrawBuffer[i].y);
    End = vec2(DrawBuffer[i+1].x,DrawBuffer[i+1].y);
    drawLine(Start,End);
  }

  // draw cube coordinate
  DrawBuffer = VPProcess(ClippedCoord);
  Len = ClippedCoord.size();
  //Len /= 2;
  for(int i = 0;i<Len/2;i++){
    setLineColour(CubeCoordColour[i]);
    Start = vec2(DrawBuffer[i<<1]);
    End = vec2(DrawBuffer[(i<<1)+1]);
    drawLine(Start,End);
  }

  DrawBuffer = VPProcess(WorldClipped);
  Len = WorldClipped.size();
  for(int i = 0; i<Len/2;i++){
    setLineColour(WorldColour[i]-CubeCoordColour[i]);
    Start = vec2(DrawBuffer[i<<1]);
    End = vec2(DrawBuffer[(i<<1)+1]);
    drawLine(Start,End);
  }

  
  // draw viewport
  setLineColour(vec3(0.4f));
  drawLine(vec2(VStartX,VStartY),vec2(VCurrentX,VStartY));
  drawLine(vec2(VStartX,VStartY),vec2(VStartX,VCurrentY));
  drawLine(vec2(VCurrentX,VCurrentY),vec2(VCurrentX,VStartY));
  drawLine(vec2(VCurrentX,VCurrentY),vec2(VStartX,VCurrentY));

  
  /*
  // Draw outer square:
  setLineColour(vec3(1.0f, 0.7f, 0.8f));
  drawLine(vec2(-0.5f, -0.5f), vec2(0.5f, -0.5f));
  drawLine(vec2(0.5f, -0.5f), vec2(0.5f, 0.5f));
  drawLine(vec2(0.5f, 0.5f), vec2(-0.5f, 0.5f));
  drawLine(vec2(-0.5f, 0.5f), vec2(-0.5f, -0.5f));


  // Draw inner square:
  setLineColour(vec3(0.2f, 1.0f, 1.0f));
  drawLine(vec2(-0.25f, -0.25f), vec2(0.25f, -0.25f));
  drawLine(vec2(0.25f, -0.25f), vec2(0.25f, 0.25f));
  drawLine(vec2(0.25f, 0.25f), vec2(-0.25f, 0.25f));
  drawLine(vec2(-0.25f, 0.25f), vec2(-0.25f, -0.25f));
  */
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A2::guiLogic()
{
  static bool firstRun(true);
  if (firstRun) {
    ImGui::SetNextWindowPos(ImVec2(50, 50));
    firstRun = false;
  }

  static bool showDebugWindow(true);
  ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
  float opacity(0.5f);

  ImGui::Begin("Properties", &showDebugWindow, ImVec2(100,100), opacity,
	       windowFlags);


  ImGui::Text("Near=%f,Far=%f",N,F);
  ImGui::SameLine();
  int something;
  if( ImGui::RadioButton( "Perspective", &something, 0 ) ) {
    KeyFlag = (1<<4);
  }
  ImGui::SameLine();
  if(ImGui::RadioButton("Unselect Perspective", &something,0)){
    something = 0;
  }

  // Add more gui elements here here ...


  // Create Button, and check if it was clicked:
  if( ImGui::Button( "Quit Application" ) ) {
    glfwSetWindowShouldClose(m_window, GL_TRUE);
  }

  ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

  ImGui::End();
}

//----------------------------------------------------------------------------------------
void A2::uploadVertexDataToVbos() {

  //-- Copy vertex position data into VBO, m_vbo_positions:
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_positions);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec2) * m_vertexData.numVertices,
		    m_vertexData.positions.data());
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK_GL_ERRORS;
  }

  //-- Copy vertex colour data into VBO, m_vbo_colours:
  {
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_colours);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * m_vertexData.numVertices,
		    m_vertexData.colours.data());
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK_GL_ERRORS;
  }
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A2::draw()
{
  uploadVertexDataToVbos();

  glBindVertexArray(m_vao);

  m_shader.enable();
  glDrawArrays(GL_LINES, 0, m_vertexData.numVertices);
  m_shader.disable();

  // Restore defaults
  glBindVertexArray(0);

  CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A2::cleanup()
{

}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A2::cursorEnterWindowEvent (
				 int entered
				 ) {
  bool eventHandled(false);

  // Fill in with event handling code...

  return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A2::mouseMoveEvent(double xPos, double yPos)
{
  bool eventHandled(false);
  if(!ImGui::IsMouseHoveringAnyWindow()){
    if(MouseFlag){
      float deltaX = (float) (xPos-LastX);
      float deltaY = (float) (yPos-LastY);
      float* FPtr;
      char PerspectiveLimit = 0;
      
      int sign; 
      Distance = sqrt(deltaX*deltaX+deltaY*deltaY)/50;
      Distance = (xPos-LastX > 0)? Distance:-Distance;
      vec3 Direction;

      glm::mat4* TMat;
      glm::mat4* RMat;
      vec3 XAx,YAx,ZAx;
      
      XAx = vec3(1.0f,0.0f,0.0f);
      YAx = vec3(0.0f,1.0f,0.0f);
      ZAx = vec3(0.0f,0.0f,1.0f);
      
      vec4 RAX,RAY,RAZ;
      vec4 Origin = vec4(0.0f);
      Origin.w = 1.0f;
      
      glm::mat4 BaseMat = glm::mat4();
      switch(KeyFlag){
      case (1<<6):
	RAX = vec4(vec3(1.0f,0.0f,0.0f),1.0f);
	RAY = vec4(vec3(0.0f,1.0f,0.0f),1.0f);
	RAZ = vec4(vec3(0.0f,0.0f,1.0f),1.0f);
	if(MouseFlag & (1<<2)) VR *= RotateArbitrary(Distance/50,RAX,Origin);
	  //Direction = Distance*XAx; 
	if(MouseFlag &(1<<1)) VR *= RotateArbitrary(Distance/50,RAY,Origin);
	if(MouseFlag & 1) VR *= RotateArbitrary(Distance,RAZ,Origin);
	break;
	
      case (1<<5):
	if(MouseFlag & (1<<2)){
	  Direction = Distance * XAx;
	  Translate(BaseMat,Direction);
	  VT *= BaseMat;
	}
	if(MouseFlag & (1<<1)){
	  Direction = Distance * YAx;
	  Translate(BaseMat,Direction);
	  VT *= BaseMat;
	}
	if(MouseFlag & 1){
	  Direction = Distance * ZAx;
	  Translate(BaseMat,Direction);
	  VT *= BaseMat;
	}
	break;
      case (1<<4):
	PerspectiveLimit |= ((((xPos-LastX)>0 && (FoVDeg+Distance) <=160.0f) || ((xPos-LastX) < 0 && (FoVDeg-Distance) >= 20.0f)) << 2);
	PerspectiveLimit |= ((((xPos-LastX)>0 && ((F-N) - Distance) > ZERO) || ((yPos-LastY)<0 && (N-Distance) > ZERO)) << 1);
	PerspectiveLimit |= (((xPos - LastX) < 0 && ((F-N)-Distance) > ZERO) || ((xPos-LastX) > 0));
	PerspectiveLimit &= MouseFlag;
	
	if(MouseFlag & (1<<2))  FoVDeg += Distance;
	if(MouseFlag &(1<<1)) N += Distance;
	if(MouseFlag & 1) F += Distance;
	break;
      case (1<<3):
	RAX = MT*vec4(vec3(1.0f,0.0f,0.0f),1.0f);
	RAY = MT*vec4(vec3(0.0f,1.0f,0.0f),1.0f);
	RAZ = MT*vec4(vec3(0.0f,0.0f,1.0f),1.0f);
	Origin = MT*Origin;
	if(MouseFlag & (1<<2)) MR *= RotateArbitrary(Distance,RAX,Origin);
	if(MouseFlag &(1<<1)) MR *= RotateArbitrary(Distance,RAY,Origin);
	if(MouseFlag & 1) MR *= RotateArbitrary(Distance,RAZ,Origin);
	break;
      case (1<<2):
	if(MouseFlag & (1<<2)){
	  Direction = Distance * XAx;
	  Translate(BaseMat,Direction);
	  MT *= BaseMat;
	}
	if(MouseFlag & (1<<1)){
	  Direction = Distance * YAx;
	  Translate(BaseMat,Direction);
	  MT *= BaseMat;
	}
	if(MouseFlag & 1){
	  Direction = Distance * ZAx;
	  Translate(BaseMat,Direction);
	  MT *= BaseMat;
	}
	break;
      case (1<<1):
	// scaling
	if(MouseFlag & (1<<2)) MS[0].x += (MS[0].x + Distance < ZERO)? 0:Distance;
	if(MouseFlag &(1<<1)) MS[1].y += (MS[1].y + Distance < ZERO)? 0:Distance;
	if(MouseFlag & 1) MS[2].z += (MS[2].z + Distance < ZERO)? 0:Distance;
	break;
      case (1):
	if(MouseFlag){
	  int w,h;
	  glfwGetWindowSize(m_window,&w,&h);
	  if(RecordStart){
	    VStartX = (2*xPos/w)-1.0f;
	    VStartY = (2*(h-yPos)/h)-1.0f;
	    RecordStart = false;
	  }
	  VCurrentX = (2*xPos/w)-1.0f;
	  VCurrentY = (2*(h-yPos)/h)-1.0f;
	}
	break;
      }
      Changed = true;
      
    }
    LastX = xPos;
    LastY = yPos;
    eventHandled = true;
  }
  // Fill in with event handling code...

  return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A2::mouseButtonInputEvent (
				int button,
				int actions,
				int mods
				) {
  bool eventHandled(false);
  if(!ImGui::IsMouseHoveringAnyWindow()){
    if(actions == GLFW_PRESS){
      if(button == GLFW_MOUSE_BUTTON_LEFT ){
	MouseFlag |= 1<<2;
      }
      if(button == GLFW_MOUSE_BUTTON_MIDDLE){
	MouseFlag |= 1<<1;
      }
      if(button == GLFW_MOUSE_BUTTON_RIGHT){
	MouseFlag |= 1;
      }
    }
    if(actions == GLFW_RELEASE){
      if(button == GLFW_MOUSE_BUTTON_LEFT ){
	MouseFlag ^= 1<<2;
      }
      if(button == GLFW_MOUSE_BUTTON_MIDDLE){
	MouseFlag ^= 1<<1;
      }
      if(button == GLFW_MOUSE_BUTTON_RIGHT){
	RecordStart = true;
	MapToViewport();
	MouseFlag ^= 1;
      }
    }
  }
  // Fill in with event handling code...
  eventHandled = true;
  return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A2::mouseScrollEvent (
			   double xOffSet,
			   double yOffSet
			   ) {
  bool eventHandled(false);

  // Fill in with event handling code...

  return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A2::windowResizeEvent (
			    int width,
			    int height
			    ) {
  bool eventHandled(false);

  // Fill in with event handling code...

  return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A2::keyInputEvent (
			int key,
			int action,
			int mods
			) {
  bool eventHandled(false);

  // Fill in with event handling code...
  if(action == GLFW_PRESS){
    switch(key){
    case GLFW_KEY_A:
      KeyFlag = (1<<7);
      Reset();
      break;
    case GLFW_KEY_O:
      KeyFlag = (1<<6);
      break;
    case GLFW_KEY_E:
      KeyFlag = (1<<5);
      break;
    case GLFW_KEY_P:
      KeyFlag = (1<<4);
      break;
    case GLFW_KEY_R:
      KeyFlag = (1<<3);
      break;
    case GLFW_KEY_T:
      KeyFlag = (1<<2);
      break;
    case GLFW_KEY_S:
      KeyFlag = (1<<1);
      break;
    case GLFW_KEY_V:
      KeyFlag = 1;
      break;
    case GLFW_KEY_Q:
      Quit = true;
      glfwSetWindowShouldClose(m_window, GL_TRUE);
      break;
    }
    eventHandled = true;
  }else if(action == GLFW_RELEASE){
    switch(key){
    case GLFW_KEY_V:
      RecordStart = true;
      MapToViewport();
    case GLFW_KEY_A:
    case GLFW_KEY_O:
    case GLFW_KEY_E:
    case GLFW_KEY_P:
    case GLFW_KEY_R:
    case GLFW_KEY_T:
    case GLFW_KEY_S:
      KeyFlag = 0;
      break;
    }
    eventHandled = true;
  }
  return eventHandled;
}


void A2::CalculateM()
{

  M = MS * MR * MT;

}
void A2::CalculateV()
{ 
  Vz  = glm::normalize(LookAt-LookFrom);
  Vy = Up;
  vec3 Direction;
  if(Vz.x == 0 && Vz.z == 0){
    if(Vz.y < 0){
      Vy = vec3(0.0f,0.0f,-1.0f);
    }else{
      Vy = vec3(0.0f,0.0f,1.0f);
    }
  }

  //Vy = vec3(VR*vec4(Vy,1.0f));
  
  // calculate x axis
  Vx = glm::normalize(glm::cross(Vz,Vy));
  Vy = glm::normalize(glm::cross(Vx,Vz));



  // Get transformation matrix V
  V = mat4();
  glm::mat4 RMatrix;

  
  Direction = WorldOrigin - LookFrom;
  Translate(V,Direction);

  // try this, or switch to row row row
  // this is column column column
  RMatrix[0][0] = Vx.x;
  RMatrix[1][0] = Vx.y;
  RMatrix[2][0] = Vx.z;
  RMatrix[0][1] = Vy.x;
  RMatrix[1][1] = Vy.y;
  RMatrix[2][1] = Vy.z;
  RMatrix[0][2] = Vz.x;
  RMatrix[1][2] = Vz.y;
  RMatrix[2][2] = Vz.z;

  V = RMatrix * V;

}


void A2::CalculateNDC()
{
  
  FoVRad = (FoVDeg/180) * PI;
  NDC = glm::mat4();
  NDC[0].x = ASPC/glm::tan(FoVRad/2);
  NDC[1].y = 1/glm::tan(FoVRad/2);
  NDC[2].z = (N+F)/N;
  NDC[3].z = -F;
  NDC[2].w = 1/N;
  NDC[3].w = 0.0f;
}


void A2::Translate(glm::mat4& BaseMat, glm::vec3& Dest)
{
  BaseMat[3].x = Dest.x;
  BaseMat[3].y = Dest.y;
  BaseMat[3].z = Dest.z;
}


void A2::RotateX(glm::mat4& BaseMat,float Rad){
  RotateX(BaseMat,glm::cos(Rad),glm::sin(Rad));
}

void A2::RotateY(glm::mat4& BaseMat,float Rad){
  RotateY(BaseMat,glm::cos(Rad),glm::sin(Rad));
}

void A2::RotateZ(glm::mat4& BaseMat,float Rad){
  RotateZ(BaseMat,glm::cos(Rad),glm::sin(Rad));
}

glm::mat4 A2::RotateArbitrary(float Rad,vec4& Axis,vec4& Origin){
  glm::mat4 BaseMat = mat4();
  glm::mat4 Align = mat4();
  glm::mat4 R = mat4();
  glm::mat4 Trans = mat4();
  glm::mat4 GetBack= mat4();
  glm::vec3 WorldOrigin(0.0f);
  glm::vec3 V3Origin = vec3(Origin);
  glm::vec3 Direction = WorldOrigin - V3Origin;
  glm::vec4 ReverseAxis = Axis;
  ReverseAxis.y = -Axis.y;
  ReverseAxis.z = -Axis.z;
  
  Translate(Trans,Direction);
  glm::vec4 Intermediate = Trans * Axis;
  AlignAxis(Align,Intermediate);
  RotateX(R,Rad);  
  glm::mat4 Reverse = Transpose(Align);
  //AlignAxis(Reverse,ReverseAxis);
  //Transpose(Align);
  Translate(GetBack,V3Origin);

  BaseMat *= GetBack;
  BaseMat *= Reverse;
  BaseMat *= R;
  BaseMat *= Align;
  BaseMat *= Trans;

  glm::vec4 TA = Axis;
  return BaseMat;
  
}

glm::mat4 A2::Transpose(glm::mat4& M)
{
  glm::mat4 Mt = mat4();
  for(int i = 0;i<4;i++){
    for(int j = 0; j<4; j++){
      Mt[i][j]= M[j][i];
    }
  }
  return Mt;
}

//XAxis must be normalized, length = 1
void A2::AlignAxis(glm::mat4& BaseMat,glm::vec4& XAxis)
{
  // First get the reflection around XAxis, then calculate the angle
  glm::mat4 RY;
  glm::mat4 RZ;
  
  // Get Rotation around Y
  // cos(fi) = x/sqrt(x^2 + z^2)
  float ProjToXZ = glm::sqrt(glm::pow(XAxis.x,2)+glm::pow(XAxis.z,2));
  if(XAxis.z !=0 || XAxis.x!=0){ 
   
    float CosPhi = XAxis.x / ProjToXZ;
    float SinPhi = XAxis.z / ProjToXZ;
    
    RotateY(RY,CosPhi,SinPhi);
  }
  float xlen = sqrt(pow(XAxis.x,2)+ pow(XAxis.y,2) + pow(XAxis.z,2));
  float CosPsi = ProjToXZ/xlen;
  float SinPsi = -XAxis.y/xlen;
  RotateZ(RZ,CosPsi,SinPsi);

  BaseMat = RZ * RY;

  glm::vec4 TA = XAxis;
}


void A2::RotateZ(glm::mat4& BaseMat,float Cos,float Sin)
{
  BaseMat[0].x = Cos;
  BaseMat[0].y = Sin;
  BaseMat[1].x = -Sin;
  BaseMat[1].y = Cos;
}


void A2::RotateY(glm::mat4& BaseMat,float Cos,float Sin)
{
  BaseMat[0].x = Cos;
  BaseMat[0].z = -Sin;
  BaseMat[2].x = Sin;
  BaseMat[2].z = Cos;
}

void A2::RotateX(glm::mat4& BaseMat,float Cos,float Sin)
{
  BaseMat[1].y = Cos;
  BaseMat[1].z = Sin;
  BaseMat[2].y = -Sin;
  BaseMat[2].z = Cos;
}

// each 2 forms a line
// Must Be represented in View Matrix
vector<vec4> A2::Clip(vector<vec4>& Lines)
{
  const char Code = 1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5); 
  size_t L = Lines.size();
  char Codes[2];

  vector<vec4> Dest;
  for(int i = 0; i<L;i++){
    Codes[i%2] = GetCode(Lines[i]);
    if(i%2){
      // if and equals 00111111, Accept
      if((Codes[0] & Codes[1]) == Code){
	Dest.emplace_back(Lines[i-1]);
	Dest.emplace_back(Lines[i]);
	continue;
      }
      
      // if or equals to 00111111, Clip
      if((Codes[0] | Codes[1]) == Code){ 
	if(ClipLine(Lines,i,Codes)){
	  Dest.emplace_back(Lines[i-1]);
	  Dest.emplace_back(Lines[i]);
	}
      }
      // otherwise it's rejected
    }
  }
  return Dest;
}

// check if really need cliping, if so
// modify the vertex to be the clipped
// otherwise make it remain untouched
bool A2::ClipLine(vector<vec4>& Lines,int OddIndex, char* Codes){
  const char Code = 1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5);
  // if SubIndex is -1, then both need to be changed
  // if subindex is OddIndex, then OddIndex need to be changed
  int SubIndex = -1;
  if(Codes[1] == Code){
    SubIndex = OddIndex - 1;
  }else if(Codes[0] == Code){
    SubIndex = OddIndex;
  }
  bool RealClip = false;
  // See wich planes need to be checked
  char Planes = Codes[0] ^ Codes[1];
  vec4 Line = Lines[OddIndex] - Lines[OddIndex-1];
  vec4 Intersect;
  // Up ,Bottom, Left, Right, Front, Back
  for(char PlaneBit = 1<<5;PlaneBit > 0; PlaneBit >>= 1){
    if(PlaneBit & Planes){
      Intersect = GetInter(Lines[OddIndex-1],Lines[OddIndex], PlaneBit);
      if(GetCode(Intersect) == Code){
	if(SubIndex == -1){
	  RealClip = true;
	  Lines[OddIndex - 1] = Intersect;
	  SubIndex = OddIndex;
	}else{
	  RealClip = true;
	  Lines[SubIndex] = Intersect;
	  break;
	}
      }
    }
  }
  return RealClip;
}

glm::vec4 A2::GetInter(glm::vec4& A,glm::vec4& B, char PlaneBit){
  float Z1Z0 = B.z - A.z;
  float t,up;
  if(PlaneBit >= (1<<4)){
    float TAN = glm::tan(FoVRad/2);
    if(PlaneBit < (1<<5)) TAN = - TAN;
    up = TAN*A.z-A.y;
    t = up/((B.y-A.y)-TAN*(Z1Z0));
  }else if(PlaneBit >= (1<<2)){
    float TAN = glm::tan(FoVRad/2);
    if(PlaneBit == (1<<3)) TAN = -TAN;
    up = TAN*A.z - A.x;
    t = up/((B.x-A.x) - TAN *(Z1Z0));
  }else{
    float Constant = (PlaneBit == 1)? F:N;
    up = Constant - A.z;
    t = up/Z1Z0;
  }
  
  glm::vec4 intersection(A.x + t*(B.x-A.x),
			 A.y + t*(B.y-A.y),
			 A.z + t*(B.z-A.z),
			 1.0f);
  return intersection;
}

char A2::GetCode(glm::vec4& Point)
{
  char Code = 1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5); 
  // Determine the code by following procedure
  // take the upper plaine of Frustum as an example
  // get how far away it is from camera
  // then get the expected y of that distance
  // compare then you get is it higher than plane or not
  // YU = XR, YB = XL = - YU
  float YU;
  YU = Point.z * glm::tan(FoVRad/2);
  if(Point.y > YU + ZERO){
    Code ^= (1<<5);
  }else if(Point.y < -YU - ZERO){
    Code ^= 1<<4;
  }
  if(Point.x > YU + ZERO){
    Code ^= (1<<2);
  }else if(Point.x < -YU - ZERO){
    Code ^= (1<<3);
  }
  if(Point.z < N - ZERO){
    Code ^= (1<<1);
  }else if(Point.z > F+  + ZERO){
    Code ^= 1;
  }
  
  return Code;
}
 
 void A2::Project(std::vector<glm::vec4>& Lines)
{
  int L = Lines.size();
  for(int i = 0; i<L; i++){
    Lines[i].x *= (N/Lines[i].z);
    Lines[i].y *= (N/Lines[i].z);
    Lines[i].z = (Lines[i].z*(F+N)-2*F*N)/(Lines[i].z*(F-N));
    Lines[i].w = 1;
  }
}
 
void A2::NDCS(std::vector<glm::vec4>& Lines)
{
  int L = Lines.size();
  for(int i = 0; i<L;i++){
    Lines[i] = NDC * Lines[i];
  }
}


void A2::Reset()
{
  VT = mat4();
  VR = mat4();
  MR = mat4();
  MT = mat4();
  MS = mat4();
  VPS = mat4();
  VPS[0][0] = VPS[1][1] = 0.95f;
  VPT = vec4(0.0f);

  VCurrentX = VCurrentY = -0.95f;
  VStartX = VStartY = 0.95f;
  
  KeyFlag = MouseFlag =  0;
  FoVDeg = FVD;
  N = ON;
  F = OF;
  Changed = true;
  RecordStart = true;
}

void A2::MapToViewport()
{
  float width = ((VStartX-VCurrentX)>0)? VStartX-VCurrentX:VCurrentX-VStartX;
  float height = ((VStartY-VCurrentY)>0)? VStartY-VCurrentY:VCurrentY-VStartY;
  glm::vec3 Center = vec3((VCurrentX+VStartX)/2,(VCurrentY+VStartY)/2,0.0f);
  Center = Center-vec3(0.0f);
  VPT = vec4(-Center,0.0f);
  VPS = mat4();
  //Translate(VPT,Center);
  VPS[0][0]=(width/2);
  VPS[1][1] = (height/2);
  //VPS *= BaseMat;
}

vector<glm::vec4> A2::VPProcess(vector<glm::vec4>& dots)
{
  vector<glm::vec4> NEW;
  int l = dots.size();
  for(int i = 0;i<l;i++){
    NEW.emplace_back((VPS * dots[i]) - VPT);
  }
  return NEW;
}
// My new class
ObjData::ObjData(int VertexCount = 0):
	size(VertexCount),
	Center(vec3(0.0f))
{}
 
