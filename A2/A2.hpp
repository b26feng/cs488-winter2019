// Winter 2019

#pragma once

#include "cs488-framework/CS488Window.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include <glm/glm.hpp>

#include <vector>

// Set a global maximum number of vertices in order to pre-allocate VBO data
// in one shot, rather than reallocating each frame.
const GLsizei kMaxVertices = 1000;


// Convenience class for storing vertex data in CPU memory.
// Data should be copied over to GPU memory via VBO storage before rendering.
class VertexData {
public:
  VertexData();

  std::vector<glm::vec2> positions;
  std::vector<glm::vec3> colours;
  GLuint index;
  GLsizei numVertices;
};

class ObjData {
public:
  ObjData(int VertexCount);
  
  std::vector<glm::vec3> Vertices;
  const size_t size;
  const glm::vec3 Center;

};

class A2 : public CS488Window {
public:
  A2();
  virtual ~A2();

protected:
  virtual void init() override;
  virtual void appLogic() override;
  virtual void guiLogic() override;
  virtual void draw() override;
  virtual void cleanup() override;

  virtual bool cursorEnterWindowEvent(int entered) override;
  virtual bool mouseMoveEvent(double xPos, double yPos) override;
  virtual bool mouseButtonInputEvent(int button, int actions, int mods) override;
  virtual bool mouseScrollEvent(double xOffSet, double yOffSet) override;
  virtual bool windowResizeEvent(int width, int height) override;
  virtual bool keyInputEvent(int key, int action, int mods) override;

  void createShaderProgram();
  void enableVertexAttribIndices();
  void generateVertexBuffers();
  void mapVboDataToVertexAttributeLocation();
  void uploadVertexDataToVbos();

  void initLineData();

  void setLineColour(const glm::vec3 & colour);

  void drawLine (
		 const glm::vec2 & v0,
		 const glm::vec2 & v1
		 );

  // Pipeline functions
  void CalculateV();
  void CalculateM();
  void CalculateNDC();
  std::vector<glm::vec4> Clip(std::vector<glm::vec4>& Lines);
  bool ClipLine(std::vector<glm::vec4>& Lines, int OddIndex, char* Codes);
  glm::vec4 GetInter(glm::vec4& A,glm::vec4& B,char PlaneBit);
  char GetCode(glm::vec4& Point);
  void Project(std::vector<glm::vec4>& Lines);
  void NDCS(std::vector<glm::vec4>& Lines);

  //Control Functions
  
  void Translate(glm::mat4& BaseMat,glm::vec3& Dest);
  void RotateX(glm::mat4& BaseMat,float Rad);
  void RotateY(glm::mat4& BaseMat,float Rad);
  void RotateZ(glm::mat4& BaseMat,float Rad);
  glm::mat4 RotateArbitrary(float Rad,glm::vec4& Axis,glm::vec4& Origin);
  glm::mat4 Transpose(glm::mat4& M);
  
  void RotateX(glm::mat4& BaseMat,float Cos,float Sin);
  void RotateY(glm::mat4& BaseMat,float Cos,float Sin);
  void RotateZ(glm::mat4& BaseMat,float Cos,float Sin);
  void AlignAxis(glm::mat4& BaseMat,glm::vec4& XAxis);

  void MapToViewport();
  std::vector<glm::vec4> VPProcess(std::vector<glm::vec4>& dots);
  void Reset();
  
  //void Rotate(glm::mat4& BaseMat,float Deg);
  
  ShaderProgram m_shader;

  GLuint m_vao;            // Vertex Array Object
  GLuint m_vbo_positions;  // Vertex Buffer Object
  GLuint m_vbo_colours;    // Vertex Buffer Object

  ObjData TheCube;

  // world
  glm::vec3 WorldOrigin;
  // view
  glm::vec3 LookFrom;
  glm::vec3 LookAt;
  glm::vec3 Up;
  glm::vec3 Vx;
  glm::vec3 Vy;
  glm::vec3 Vz;
  glm::mat4 V;
  glm::mat4 M;

  // perspective
  float FoVDeg;
  float FoVRad;
  float ASPC;
  float N;
  float F;
  const float FVD;
  const float ON;
  const float OF;
  glm::mat4 NDC;
  std::vector<glm::vec4> Clipped;
  std::vector<glm::vec4> ClippedCoord;
  std::vector<glm::vec4> WorldClipped;
  // Control Flags
  // A,O,E,P,R,T,S,V
  char KeyFlag;
  // 00000 Left Middle Right
  char MouseFlag;
  bool Quit;
  glm::mat4 VT;
  glm::mat4 VR;
  glm::mat4 MT;
  glm::mat4 MR;
  glm::mat4 MS;
  glm::mat4 VPS;
  glm::vec4 VPT;
  bool Changed;

  double LastX;
  double LastY;
  float Distance;

  // view port
  bool RecordStart;
  double VStartX;
  double VStartY;
  double VCurrentX;
  double VCurrentY;
  

  VertexData m_vertexData;

  glm::vec3 m_currentLineColour;

};
