Update on Oct 11th, 2019.(After the course is finished)

I implemented a naive path tracer. Here are some demo pics.

## Reflection
![AA](/mirrow-8.png)

## Refraction
![AA](/glass1024.png)

## Object lighting and caustics.
![AA](/caustic.png)

![AA](/caustic2.png)

# Path Tracing With Direct Lighting

## Path Tracing With Direct Lighting Large Light Source
![AA](/simple2-PTDLLL.png)

## Path Tracing With Direct Lighting Small Light Source
![AA](/simple2-PTDLL.png)

# A5 Final Project:

## Anti Aliasing, Relfection, Depth of Field, 
![AA-DOF](/512SampleMir.png)

## Without Depth of Field
![AA](/anti-alias.png)

## Without Anti-Aliasing and Depth of Field
![AA](/non-aa-dof.png)

## Soft shadow
![AA](/softshadow.png)

## Without soft shadow
![AA](/non-softshadow.png)

## Infinit Reflection(almost)
![AA](/correct.png)

## Glossy Reflection with glossiness 10000
![AA](/box104.png)

## Glossy Reflection with glossiness 1000000
![AA](/box106.png)

## Glossy Reflection with glossiness 100000000
![AA](/box108.png)

## Glossy Reflection with glossiness 800000
![AA](/box805.png)

## Glossy Reflection with glossiness 800000, anti aliasing
![AA](/box805AA.png)


## Refraction,efraction index 2.3
![AA](/diamond.png)


## Glossy Refraction with glossiness 100000000, anti aliasing, refraction index 2.3
![AA](/diamond108aa.png)


## Glossy Refraction with glossiness 100000000, refraction index 2.3
![AA](/diamond108.png)


## Glossy Refraction with glossiness 10000000, anti aliasing, refraction index 2.3
![AA](/diamond107aa.png)


## Glossy Refraction with glossiness 10000000, , refraction index 2.3
![AA](/diamond107.png)


## Bump mapping with different lighting
![AA](/bump203l.png)
![AA](/bump203r.png)
![AA](/bump.gif)

## Ball in flat water with and without mirrow
![AA](flatwater.png)
![AA](in_flat_water.png)

## Perlin noise water surface, octave 2 3 5 8 1
![AA](octave235813.png)

## Perlin noise water surface, octave 1 2 3 5 8 13
![AA](octave1235813.png)

## Perlin noise water surface, octave 2 3 5 8 13 21 34
![AA](octave2358132134.png)

## Non-super sampling 
![AA](xtrans-demo.png)

## Xtrans super sampling Without Dispersion
![AA](nodispersion.png)

## Xtrans super sampling With Dispersion
![AA](weakdipersion.png)

## Xtrans super sampling with more obvious dispersion
![AA](obvious.png)

## Xtrans super sampling, fail to interpolate.
![AA](xtrans-fail.png)

KD Tree Mesh Acceleration:

I used KD tree for mesh rendering.

I created this Gundam Unicorn in A3 using 3ds Max and used Blender to convert it to triangles. The head consists of 677 triangles, the whole body is consist of 4296 triangles. 

It took 20 seconds to render the only the head without KD-tree. It took 12 seconds to render the whole model with KD-tree acceleration.

The scene layout is similar, rendering a perlin-noised water surface is more time consuming than a flat water surface.


## Gundam Unicorn Head
![AA](unicorn-phong.png)

## Gundam Unicorn Full
![AA](gundaminwater.png)

## Final Scene
![AA](final_scene.png)

## Why I don't render shades for transparent objects
![AA](blackwater.png)

# A3 Puppet:
Gundam unicorn 3ds Max model is in /A3/Assets


# CS488 Winter 2019 Project Code

---

## Dependencies
* OpenGL 3.2+
* GLFW
    * http://www.glfw.org/
* Lua
    * http://www.lua.org/
* Premake4
    * https://github.com/premake/premake-4.x/wiki
    * http://premake.github.io/download.html
* GLM
    * http://glm.g-truc.net/0.9.7/index.html
* ImGui
    * https://github.com/ocornut/imgui


---

## Building Projects
We use **premake4** as our cross-platform build system. First you will need to build all
the static libraries that the projects depend on. To build the libraries, open up a
terminal, and **cd** to the top level of the CS488 project directory and then run the
following:

    $ premake4 gmake
    $ make

This will build the following static libraries, and place them in the top level **lib**
folder of your cs488 project directory.
* libcs488-framework.a
* libglfw3.a
* libimgui.a

Next we can build a specific project.  To do this, **cd** into one of the project folders,
say **A0** for example, and run the following terminal commands in order to compile the A0 executable using all .cpp files in the A0 directory:

    $ cd A0/
    $ premake4 gmake
    $ make


----

## Windows
Sorry for all of the hardcore Microsoft fans out there.  We have not had time to test the build system on Windows yet. Currently our build steps work for OSX and Linux, but all the steps should be the same on Windows, except you will need different libraries to link against when building your project executables.  Some good news is that premake4 can output a Visual Studio .sln file by running:

    $ premake4 vs2013

 This should point you in the general direction.
