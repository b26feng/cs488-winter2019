
// Winter 2019

#pragma once

#include <glm/glm.hpp>

#include "Material.hpp"

class PhongMaterial : public Material {
public:
  PhongMaterial(const glm::vec3& kd, const glm::vec3& ks, double shininess);
  virtual ~PhongMaterial();
  virtual void GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t) const override;
  virtual glm::dvec3 GetDLColor(const glm::dvec4& Inter, const glm::dvec4& N, const SceneNode* Light, SceneNode* Root) const override;
  virtual glm::dvec3 GetColor() const override;
  virtual bool IsLight() override;
  // Return true if use the refrction intersection.
  virtual bool GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NextRay) override;
private:
  glm::dvec3 m_kd;
  glm::dvec3 m_ks;
  double m_shininess;


};
