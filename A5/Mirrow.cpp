#include "Mirrow.hpp"
#include "Random.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/transform.hpp>

static const double ZERO = 0.0001;
static const double PI = 3.14159265358979323846;

Mirrow::Mirrow(const glm::dvec3& ks,int rc,double anu, double shininess,double G):
  ks{ks},
  Shininess{shininess},
  Glossiness{G}
{
  ReflectCount = rc;
  RAttenu = anu;
  Type = MatType::Mirrow;
  tex = nullptr;
}

void Mirrow::GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t)const
{
  if(!Hit){
    double Dist = glm::length(Ray);
    glm::dvec4 SRay = Origin - glm::dvec4(L->position,1.0f);
    SRay = glm::reflect(SRay,-Normal);
    double DIF = -glm::dot(glm::normalize(SRay),glm::normalize(EyeRay));
    DIF = glm::pow(DIF,Shininess);
    //if(DIF > 1-0.3 && DIF < 1+0.3){
    /*if(DIF > 0){
      //color.x += DIF;
      //color.y += DIF;
      //color.z += DIF;
      //std::cout<<glm::to_string(L->colour)<<"|"<<glm::to_string(ks)<<std::endl;
      color.x += (DIF * ks.x * (L->colour.x));
      color.y += (DIF * ks.y * (L->colour.y));
      color.z += (DIF * ks.z * (L->colour.z));
      }*/
  }
}

glm::dvec3 Mirrow::GetColor() const{
  return ks;
}

void Mirrow::GetReflect(glm::dvec4& InRay,const glm::dvec4& Normal){
  Material::GetReflect(InRay,Normal);
  // using the cosine distribution given by the course
  if(Glossiness >= 0){
    double x1,x2,polar,azimuthal;
    glm::dmat4 PolarM,AzimuM;
    glm::dvec4 ImperfectRay;
    while(true){
      x1 = RandRange(0,1,10000);
      x2 = RandRange(0,1,10000);
      polar = glm::acos(glm::pow((1-x1),1.0/(Glossiness+1)));
      //polar = glm::pow(glm::acos(1-x1),1.0/(Glossiness+1));
      azimuthal = 2*PI*x2;
      //std::cout<<polar<<std::endl;
      PolarM = glm::rotate(glm::degrees(polar),glm::cross(glm::dvec3(Normal),glm::dvec3(InRay)));
      AzimuM = glm::rotate(glm::degrees(azimuthal),glm::dvec3(InRay));
      ImperfectRay = AzimuM * PolarM * InRay;

      // check if the ray goes below the surface
      if(glm::dot(glm::dvec3(ImperfectRay),glm::dvec3(Normal)) >= 0) break;
    }
    InRay = ImperfectRay;
  }
}

bool Mirrow::GetNextRay(const glm::dvec4& InRay, const glm::dvec4& Normal, glm::dvec4& NextRay){
  NextRay = InRay;
  GetReflect(NextRay,Normal);
  return false;
}
