// Winter 2019

#pragma once

#include <vector>
#include <iosfwd>
#include <string>

#include <glm/glm.hpp>

#include "KDTree.hpp"

// A polygonal mesh.
class Mesh : public Primitive {
public:
  Mesh( const std::string& fname );
  ~Mesh();
  virtual double GetIntersect(const glm::dvec4& Origin,const glm::dvec4&Ray,glm::dvec4& Normal) override;
  //virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
  virtual void UpdatePos(glm::dmat4& TMat) override;
  virtual void Print() const override;
  virtual void GetCenterSize(glm::dvec4& CandS) override;
private:
  std::vector<glm::dvec3> m_vertices;
  std::vector<Triangle> m_faces;
  glm::dvec4* Normals;
  glm::dvec3 Center;
  double Radius;
  KD* mKDRoot;
  KD* S;
  KD* L;
  friend std::ostream& operator<<(std::ostream& out, const Mesh& mesh);\
};
