-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp2 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp1 = gr.bump(2,0.3)
perlin2d = gr.perlin2(1,0.000001)

per1 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
per1:set_texture(perlin2d)
bmp2:set_texture(bmp1)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
sora = gr.material({0.34375, 0.6953125, 0.859375}, {1.0, 1.0, 1.0}, 25)
red = gr.material({0.793,0.25,0.2578}, {0.8, 0.8, 0.8}, 25)
Red = gr.material({1,0.25,0.2578},{1.0,1.0,1.0},25)
Ruri = gr.material({0,0.359375,0.68359375},{1.0,1.0,1.0},25)
mat6 = gr.material({0.7,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
blue = gr.material({0.6445,0.8672,0.8906}, {1, 1, 1}, 25)
ginnezumi = gr.material({0.5664065,0.59375,0.6210935},{1,1,1},25)
mat8 = gr.material({1.0,1.0,1.0}, {1.0, 1.0, 1.0}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},30,1.0,25,-1)

mir3 = gr.mirrow({1.0,1.0,1.0},240,1.0,25,-1)
mir1:set_texture(perlin2d)
mir2 = gr.mirrow({1.0,1.0,1.0},20,1.0,25,10000000)
diamond = gr.glass({1.0,1.0,1.0},40,1.0,25,-1,2.42)
water1 = gr.glass({1.0,1.0,1.0},14,0.8,25,-1,1.33)
--water1:set_texture(perlin2d)
glass3 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3:set_texture(bmp1)
glass2 = gr.glass({1.0,1.0,1.0},20,1.0,25,100000000,2.42)

scene = gr.node( 'scene' )
--scene:translate(0, 0, -1300)



s0 = gr.nh_box('s0', {-450, -490,-650}, 300)
scene:add_child(s0)
s0:set_material(mat3)

s1 = gr.nh_box('s1', {-300, -50,-550}, 100)
scene:add_child(s1)
s1:set_material(ginnezumi)

s2 = gr.nh_box('s2', {-500,300,-100}, 200)
scene:add_child(s2)
s2:set_material(ginnezumi)

s3 = gr.nh_box('s3', {-100, 170,-30}, 100)
scene:add_child(s3)
s3:set_material(ginnezumi)

s4 = gr.nh_box('s4', {220, 220,-37}, 160)
scene:add_child(s4)
s4:set_material(ginnezumi)

s5 = gr.nh_box('s5', {260, -28,-94}, 120)
scene:add_child(s5)
s5:set_material(ginnezumi)

s6 = gr.nh_box('s6', {-100, 6,-162}, 180)
scene:add_child(s6)
s6:set_material(ginnezumi)

s7 = gr.nh_box('s7', {280,-460,160}, 80)
scene:add_child(s7)
s7:set_material(ginnezumi)

s8 = gr.nh_box('s8', {-30, -430,-230}, 60)
scene:add_child(s8)
s8:set_material(ginnezumi)


--white_light = gr.alight({0,400, -750}, {0.8,0.8,0.8}, {1, 0, 0},30,30,30)
white_light = gr.light({0,370, 430}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

--gr.render(scene, 'perf-c.png',256,256,
gr.render(scene, 'perf-c.png',2048,2048,
	  {0, 0, 1300}, {0, 0, -1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light})
