-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp2 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp1 = gr.bump(2,0.3)
perlin2d = gr.perlin2(1,0.000001)

per1 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
fres1 = gr.fres({0.8,0.8,0.8},10,1.0,25,-1,1.33)

fres2 = gr.fres({0.34375, 0.6953125, 0.859375},10,1.0,25,-1,2.42)
fres3 = gr.fres({0.8,0.8,0.8},10,1.0,25,1000000,1.33)

per1:set_texture(perlin2d)
bmp2:set_texture(bmp1)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
sora = gr.material({0.34375, 0.6953125, 0.859375}, {1.0, 1.0, 1.0}, 25)
red = gr.material({0.793,0.25,0.2578}, {0.8, 0.8, 0.8}, 25)
Red = gr.material({1,0.25,0.2578},{1.0,1.0,1.0},25)
Ruri = gr.material({0,0.359375,0.68359375},{1.0,1.0,1.0},25)
mat6 = gr.material({0.7,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
blue = gr.material({0.6445,0.8672,0.8906}, {1, 1, 1}, 25)
ginnezumi = gr.material({0.5664065,0.59375,0.6210935},{1,1,1},25)
mat8 = gr.material({1.0,1.0,1.0}, {1.0, 1.0, 1.0}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},30,1.0,25,-1)

mir3 = gr.mirrow({1.0,1.0,1.0},240,1.0,25,-1)
mir1:set_texture(perlin2d)
mir2 = gr.mirrow({1.0,1.0,1.0},4,1.0,25,10000000)
diamond = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
water1 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,1.33)
water1:set_texture(perlin2d)
water2 = gr.glass({1.0,1.0,1.0},12,1,25,-1,1.33)
glass3 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3:set_texture(bmp1)
glass2 = gr.glass({1.0,1.0,1.0},12,1.0,25,100000000,2.3)

scene = gr.node( 'scene' )
--scene:translate(0, 0, -1300)



s2 = gr.nh_sphere('s2', {-25, 400,-700}, 150)
--scene:add_child(s2)
s2:set_material(water2)

s3 = gr.nh_sphere('s3', {-160, -300,0}, 100)
scene:add_child(s3)
s3:set_material(fres3)


p1 = gr.plane("p1",{0,0,0},{1,0,0},{0,1,0},5800)
--p1:rotate('X',60)
p1:translate(0,0,-1400)
scene:add_child(p1)
p1:set_material(mir3)

p2 = gr.plane("p2",{0,0,0},{0,1,0},{0,0,1},5800)
p2:rotate('Y',-30)
p2:translate(-1100,0,0)
scene:add_child(p2)
p2:set_material(mir3)

p25 = gr.plane("p25",{0,0,0},{0,1,0},{0,0,1},5800)
p25:rotate('Y',30)
p25:translate(-1100,0,0)
scene:add_child(p25)
p25:set_material(Red)


p3 = gr.plane("p3",{0,0,0},{0,0,1},{0,1,0},5800)
p3:rotate('Y',30)
p3:translate(1100,0,0)
scene:add_child(p3)
p3:set_material(mir3)

p35 = gr.plane("p35",{0,0,0},{0,0,1},{0,1,0},5800)
p35:rotate('Y',-30)
p35:translate(1100,0,0)
scene:add_child(p35)
p35:set_material(mat1)



p4 = gr.plane("p4",{0,0,0},{1,0,0},{0,0,1},5800)
--p4:rotate('Z',20)
p4:translate(0,800,0)
scene:add_child(p4)
p4:set_material(sora)

p45 = gr.plane("p45",{0,0,0},{0,0,1},{1,0,0},5800)
--p45:rotate('Z',60)
p45:translate(0,-250,0)
scene:add_child(p45)
p45:set_material(water1)

p5 = gr.plane("p5",{0,0,0},{0,0,1},{1,0,0},5800)
--p5:rotate('Z',60)
p5:translate(0,-1000,0)
scene:add_child(p5)
p5:set_material(Ruri)


p6 = gr.plane("p6",{0,0,0},{0,1,0},{1,0,0},5800)
--p6:rotate('X',-60)
p6:translate(0,0,3400)
scene:add_child(p6)
p6:set_material(sora)


-- A small stellated dodecahedron.

steldodec = gr.mesh( 'dodec', 'smstdodeca.obj' )
--steldodec:set_material(mir3)
steldodec:set_material(diamond)
steldodec:translate(300,100,200)
--scene:add_child(steldodec)

head = gr.mesh( 'head', 'head.obj' )
--head:set_material(mir3)
head:set_material(mat1)
head:rotate('y',90)
head:translate(0,0,200)
--scene:add_child(head)

chest = gr.mesh( 'chest', 'chest.obj' )
--chest:set_material(mir3)
chest:set_material(mat1)
chest:scale(0.7,0.7,0.7)
chest:rotate('y',90)
chest:translate(0,0,200)
--scene:add_child(chest)

torso = gr.mesh( 'torso', 'torso.obj' )
--torso:set_material(mir3)
torso:set_material(mat1)
torso:scale(0.7,0.5,0.7)
--torso:rotate('y',90)
torso:translate(20,-150,200)
--scene:add_child(torso)

left = gr.mesh( 'left', 'arm.obj' )
--arm:set_material(mir3)
left:set_material(mat1)
--left:scale(0.7,0.5,0.7)
left:rotate('y',90)
--left:translate(200,-180,250)
--scene:add_child(left)


gundam = gr.mesh( 'gundam', 'gundam.obj' )
--gundam:set_material(mir3)
gundam:set_material(fres1)
gundam:scale(12,12,12)
gundam:rotate('y',70)
gundam:translate(0,0,-400)
scene:add_child(gundam)



white_light = gr.alight({0,200, 980}, {0.8,0.8,0.8}, {1, 0, 0},30,30,0)
--white_light = gr.light({0,200, 980}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

gr.render(scene, 'rx0.png',2048,2048,
--gr.render(scene, 'unicorn.png',2048,2048,
	  {0, 200, 980}, {0, -0.087156, -0.996195}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light})
