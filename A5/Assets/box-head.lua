-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp2 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp1 = gr.bump(2,0.3)
perlin2d = gr.perlin2(1,0.000001)

per1 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
fres1 = gr.fres({0.8,0.8,0.8},10,1.0,25,-1,1.33)

fres2 = gr.fres({0.34375, 0.6953125, 0.859375},10,1.0,25,-1,2.42)
fres3 = gr.fres({0.8,0.8,0.8},10,1.0,25,1000000,1.33)

per1:set_texture(perlin2d)
bmp2:set_texture(bmp1)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
sora = gr.material({0.34375, 0.6953125, 0.859375}, {1.0, 1.0, 1.0}, 25)
red = gr.material({0.793,0.25,0.2578}, {0.8, 0.8, 0.8}, 25)
Red = gr.material({1,0.25,0.2578},{1.0,1.0,1.0},25)
Ruri = gr.material({0,0.359375,0.68359375},{1.0,1.0,1.0},25)
mat6 = gr.material({0.7,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
blue = gr.material({0.6445,0.8672,0.8906}, {1, 1, 1}, 25)
ginnezumi = gr.material({0.5664065,0.59375,0.6210935},{1,1,1},25)
mat8 = gr.material({1.0,1.0,1.0}, {1.0, 1.0, 1.0}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},30,1.0,25,-1)

mir3 = gr.mirrow({1.0,1.0,1.0},240,1.0,25,-1)
mir1:set_texture(perlin2d)
mir2 = gr.mirrow({1.0,1.0,1.0},4,1.0,25,10000000)
diamond = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
water1 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,1.33)
water1:set_texture(perlin2d)
water2 = gr.glass({1.0,1.0,1.0},12,1,25,-1,1.33)
glass3 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3:set_texture(bmp1)
glass2 = gr.glass({1.0,1.0,1.0},12,1.0,25,100000000,2.3)

scene = gr.node( 'scene' )
--scene:translate(0, 0, -1300)



s3 = gr.nh_sphere('s3', {-160, 0,0}, 100)
scene:add_child(s3)
s3:set_material(fres2)


p1 = gr.plane("p1",{0,0,0},{1,0,0},{0,1,0},5800)
--p1:rotate('X',60)
p1:translate(0,0,-400)
scene:add_child(p1)
p1:set_material(mat1)

p2 = gr.plane("p2",{0,0,0},{0,1,0},{0,0,1},5800)
p2:translate(-400,0,0)
scene:add_child(p2)
p2:set_material(Red)


p3 = gr.plane("p3",{0,0,0},{0,0,1},{0,1,0},5800)
--p3:rotate('Y',30)
p3:translate(400,0,0)
scene:add_child(p3)
p3:set_material(sora)


p4 = gr.plane("p4",{0,0,0},{1,0,0},{0,0,1},5800)
--p4:rotate('Z',20)
p4:translate(0,400,0)
scene:add_child(p4)
p4:set_material(mat1)

p45 = gr.plane("p45",{0,0,0},{0,0,1},{1,0,0},5800)
--p45:rotate('Z',60)
p45:translate(0,-400,0)
--scene:add_child(p45)
p45:set_material(mat1)



head = gr.mesh( 'head', 'head.obj' )
--head:set_material(mat1)
head:set_material(fres1)
head:rotate('y',40)
head:translate(0,0,600)
--scene:add_child(head)


gundam = gr.mesh( 'gundam', 'gundam.obj' )
--gundam:set_material(mir3)
gundam:set_material(fres1)
--gundam:set_material(mat1)
gundam:scale(8,8,8)
gundam:rotate('y',70)
gundam:translate(0,0,0)
scene:add_child(gundam)



--white_light = gr.alight({0,200, 980}, {0.8,0.8,0.8}, {1, 0, 0},30,30,0)
white_light = gr.light({0,200, 980}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

gr.render(scene, 'cornell.png',2048,2048,
--gr.render(scene, 'cornell.png',256,256,
	  {0, 0, 980}, {0, 0,-1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light})
