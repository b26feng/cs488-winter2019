-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp2 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp1 = gr.bump(2,0.3)
bmp2:set_texture(bmp1)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
sora = gr.material({0.34375, 0.6953125, 0.859375}, {1.0, 1.0, 1.0}, 25)
red = gr.material({0.793,0.25,0.2578}, {0.8, 0.8, 0.8}, 25)
Red = gr.material({1,0.25,0.2578},{1.0,1.0,1.0},25)
mat6 = gr.material({0.7,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
blue = gr.material({0.6445,0.8672,0.8906}, {1, 1, 1}, 25)
ginnezumi = gr.material({0.5664065,0.59375,0.6210935},{1,1,1},25)
mat8 = gr.material({1.0,1.0,1.0}, {1.0, 1.0, 1.0}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},480,1.0,25,-1)

mir3 = gr.mirrow({1.0,1.0,1.0},240,1.0,25,-1)
mir1:set_texture(bmp1)
mir2 = gr.mirrow({1.0,1.0,1.0},4,1.0,25,10000000)
glass1 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3:set_texture(bmp1)
glass2 = gr.glass({1.0,1.0,1.0},12,1.0,25,100000000,2.3)

scene = gr.node( 'scene' )
--scene:translate(0, 0, -1300)



s2 = gr.nh_sphere('s2', {0, -340,-400}, 150)
scene:add_child(s2)
s2:set_material(bmp2)


s6 = gr.plane("s6",{0,0,0},{1,0,0},{0,1,0},1800)
--s6:rotate('X',60)
s6:translate(0,0,-900)
scene:add_child(s6)
s6:set_material(Red)

p2 = gr.plane("p2",{0,0,0},{0,1,0},{0,0,1},1800)
--p2:rotate('Y',-10)
p2:translate(-501,0,0)
scene:add_child(p2)
p2:set_material(sora)

p3 = gr.plane("p3",{0,0,0},{0,0,1},{0,1,0},1800)
--p3:rotate('Z',60)
p3:translate(500,0,0)
scene:add_child(p3)
p3:set_material(blue)


p4 = gr.plane("p4",{0,0,0},{1,0,0},{0,0,1},1800)
--p4:rotate('Z',60)
p4:translate(0,500,0)
scene:add_child(p4)
p4:set_material(mat8)

p5 = gr.plane("p5",{0,0,0},{0,0,1},{1,0,0},1800)
--p5:rotate('Z',60)
p5:translate(0,-500,0)
scene:add_child(p5)
p5:set_material(mat8)


p6 = gr.plane("p6",{0,0,0},{0,1,0},{1,0,0},1800)
--p6:rotate('X',60)
p6:translate(0,0,1600)
scene:add_child(p6)
p6:set_material(Red)


-- A small stellated dodecahedron.

steldodec = gr.mesh( 'dodec', 'smstdodeca.obj' )
steldodec:set_material(glass1)
--steldodec:set_material(glass1)
steldodec:translate(200,-150,300)
--scene:add_child(steldodec)

--white_light = gr.alight({0,400, -750}, {0.8,0.8,0.8}, {1, 0, 0},30,30,30)
white_light = gr.light({60,370, 430}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

gr.render(scene, 'ball.png',2048,2048,
	  {0, 0, 1300}, {0, 0, -1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light})
