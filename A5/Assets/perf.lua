-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp2 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
bmp1 = gr.bump(2,0.3)
perlin2d = gr.perlin2(1,0.000001)

per1 =  gr.material({0.8, 0.8, 0.8}, {0.8, 0.8, 0.8}, 25)
per1:set_texture(perlin2d)
bmp2:set_texture(bmp1)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
sora = gr.material({0.34375, 0.6953125, 0.859375}, {1.0, 1.0, 1.0}, 25)
red = gr.material({0.793,0.25,0.2578}, {0.8, 0.8, 0.8}, 25)
Red = gr.material({1,0.25,0.2578},{1.0,1.0,1.0},25)
Ruri = gr.material({0,0.359375,0.68359375},{1.0,1.0,1.0},25)
mat6 = gr.material({0.7,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
blue = gr.material({0.6445,0.8672,0.8906}, {1, 1, 1}, 25)
ginnezumi = gr.material({0.5664065,0.59375,0.6210935},{1,1,1},25)
mat8 = gr.material({1.0,1.0,1.0}, {1.0, 1.0, 1.0}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},30,1.0,25,-1)

mir3 = gr.mirrow({1.0,1.0,1.0},240,1.0,25,-1)
mir1:set_texture(perlin2d)
mir2 = gr.mirrow({1.0,1.0,1.0},20,1.0,25,10000000)
diamond = gr.glass({1.0,1.0,1.0},40,1.0,25,-1,2.42)
water1 = gr.glass({1.0,1.0,1.0},14,0.8,25,-1,1.33)
--water1:set_texture(perlin2d)
glass3 = gr.glass({1.0,1.0,1.0},12,1.0,25,-1,2.3)
glass3:set_texture(bmp1)
glass2 = gr.glass({1.0,1.0,1.0},20,1.0,25,100000000,2.42)

scene = gr.node( 'scene' )
--scene:translate(0, 0, -1300)



s0 = gr.nh_sphere('s0', {-300, -340,-500}, 150)
scene:add_child(s0)
s0:set_material(mat3)

s1 = gr.nh_sphere('s1', {-250, 0,-500}, 50)
scene:add_child(s1)
s1:set_material(ginnezumi)

s2 = gr.nh_sphere('s2', {-400,400,0}, 100)
scene:add_child(s2)
s2:set_material(ginnezumi)

s3 = gr.nh_sphere('s3', {-50, 220,20}, 50)
scene:add_child(s3)
s3:set_material(ginnezumi)

s4 = gr.nh_sphere('s4', {300, 300,43}, 80)
scene:add_child(s4)
s4:set_material(ginnezumi)

s5 = gr.nh_sphere('s5', {320, 32,-34}, 60)
scene:add_child(s5)
s5:set_material(ginnezumi)

s6 = gr.nh_sphere('s6', {-10, 96,-72}, 90)
scene:add_child(s6)
s6:set_material(ginnezumi)

s7 = gr.nh_sphere('s7', {320,-420,200}, 40)
scene:add_child(s7)
s7:set_material(ginnezumi)

s8 = gr.nh_sphere('s8', {0, -400,-200}, 30)
scene:add_child(s8)
s8:set_material(ginnezumi)


--white_light = gr.alight({0,400, -750}, {0.8,0.8,0.8}, {1, 0, 0},30,30,30)
white_light = gr.light({0,370, 430}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

--gr.render(scene, 'perf.png',256,256,
gr.render(scene, 'perf.png',2048,2048,
	  {0, 0, 1300}, {0, 0, -1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light})
