-- A simple scene with some miscellaneous geometry.
mat1 = gr.material({0.9, 0.9, 0.9}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({0.95, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)

mat2 = gr.material({0.9, 0.2, 0.2}, {0.5, 0.7, 0.5}, 25)
mat4 = gr.material({0.2, 0.7, 0.5}, {0.5, 0.7, 0.5}, 25)

mat5 = gr.material({0.1,0.5,0.9}, {0.5, 0.7, 0.5}, 25)

mat6 = gr.material({0.9,0.4,0.4}, {0.5, 0.7, 0.5}, 25)

light1 = gr.material({6,6,6}, {0.5, 0.7, 0.5}, 10)
light2 = gr.material({1,1,1}, {0.5, 0.7, 0.5}, 10)


mir1 = gr.mirrow({1.0,0.7,0.2},30,0.7,25,-1)

glass1 = gr.glass({1,1,1},12,1.0,25,-1,1.66)

scene_root = gr.node('root')

p1 = gr.plane("p1",{0,0,0},{0,0,1},{1,0,0},1600)
p1:translate(0,-400,-800)
scene_root:add_child(p1)
p1:set_material(mat1)

p2 = gr.plane("p2",{0,0,0},{0,1,0},{0,0,1},1600)
p2:translate(-400,0,-800)
scene_root:add_child(p2)
p2:set_material(mat6)

p3 = gr.plane("p3",{0,0,0},{1,0,0},{0,1,0},1600)
p3:translate(0,0,-800)
scene_root:add_child(p3)
p3:set_material(mat1)

p4 = gr.plane("p4",{0,0,0},{0,0,1},{0,1,0},1600)
p4:translate(400,0,-800)
scene_root:add_child(p4)
p4:set_material(mat4)

p5 = gr.plane("p5",{0,0,0},{1,0,0},{0,0,1},1600)
p5:translate(0,400,-800)
scene_root:add_child(p5)
p5:set_material(mat1)

p6 = gr.plane("p6",{0,0,0},{0,1,0},{1,0,0},1600)
p6:translate(0,0,800)
scene_root:add_child(p6)
p6:set_material(mat1)

L1 = gr.plane("L1",{0,0,0},{1,0,0},{0,0,1},300)
L1:translate(0,399.5,-300)
scene_root:add_child(L1)
L1:set_material(light2)

s2 = gr.nh_sphere('s2', {150, -250, -300}, 150)
scene_root:add_child(s2)
--s2:set_material(mat3)
--s2:set_material(mir1)
s2:set_material(glass1)


s9 = gr.nh_sphere('s9', {-200, -250, -600}, 150)
scene_root:add_child(s9)
--s9:set_material(mat3)
s9:set_material(mir1)
--s9:set_material(glass1)

--s3 = gr.nh_sphere('s3', {180,-50,50}, 50)
--scene_root:add_child(s3)
--s3:set_material(mat2)

--s4 = gr.nh_sphere('s4', {-100,-30,160}, 70)
--scene_root:add_child(s4)
--s4:set_material(mat4)

--s5 = gr.nh_sphere('s5', {240,220,0}, 60)
--scene_root:add_child(s5)
--s5:set_material(light1)

white_light = gr.light({-100.0, 150.0, 400.0}, {0.9, 0.9, 0.9}, {1, 0, 0})
magenta_light = gr.light({400.0, 100.0, 150.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

gr.render(scene_root, 'simple2-PTDLLL.png',512,512,
--gr.render(scene_root, 'simple2.png', 1024,1024,
	  {0, 0, 800}, {0, 0, -1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light, magenta_light})
