-- A simple scene with some miscellaneous geometry.
-- This file is very similar to nonhier.lua, but interposes
-- an additional transformation on the root node.  
-- The translation moves the scene, and the position of the camera
-- and lights have been modified accordingly.

mat1 = gr.material({0.7, 0.7, 0.7}, {0.5, 0.5, 0.5}, 25)
mat2 = gr.material({0.5, 0.5, 0.5}, {0.5, 0.7, 0.5}, 25)
mat3 = gr.material({1.0, 0.6, 0.1}, {0.5, 0.7, 0.5}, 25)
mat4 = gr.material({0.7, 0.6, 1.0}, {0.5, 0.4, 0.8}, 25)
mat5 = gr.material({0.7,0.0,0.0}, {0.5, 0.5, 0.5}, 25)
mat6 = gr.material({0.4,0.6,0.1}, {0.5, 0.5, 0.5}, 25)
mat7 = gr.material({0.0,0.0,0.7}, {0.5, 0.5, 0.5}, 25)
mat8 = gr.material({1,1,1}, {0.5, 0.5, 0.5}, 25)
mir1 = gr.mirrow({1.0,1.0,1.0},4,1.0,25)

scene = gr.node( 'scene' )
scene:translate(0, 0, -1300)

s1 = gr.nh_sphere('s1', {0, 0, -400}, 100)
scene:add_child(s1)
s1:set_material(mat1)

s2 = gr.nh_sphere('s2', {200, 50, -100}, 150)
scene:add_child(s2)
s2:set_material(mir1)

s3 = gr.nh_sphere('s3', {0, -1200, -500}, 1000)
scene:add_child(s3)
s3:set_material(mat2)

b1 = gr.nh_box('b1', {-200, -125, 0}, 100)
scene:add_child(b1)
b1:set_material(mat4)

s4 = gr.nh_sphere('s4', {-100, 25, -300}, 50)
scene:add_child(s4)
s4:set_material(mat3)

s5 = gr.nh_sphere('s5', {0, 100, -250}, 25)
scene:add_child(s5)
s5:set_material(mat1)

s6 = gr.plane("s6",{0,0,0},{1,0,0},{0,1,0},1800)
--s6:rotate('X',60)
s6:translate(0,0,-900)
scene:add_child(s6)
s6:set_material(mat6)

p2 = gr.plane("p2",{0,0,0},{0,1,0},{0,0,1},1800)
p2:rotate('Y',-10)
p2:translate(-300,0,0)
scene:add_child(p2)
p2:set_material(mir1)

p3 = gr.plane("p3",{0,0,0},{0,0,1},{0,1,0},1800)
--p3:rotate('Z',60)
p3:translate(500,0,0)
scene:add_child(p3)
p3:set_material(mat7)


p4 = gr.plane("p4",{0,0,0},{1,0,0},{0,0,1},1800)
--p4:rotate('Z',60)
p4:translate(0,500,0)
scene:add_child(p4)
p4:set_material(mat8)



b2 = gr.nh_box("b2",{-100,-225,400},100)
--s7:rotate('Z',-20)
--b2:translate(0,0,1900)
scene:add_child(b2)
b2:set_material(mat3)

-- A small stellated dodecahedron.

steldodec = gr.mesh( 'dodec', 'smstdodeca.obj' )
steldodec:set_material(mat3)
scene:add_child(steldodec)

white_light = gr.light({-100,50, 500.0}, {0.8,0.8,0.8}, {1, 0, 0})
magenta_light = gr.light({200.0, 100.0, -400.0}, {0.7, 0.0, 0.7}, {1, 0, 0})

gr.render(scene, 'mirrow_test.png',1024,1024,
	  {0, 0, 0}, {0, 0, -1}, {0, 1, 0}, 50,
	  {0.3, 0.3, 0.3}, {white_light,magenta_light})
