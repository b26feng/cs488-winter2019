#include "Perlin.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

Perlin::Perlin(const int size,const double Epsilon):
  Texture(size,Epsilon)
{
  Type == TexType::Perlin;
}

void Perlin::PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal)
{
  glm::dvec4 Intersection = NewNormal;
  Intersection = glm::dvec4(PCenter-glm::dvec3(Intersection),0);
  Intersection.x += PR;
  Intersection.z += PR;
  Intersection = (Size/(2*PR)) * Intersection;
  
  //std::cout<<glm::to_string(PCenter)<<std::endl;
  glm::dvec4 U,D,L,R,Holder;
  /*
  U = glm::dvec4(Intersection.x,Intersection.z-epsilon,Intersection.y,Intersection.w);
  D = glm::dvec4(Intersection.x,Intersection.z+epsilon,Intersection.y,Intersection.w);
  L = glm::dvec4(Intersection.x-epsilon,Intersection.z,Intersection.y,Intersection.w);
  R = glm::dvec4(Intersection.x+epsilon,Intersection.z,Intersection.y,Intersection.w);*/
  U = glm::dvec4(Intersection.x,Intersection.z-epsilon,0,0);
  D = glm::dvec4(Intersection.x,Intersection.z+epsilon,0,0);
  L = glm::dvec4(Intersection.x-epsilon,Intersection.z,0,0);
  R = glm::dvec4(Intersection.x+epsilon,Intersection.z,0,0);
  
  double up,down,left,right,eff;
  up = down = left = right = 0;
  // fib octaves
  
  int octaves[7] = {0,1,2,3,5,8,13};
  for(int i = 1;i<7;i++){
    eff = (double)(1<<octaves[i]);
    Holder = eff * U;
    up += NoiseFunc->noise(U)/eff;
    
    Holder = eff * D;
    down += NoiseFunc->noise(D)/eff;
    Holder = eff * L;
    left += NoiseFunc->noise(L)/eff;
    Holder = eff * R;
    right += NoiseFunc->noise(R)/eff;
    }
  //std::cout<<up<<","<<down<<std::endl;
  

  double tangent1 = (up-down)/(2*epsilon);
  double tangent2 = (left-right)/(2*epsilon);
  glm::dmat4 R1,R2;
  double deg1,deg2;
  deg1 = glm::degrees(glm::atan(tangent1));
  // deg1 /= 6;
  deg2 = glm::degrees(glm::atan(tangent2));
  //deg2 /= 6;
  R1 = glm::rotate(deg1,glm::dvec3(0,0,1));
  R2 = glm::rotate(deg2,glm::dvec3(1,0,0));
  /*
  if(deg1 >= 90 || deg1 <= -90 || deg2 >= 90 || deg2 <= -90){
    std::cout<<deg1<<","<<deg2<<std::endl;
  }*/
  
  NewNormal = R2 * R1 * Normal;
}

Perlin::~Perlin(){
  delete NoiseFunc;
}
