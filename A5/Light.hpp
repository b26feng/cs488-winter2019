// Winter 2019

#pragma once

#include <iosfwd>

#include <glm/glm.hpp>

// Represents a simple point light.
struct Light {
  Light();
  
  glm::dvec3 colour;
  glm::dvec3 position;
  double falloff[3];
  int xrange;
  int yrange;
  int zrange;
};

std::ostream& operator<<(std::ostream& out, const Light& l);
