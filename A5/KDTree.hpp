#pragma once

#include "Primitive.hpp"
#include "SAH.hpp"

#include <glm/glm.hpp>
#include <vector>

class Bounding{
protected:
  glm::dvec3 Center;
  double Radius;
public:
  Bounding(const glm::dvec3& center,const double radius);
  bool Hit(const glm::dvec4& Origin,const glm::dvec4& Ray) const;
};

class KD{
protected:
  //Triangle* TRI;
  KD* smaller;
  KD* larger;
  Primitive* mBound;
  Primitive** mCompact;
  size_t* mSkipList;
  size_t mSize;
  size_t mIndex;
  //void GetMinMax(glm::dvec3& min,glm::dvec3& max,Triangle** Ts,size_t start,size_t end,const std::vector<glm::dvec3>& vecs);
public:
  KD(KD* S = nullptr, KD* L = nullptr); 
  virtual ~KD();
  void AddSmall(KD* s);
  void AddLarge(KD* l);
  void SetBound(Primitive* bound);
  //void SetBound(const::glm::dvec3& C,const double R);
  void SetTRI(Triangle* T,const std::vector<glm::dvec3>& vecs);
  void SetTRI(SAHObject* SH);
  double GetIntersect(const glm::dvec4& Origin,const glm::dvec4& Ray,const std::vector<glm::dvec3>& vecs,Tri** TR);
  double CompactInter(const glm::dvec4& Origin,const glm::dvec4& Ray,const std::vector<glm::dvec3>& vecs,Tri** TR);
  size_t GetSizeAndIndex(size_t* Index);
  void FillInCompact(Primitive** Compact,size_t* SkipList);
  void MakeCompact();
  
  int Size;
};

KD* MakeTree(Triangle** Ts, size_t count,size_t start, size_t end,const std::vector<glm::dvec3>& vecs,const char idx);

KD* MakeTreeSmart(Triangle** Ts, size_t count,size_t start, size_t end,const std::vector<glm::dvec3>& vecs,bool is_root = false);

KD* MakeSAH(const SAHList& src, bool is_root = false);
