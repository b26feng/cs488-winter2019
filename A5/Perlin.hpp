#pragma once

#include "Texture.hpp"

class Perlin: public Texture{
public:
  Perlin(const int size,const double Epsilon);
  ~Perlin();
  virtual void PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal) override;
};

