#pragma once
#include "Mirrow.hpp"

class Transparent: public Mirrow{
protected:
  const double RefrIndex;
  const double RRate;
  const double GRate;
  const double BRate;
  const double Base;
  double LimitAngle;
  double Fres(double n1,double n2, double theta1, double theta2) const;
  
  double AdvFres(double n1,double n2, double theta1) const;
public:
  Transparent(const glm::dvec3& ks,int rc,double anu,double shininess,double G,double RI);

  virtual void GetReflect(glm::dvec4& InRay,const glm::dvec4& Normal) override;

  virtual void PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal) override;
  
  // InRay uses homogeneeous coordinate to indicate whether ray going in or out
  virtual double FresnellRate(const glm::dvec4& InRay, const glm::dvec4& Normal,glm::dvec4& Refraction,char RayColor) override;

  // Return true if use the refraction intersection.
  virtual bool GetNextRay(const glm::dvec4& InRay, const glm::dvec4& Normal, glm::dvec4& NextRay) override;
};
