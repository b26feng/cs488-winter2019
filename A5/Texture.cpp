#include "Texture.hpp"

Texture::Texture(const int size,const double Epsilon):
  Size(size),
  epsilon(Epsilon)
{
  Type = TexType::Texture;
  NoiseFunc = nullptr;
}

void Texture::PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal)
{
  NewNormal = Normal;
}

void Texture::SetData(const glm::dvec3& Center,const double radius)
{
  PCenter = Center;
  PR = radius;
}

Texture::~Texture(){
  if(NoiseFunc!=nullptr) delete NoiseFunc;
}
