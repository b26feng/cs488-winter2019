// Winter 2019

#include "Primitive.hpp"
#include "polyroots.hpp"
#include "Constant.hpp"
#include "Triangle.hpp"
#include "Config.hpp"
#include "Random.hpp"
#include "SAH.hpp"

#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
#include <cmath>

static const int Recs[24] = {0,1,2,3,5,1,0,4,5,4,7,6,6,7,3,2,6,2,1,5,0,3,7,4};
//static const double PI = 3.14159265358979323846;
//static const double ZERO = 0.00001;
Primitive::Primitive():
  TMat{glm::dmat4()}
{
  Bound = nullptr;
}

Primitive::~Primitive(){
  //if(Bound!=nullptr) delete Bound;
}

void Primitive::Print() const
{
  std::cout<<"Empty"<<std::endl;
}


double Primitive::GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal){
  return 0;
}

//void Primitive::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
void Primitive::UpdatePos(glm::dmat4& TMat){
  
  this->TMat = TMat;
  //this->RMat = glm::inverse(RMat);
  //Trans = trans;
  //Inv = invtrans;
  //SMat = glm::inverse(RMat) * glm::inverse(TMat) * trans;
  return;
}

void Primitive::GetCenterSize(glm::dvec4& CandS){
  CandS = glm::dvec4(0.0);
}

void Primitive::GetRandomPoint(glm::dvec4& RandomPoint,glm::dvec4& Normal){
  RandomPoint = glm::dvec4(0);
  Normal = glm::dvec4(0);
}


NonhierSphere::NonhierSphere(const glm::dvec3& pos, double radius)
  : m_pos(pos), m_radius(radius)
{
  Type = PType::NSphere;
}

void NonhierSphere::Print() const{
  std::cout<<glm::to_string(m_pos)<<","<<m_radius<<std::endl;
}

double NonhierSphere::GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal){
  
  // get intersection with plane one by one
  //std::cout<<glm::to_string(Origin)<<std::endl;
  glm::dvec4 AC = Origin - glm::dvec4(m_pos,1.0f);
  glm::dvec4 Longer;
  double A = glm::dot(Ray,Ray);
  double B = 2*glm::dot(Ray,AC);
  double C = glm::dot(AC,AC) - (m_radius*m_radius);
  double Roots[2] = {0,0};
  int n = quadraticRoots(A,B,C,Roots);
  double Result = 0;
  //std::cout<<A<<","<<B<<std::endl;
  if(n){
    if(Roots[0] == Roots[1]){
      Result = Roots[0];
    }else{
      // return the one that's closer
      double Larger = (Roots[0] > Roots[1])? Roots[0]:Roots[1];
      double Smaller = Roots[0]+Roots[1]-Larger;
      Longer = Ray;
      if(Smaller>0){
	Result = Smaller - ZERO;
      }else if(Smaller <= 0 && Larger > ZERO){
	Result = Larger + ZERO;
      }
    }
    if(Result != 0){
      Longer.x *= Result;
      Longer.y *= Result;
      Longer.z *= Result;
      // Calculate Normal
      Normal = (Origin + Longer) - glm::dvec4(m_pos,1.0f);
      /*
      Normal.x *= 10000;
      Normal.y *= 10000;
      Normal.z *= 10000;
      */
      return Result;
    }
  }else{
    return 0;
  }
}

void NonhierSphere::GetCenterSize(glm::dvec4& CandS){
  glm::dvec3 worldpos = glm::dvec3(TMat*glm::dvec4(m_pos,1.0));
  CandS = glm::dvec4(worldpos,m_radius);
}


NonhierSphere::~NonhierSphere()
{
}


NonhierBox::NonhierBox(const glm::dvec3& pos, double size):
  m_pos(pos),
  m_size(size)
{
  Type = PType::NBox;
  Vertices = new glm::dvec4[8];
  Vertices[0] = glm::dvec4(size,0.0f,0.0f,1.0f);
  Vertices[1] = glm::dvec4(size,0.0f,size,1.0f);
  Vertices[2] = glm::dvec4(size,size,size,1.0f);
  Vertices[3] = glm::dvec4(size,size,0.0f,1.0f);
  Vertices[4] = glm::dvec4(0.0f,0.0f,0.0f,1.0f);
  Vertices[5] = glm::dvec4(0.0f,0.0f,size,1.0f);
  Vertices[6] = glm::dvec4(0.0f,size,size,1.0f);
  Vertices[7] = glm::dvec4(0.0f,size,0.0f,1.0f);
  for(int i = 0;i<8;i++){
    Vertices[i] = glm::dvec4(pos,0.0f) + Vertices[i];
  }
  
  Normals = new glm::dvec4[6];
}

void NonhierBox::Print() const{
  std::cout<<glm::to_string(m_pos)<<","<<m_size<<std::endl;
}


//void NonhierBox::UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans){
void NonhierBox::UpdatePos(glm::dmat4& TMat){
  
  this->TMat = TMat;
  //Trans = trans;
  //Inv = invtrans;
  //m_pos = glm::dvec3(TMat * RMat * glm::dvec4(m_pos,1.0f));
  
  for(int i = 0; i<6;i++){
    Normals[i] = glm::dvec4(glm::cross(glm::dvec3(Vertices[Recs[(i<<2)+1]]-Vertices[Recs[(i<<2)+2]]),glm::dvec3(Vertices[Recs[i<<2]]-Vertices[Recs[(i<<2)+1]])),0.0f);
    }
  
}


double NonhierBox::GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal){
  double Rad,last;
  Rad = 0;
  double T;
  int Index = 0;
  bool Init = false;
  glm::dvec4 IN,V1,V2;
  // get intersection with plane one by one
  //std::cout<<glm::to_string(Ray)<<std::endl;
  for(int i = 0; i< 6; i++){

    T = glm::dot(-Normals[i],Origin-Vertices[Recs[i<<2]])/glm::dot(Normals[i],Ray);
    if(T > 0 && (T < last || !Init)){
      // determine if that's within the plane
      IN = Origin + glm::dvec4(Ray.x*T,Ray.y*T,Ray.z*T,0.0f);
      V1 = Vertices[Recs[(i<<2)]] - IN;
      // check inner angle sum
      for(int j = 0;j<4;j++){
	V2 = Vertices[Recs[(i<<2)+((j+1)%4)]]-IN;
	Rad += glm::acos(glm::dot(V1,V2)/(glm::length(V1)*glm::length(V2)));
	V1 = V2;
      }
      
      if(Rad <= 2*PI+ZERO && Rad >= 2*PI-ZERO){
	//std::cout<<Rad<<std::endl;
	last = T;
	Index = i;
	Init = true;
	//RealIN = IN;
	//Store = testCross;
      }
      Rad = 0;
    }
  };
  // keep the closer one
  if(Init){
    Normal =  Normals[Index];/*
    Normal.x *= 10000;
    Normal.y *= 10000;
    Normal.z *= 10000;*/
    //std::cout<<Index<<"|"<<glm::to_string(Normal)<<std::endl;
    //Normal = Store;
    //Inter = RealIN;
    return last;
  }
  return 0;
  
}


void NonhierBox::GetCenterSize(glm::dvec4& CandS){
  glm::dvec3 worldpos = glm::dvec3(TMat*glm::dvec4(m_pos,1.0));
  CandS = glm::dvec4(worldpos,m_size);
}


NonhierBox::~NonhierBox()
{
  delete[] Normals;
  delete[] Vertices;
}

Sphere::Sphere():
  NonhierSphere(glm::dvec3(0.0f),1.0f)
{
  Type = PType::Sphere;
}

Cube::Cube():
  NonhierBox(glm::dvec3(0.0f),1.0f)
{
  Type = PType::Cube;
}

/*
 * Box by min and max
 */
Box::Box(const glm::dvec3& min,const glm::dvec3& max):
  m_min(min),
  m_max(max)
{
  m_pos = (1/2.0) * (min+max);
  Type = PType::Box;

}

double Box::positive_min(double a, double b){
  double c = (a>ZERO)*a;
  double d = (b>ZERO)*b;
  return (c*d == 0)? std::max(c,d):std::min(c,d); 
}

double Box::GetIntersect(const glm::dvec4& Origin,const glm::dvec4& ray, glm::dvec4& Normal){
  double Ts[6]= {-1,-1,-1,-1,-1,-1};
  double min_t,max_t,max_min,min_max;
  char state = 0;
  glm::dvec3 diff_min = m_min - glm::dvec3(Origin);
  glm::dvec3 diff_max = m_max - glm::dvec3(Origin);

  glm::dvec4 inter_holder;
  glm::dvec4 normal_holder = glm::dvec4(0);

  // calculate all t
  // if not parallel to x plane, get x slab
  if(std::abs(ray.x) < ZERO && (Origin.x < m_min.x || Origin.x > m_max.x)){
    return 0;
  }else{
    Ts[0] = diff_min.x/ray.x;
    Ts[1] = diff_max.x/ray.x;
    max_min = std::min(Ts[0],Ts[1]);
    min_max = std::max(Ts[0],Ts[1]);
    state |= 1;
  }
  
  // if not parallel to y plane, get y slab
  if(std::abs(ray.y) < ZERO && (Origin.y < m_min.y || Origin.y > m_max.y)){
    return 0;
  }else{
    Ts[2] = diff_min.y/ray.y;
    Ts[3] = diff_max.y/ray.y;
    min_t = std::min(Ts[2],Ts[3]);
    max_t = std::max(Ts[2],Ts[3]);
    switch(state){
    case 0:
      max_min = min_t;
      min_max = max_t;
      break;
    case 1:
      max_min = std::max(max_min,min_t);
      min_max = std::min(min_max,max_t);
      break;
    }
    state |= 2;
  }

  // if not prallel to z plane, get z slab
  if(std::abs(ray.z) < ZERO && (Origin.z < m_min.z || Origin.z > m_max.z)){
    return 0;
  } else {
    Ts[4] = diff_min.z/ray.z;
    Ts[5] = diff_max.z/ray.z;
    min_t = std::min(Ts[4],Ts[5]);
    max_t = std::max(Ts[4],Ts[5]);
    switch(state){
    case 0:
      max_min = min_t;
      min_max = max_t;
      break;
    case 1:
    case 2:
    case 3:
      max_min = std::max(max_min,min_t);
      min_max = std::min(min_max,max_t);
      break;
    }
  }

  
  // check if intersect inside box by comparing max_min and min_max
  if(max_min > min_max || min_max <= 0) return 0;
  max_min = positive_min(max_min,min_max);

  double* holder[6] = {&(normal_holder.x),&(normal_holder.x),
		       &(normal_holder.y),&(normal_holder.y),
		       &(normal_holder.z),&(normal_holder.z)};
  for(int i = 0;i<6;i++){
    *(holder[i]) = std::pow(-1,i+1)*(max_min==Ts[i]);
    //std::cout<<Ts[i]<<" ";
    if(*(holder[i])) break;
  }

  Normal = normal_holder;
  return max_min;
  
}

double Box::IntersectTest(const glm::dvec4& Origin,const glm::dvec4& ray){

  
  #ifdef ANALYSIS
  std::cout<<'B'<<std::endl;
  #endif
  
  double tmin_x,tmin_y,tmin_z;
  double tmax_x,tmax_y,tmax_z;
  glm::dvec3 ori = glm::dvec3(Origin);
  glm::dvec3 inv_ray = glm::dvec3(1/ray.x,1/ray.y,1/ray.z);
  glm::dvec3 min_diff = m_min - ori;
  glm::dvec3 max_diff = m_max - ori;
  
  if(inv_ray.x >=0){
    tmin_x = min_diff.x*inv_ray.x;
    tmax_x = max_diff.x*inv_ray.x;
  }else{
    tmin_x = max_diff.x*inv_ray.x;
    tmax_x = min_diff.x*inv_ray.x;
  }

  if(inv_ray.y >=0){
    tmin_y = min_diff.y*inv_ray.y;
    tmax_y = max_diff.y*inv_ray.y;
  }else{
    tmin_y = max_diff.y*inv_ray.y;
    tmax_y = min_diff.y*inv_ray.y;
  }
  
  if((tmin_x>tmax_y)||(tmax_x<tmin_y)) return 0;
  if(tmin_x < tmin_y) tmin_x = tmin_y;
  if(tmax_x > tmax_y) tmax_x = tmax_y;

  if(inv_ray.z>=0){
    tmin_z = min_diff.z*inv_ray.z;
    tmax_z = max_diff.z*inv_ray.z;
  }else{
    tmin_z = max_diff.z*inv_ray.z;
    tmax_z = min_diff.z*inv_ray.z;
  }

  
  if((tmin_x>tmax_z)||(tmax_x<tmin_z)) return 0;
  if(tmin_x < tmin_z) tmin_x = tmin_z;
  if(tmax_x > tmax_z) tmax_x = tmax_z;

  if((tmin_x < 0) && (tmax_x < 0)) return 0;

  double t = tmax_x;
  if(tmin_x >= 0) t=tmin_x;
  
  return t;
}

void Box:: GetCenterSize(glm::dvec4& CandS){
  glm::dvec3 temp = m_max-m_min;
  CandS = glm::dvec4(m_pos,std::max(temp.x,std::max(temp.z,temp.y)));
}

Box::~Box(){
}

/*
 * Triangle Tri
 */
Tri::Tri(const Triangle* src,const std::vector<glm::dvec3>& vecs):
  mE1(vecs[src->v2]-vecs[src->v1]),
  mE2(vecs[src->v3]-vecs[src->v1]),
  mV1(vecs[src->v1]),
  mNormal(src->N)
{
  Type = PType::Tri;
}

Tri::Tri(const SAHObject* SAH):
  mE1(SAH->mP2 - SAH->mP1),
  mE2(SAH->mP3 - SAH->mP1),
  mV1(SAH->mP1),
  mNormal(glm::dvec4(glm::cross(SAH->mP1-SAH->mP2,SAH->mP2-SAH->mP3),0.0))
{
  mV2 = SAH->mP2;
  mV3 = SAH->mP3;
  Type = PType::Tri;
}

double Tri::GetIntersect(const glm::dvec4& Origin,const glm::dvec4& Ray,glm::dvec4& Normal){
  #ifdef ANALYSIS
  std::cout<<'T'<<std::endl;
  #endif
  /*
  glm::dvec4 IN,V1,V2,V3;
  double Rad = 0;
  V1 = Origin - glm::dvec4(mV1,1.0f);
  double T = glm::dot(-mNormal,V1)/glm::dot(mNormal,Ray);
  if(T>ZERO){
    IN = Origin + glm::dvec4(Ray.x*T,Ray.y*T,Ray.z*T,0);
    V1 = glm::dvec4(mV1,1) - IN;
    V2 = glm::dvec4(mV2,1) - IN;
    V3 = glm::dvec4(mV3,1) - IN;
    Rad += glm::acos(glm::dot(V1,V2)/(glm::length(V1)*glm::length(V2)));
    Rad += glm::acos(glm::dot(V2,V3)/(glm::length(V2)*glm::length(V3)));
    Rad += glm::acos(glm::dot(V3,V1)/(glm::length(V1)*glm::length(V3)));
    if(Rad <= 2*PI+ZERO && Rad >= 2*PI - ZERO){
      Normal = mNormal;
      return T;
    }
    }*/
  glm::dvec3 h,s,q,ori,ray;
  ori = glm::dvec3(Origin);
  ray = glm::dvec3(Ray);
  double a,f,u,v,t;
  h = glm::cross(ray,mE2);
  a = glm::dot(mE1,h);
  if(a > -ZERO && a < ZERO) return 0;
  f = 1/a;
  s = ori - mV1;
  u = f*(glm::dot(s,h));
  if(u<0 || u> 1) return 0;
  q = glm::cross(s,mE1);
  v = f*(glm::dot(ray,q));
  if(v<0 || u+v > 1) return 0;
  t = f*glm::dot(mE2,q);
  if(t>ZERO){
    Normal = mNormal;
    return t;
  }
  return 0;
}

void Tri::GetCenterSize(glm::dvec4& CandS){
  CandS = glm::dvec4(mV1,std::max(glm::length(mE2),glm::length(mE1)));
}

/*
 * Plane
 */

Plane::Plane(glm::dvec3 pos,glm::dvec3 v1,glm::dvec3 v2,double size):
  Center(pos),
  vec1(v1),
  vec2(v2),
  N(glm::dvec4(glm::cross(v1,v2),0.0f)),
  size(size)
{
  Type=PType::Plane;
}

double Plane::GetIntersect(const glm::dvec4& Origin,const glm::dvec4& Ray,glm::dvec4& Normal){
  double T = glm::dot(-N,Origin-glm::dvec4(Center,1.0f))/glm::dot(N,Ray);
  if(T > 0){
    glm::dvec4 Rad = (Origin+glm::dvec4(T*Ray.x,T*Ray.y,T*Ray.z,0.0f))-glm::dvec4(Center,1.0f);
    if(glm::length(Rad) <= size){
      //std::cout<<glm::length(Rad)<<std::endl;
      Normal =  N;
      return T;
    }
  }
  return 0;
}


void Plane::GetCenterSize(glm::dvec4& CandS){
  glm::dvec3 worldpos = glm::dvec3(TMat*glm::dvec4(Center,1.0));
  CandS = glm::dvec4(worldpos,size);
}

void Plane::GetRandomPoint(glm::dvec4& RandomPoint,glm::dvec4& Normal){
  double dist,deg;
  dist = RandRange(0,size,100);
  deg = RandRange(0,360,100);
  RandomPoint = TMat * glm::dvec4(dist*glm::cos(deg)*vec2 + dist*glm::sin(deg)*vec1,1.0);
  Normal = N;
}
