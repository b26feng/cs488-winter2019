#pragma once

#include "Material.hpp"

class Mirrow: public Material{
protected:
  const glm::dvec3 ks;
  double Shininess;
  double Glossiness;
public:
  Mirrow(const glm::dvec3& ks,int rc,double anu, double shininess,double G = -1.0);
  virtual void GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light*,bool Hit,double t)const override;
  virtual glm::dvec3 GetColor() const override;
  virtual void GetReflect(glm::dvec4& InRay,const glm::dvec4& Normal) override;
  // Return true if use the refraction intersection.
  virtual bool GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NextRay) override;
};
