#include "KDTree.hpp"
#include "polyroots.hpp"
#include "Helper.hpp"
#include "Constant.hpp"

#include <algorithm>
#include <iostream>
#include <glm/gtx/string_cast.hpp>
// Bouding Volume
Bounding::Bounding(const glm::dvec3& center,const double radius):
  Center(center),
  Radius(radius)
{}

bool Bounding::Hit(const glm::dvec4& Origin,const glm::dvec4& Ray) const{
  glm::dvec4 AC = Origin - glm::dvec4(Center,1.0);
  glm::dvec4 Longer;
  double A = glm::dot(Ray,Ray);
  double B = 2*glm::dot(Ray,AC);
  double C = glm::dot(AC,AC) - (Radius*Radius);
  double Roots[2] = {0,0};
  double Larger;
  int n = quadraticRoots(A,B,C,Roots);
  if(n){
    // two equal roots and both smaller than or equal to 0
    if(Roots[0] == Roots[1] && Roots[0] <= 0) return false;
    Larger = (Roots[0] > Roots[1])? Roots[0]:Roots[1];
    // two un equal roots and larger one smaller than or equal to 0
    if(Larger <= 0) return false;
  }
  return true;
}

// KD Tree
KD::KD(KD* S, KD* L):
  smaller{S},
  larger{L}
{
  mBound = nullptr;
  mCompact = nullptr;
  mSkipList = nullptr;
  mSize = 0;
}

void KD::AddSmall(KD* s) { smaller = s; }
void KD::AddLarge(KD* l) { larger = l; }
void KD::SetBound(Primitive* bound) { mBound = bound; }

void KD::SetTRI(Triangle* T,const std::vector<glm::dvec3>& vecs) { mBound = (Primitive*) (new Tri(T,vecs)); }

void KD::SetTRI(SAHObject* SH) { mBound = (Primitive*)(new Tri(SH)); }

double KD::GetIntersect(const glm::dvec4& Origin,const glm::dvec4& Ray,const std::vector<glm::dvec3>& vecs,Tri** TR)
{
  
  // check if I'm leaf
  double TT1,TT2,R1,R2,RA1;
  glm::dvec4 FakeNormal;
  TT1 = 0;
  // if I have not child
  if(smaller == larger && smaller  == nullptr){

    // get the triangle 
    TT1 = ((Tri*)mBound)->GetIntersect(Origin,Ray,FakeNormal);
    if(TT1>0){
      *TR = (Tri*)mBound;
    }
    return TT1;
    
  }else{
    // check with bounding box
    Tri* T_Hold1,*T_Hold2;
    double LeftT,RightT, SelfT;
    SelfT = mBound->GetIntersect(Origin,Ray,FakeNormal);
    // if hit with myself
    if( SelfT > 0){
      // check with left and right child
      LeftT = smaller->GetIntersect(Origin,Ray,vecs,&T_Hold1);
      RightT = larger->GetIntersect(Origin,Ray,vecs,&T_Hold2);
      if(LeftT <= RightT && LeftT > 0 || (LeftT>0 && RightT<=0)){
	*TR = T_Hold1;
	return LeftT;
      }
      if(RightT < LeftT && RightT > 0 || (RightT>0 && LeftT<=0)){
	*TR = T_Hold2;
	return RightT;
      }
    }
    return 0;
    
  }
  return 0;
}

double KD::CompactInter(const glm::dvec4& Origin,const glm::dvec4& Ray,const std::vector<glm::dvec3>& vecs,Tri** TR){
  double T0,T1;
  glm::dvec4 normal_holder;
  T0 = 0;
  size_t total = mSize+1;
  size_t skip;
  Primitive* p_holder;
  Box* b_holder;
  Tri* t_holder;

  // Loop through the KD-Tree Compact Array to find intersection
  for(size_t i=0;i<total;i++){
    skip = mSkipList[i];
    p_holder = mCompact[i];
    t_holder = (Tri*)p_holder;
    b_holder = (Box*)p_holder;

    // If skip size is not 0, it's a bounding box.
    // Otherwise it's a geometry, i.e. triangle.
    if(skip != 0){
      T1 = b_holder->IntersectTest(Origin,Ray);
      i += (T1<=0)* skip;
    }else{
      T1 = t_holder->GetIntersect(Origin,Ray,normal_holder);
      if(T1 > 0 && (T1<T0 || T0<=0)){
	T0 = T1;
	*TR = t_holder;
      }
    }
    
  }
  return T0;
}


size_t KD::GetSizeAndIndex(size_t* Index){
  mIndex = *Index;
  *Index +=1;
  if(smaller==larger && smaller == nullptr) return 1;
  mSize = smaller->GetSizeAndIndex(Index)+ larger->GetSizeAndIndex(Index);
  return mSize+1;
}

void KD::FillInCompact(Primitive** Compact,size_t* SkipList){
  SkipList[mIndex] = mSize;
  Compact[mIndex] = mBound;
  if(smaller && larger){
    smaller->FillInCompact(Compact,SkipList);
    larger->FillInCompact(Compact,SkipList);
  }
}

void KD::MakeCompact(){
  // count the total node inside the kdtree recursively
  size_t IDX = 0;
  size_t total = GetSizeAndIndex(&IDX)+1;
  mCompact = new Primitive*[total];
  mSkipList = new size_t[total];
  // fill in the primitives and the skip distance
  FillInCompact(mCompact,mSkipList);
  int counter = 0;
  for(int i = 0; i<total;i++){
    if(mSkipList[i] == 0) counter ++;
  }
  //std::cout<<"FLAT:"<<counter<<std::endl;
}

KD::~KD(){
  if(smaller!=nullptr) delete smaller;
  if(larger != nullptr) delete larger;
  if(mCompact != nullptr) delete[] mCompact;
  if(mSkipList != nullptr) delete[] mSkipList;
  if(mBound != nullptr) delete mBound;
}

// other functions

// helper functions


void GetMinMax(glm::dvec3& min,glm::dvec3& max,Triangle** Ts,size_t start,size_t end,const std::vector<glm::dvec3>& vecs){
  bool init = false;
  Triangle* t;
  for(size_t i = start;i<end;i++){
    t = Ts[i];
    if(init){
      min.x = std::min(min.x,std::min(vecs[t->v1].x,std::min(vecs[t->v2].x,vecs[t->v3].x)));
      max.x = std::max(max.x,std::max(vecs[t->v1].x,std::max(vecs[t->v2].x,vecs[t->v3].x)));
      
      min.y = std::min(min.y,std::min(vecs[t->v1].y,std::min(vecs[t->v2].y,vecs[t->v3].y)));
      max.y = std::max(max.y,std::max(vecs[t->v1].y,std::max(vecs[t->v2].y,vecs[t->v3].y)));
      
      min.z = std::min(min.z,std::min(vecs[t->v1].z,std::min(vecs[t->v2].z,vecs[t->v3].z)));
      max.z = std::max(max.z,std::max(vecs[t->v1].z,std::max(vecs[t->v2].z,vecs[t->v3].z)));
      
    }else{
      min.x = std::min(vecs[t->v1].x,std::min(vecs[t->v2].x,vecs[t->v3].x));
      max.x = std::max(vecs[t->v1].x,std::max(vecs[t->v2].x,vecs[t->v3].x));
      
      min.y = std::min(vecs[t->v1].y,std::min(vecs[t->v2].y,vecs[t->v3].y));
      max.y = std::max(vecs[t->v1].y,std::max(vecs[t->v2].y,vecs[t->v3].y));
      
      min.z = std::min(vecs[t->v1].z,std::min(vecs[t->v2].z,vecs[t->v3].z));
      max.z = std::max(vecs[t->v1].z,std::max(vecs[t->v2].z,vecs[t->v3].z));
      init = true;
    }
  }
}


void GetMinMax(glm::dvec3& min,glm::dvec3& max,std::vector<SAHObject*> src){

  bool init = false;
  for(SAHObject* item:src){
    if(init){
      min.x = std::min(min.x,std::min(item->mP1.x,std::min(item->mP2.x,item->mP3.x)));
      max.x = std::max(max.x,std::max(item->mP1.x,std::max(item->mP2.x,item->mP3.x)));
      
      min.y = std::min(min.y,std::min(item->mP1.y,std::min(item->mP2.y,item->mP3.y)));
      max.y = std::max(max.y,std::max(item->mP1.y,std::max(item->mP2.y,item->mP3.y)));
      
      min.z = std::min(min.z,std::min(item->mP1.z,std::min(item->mP2.z,item->mP3.z)));
      max.z = std::max(max.z,std::max(item->mP1.z,std::max(item->mP2.z,item->mP3.z)));
      
    }else{
      min.x = std::min(item->mP1.x,std::min(item->mP2.x,item->mP3.x));
      max.x = std::max(item->mP1.x,std::max(item->mP2.x,item->mP3.x));
      
      min.y = std::min(item->mP1.y,std::min(item->mP2.y,item->mP3.y));
      max.y = std::max(item->mP1.y,std::max(item->mP2.y,item->mP3.y));
      
      min.z = std::min(item->mP1.z,std::min(item->mP2.z,item->mP3.z));
      max.z = std::max(item->mP1.z,std::max(item->mP2.z,item->mP3.z));
      init = true;
    }
  }
}


KD* MakeTree(Triangle** Ts, size_t count,size_t start, size_t end,const std::vector<glm::dvec3>& vecs,const char idx){
  KD* Self = new KD();
  Self->Size = end-start;
  size_t realSize= end-start;
  if(realSize > count) return nullptr;
  if( realSize == 1){
    Self->SetTRI(Ts[start],vecs);
  }else{
    glm::dvec3 min_p,max_p;
    GetMinMax(min_p,max_p,Ts,start,end,vecs);
    Self->SetBound((Primitive*)(new Box(min_p,max_p)));
    // make a copy of all triangles
    Triangle** LocalCopy = new Triangle*[end-start];
    size_t total = end-start;
    for(size_t i = start; i< end;i++){
      LocalCopy[i-start] = Ts[i];
    }
    char nextIDX;
    // construct child
    switch(idx){
    case 'z':
      std::sort(LocalCopy,LocalCopy+total,Compare);
      nextIDX = 'x';
      break;
    case 'y':
      std::sort(LocalCopy,LocalCopy+total,CompareY);
      nextIDX = 'z';
      break;
    case 'x':
      std::sort(LocalCopy,LocalCopy+total,CompareX);
      nextIDX = 'y';
      break;
    }
    Self->AddSmall(MakeTree(LocalCopy,total,0,total>>1,vecs,nextIDX));
    Self->AddLarge(MakeTree(LocalCopy,total,total>>1,total,vecs,nextIDX));
    delete[] LocalCopy;
  }
  return Self;
}

double GetDist(glm::dvec3& lmin,glm::dvec3& lmax, glm::dvec3& rmin, glm::dvec3& rmax,char* ax){
  glm::dvec3 min,max;
  min.x = std::max(lmin.x,rmin.x);
  min.y = std::max(lmin.y,rmin.y);
  min.z = std::max(lmin.z,rmin.z);

  max.x = std::min(lmax.x,rmax.x);
  max.y = std::min(lmax.y,rmax.y);
  max.z = std::min(lmax.z,rmax.z);

  // count the number of axis that are not over lapping
  *ax = ((max.x<min.x)+(max.y<min.y)+(max.z<min.z));
  int sign = (*ax > 0)? -1:1;
  //double region = sign*std::abs(max.x-min.x)*std::abs(max.y-min.y)*std::abs(max.z-min.z);
  double region = sign*(max.x-min.x)*(max.y-min.y)*(max.z-min.z);
  return region;
}

KD* MakeTreeSmart(Triangle** Ts, size_t count,size_t start, size_t end,const std::vector<glm::dvec3>& vecs,bool is_root){
  KD* Self = new KD();
  Self->Size = end-start;
  size_t realSize= end-start;
  if(realSize > count) return nullptr;
  if( realSize == 1){
    Self->SetTRI(Ts[start],vecs);
  }else{
    glm::dvec3 min_1,max_1,min_2,max_2;
    glm::dvec3 rmin_1,rmax_1,rmin_2,rmax_2;
    double dist1, dist2;
    char ax_count_1,ax_count_2;
    if(is_root){
      GetMinMax(min_1,max_1,Ts,start,end,vecs);
      Self->SetBound((Primitive*)(new Box(min_1,max_1)));
    }
    // make a copy of all triangles
    size_t total = end-start;
    Triangle** LocalCopy = new Triangle*[total];
    Triangle** LocalCopy2 = new Triangle*[total];
    Triangle** LocalHolder = LocalCopy2;
    for(size_t i = start; i< end;i++){
      LocalCopy[i-start] = LocalCopy2[i-start]= Ts[i];
    }
    
    // Get left and right box based on z
    std::sort(LocalCopy,LocalCopy+total,Compare);
    GetMinMax(min_1,max_1,LocalCopy,0,total>>1,vecs);
    GetMinMax(rmin_1,rmax_1,LocalCopy,total>>1,total,vecs);
    dist1 = GetDist(min_1,max_1,rmin_1,rmax_1,&ax_count_1);
    
    std::sort(LocalCopy2,LocalCopy2+total,CompareY);
    GetMinMax(min_2,max_2,LocalCopy2,0,total>>1,vecs);
    GetMinMax(rmin_2,rmax_2,LocalCopy2,total>>1,total,vecs);
    dist2 = GetDist(min_2,max_2,rmin_2,rmax_2,&ax_count_2);
    
    // compare the distance
    // distance heuristic
#ifdef DISTANCE_HEURISTIC
    if(dist2 < dist1 && ax_count_2 >= ax_count_1){
#else
    glm::dvec3 ldiff1,ldiff2,rdiff1,rdiff2;
    ldiff1 = max_1 - min_1;
    ldiff2 = max_2 - min_2;
    rdiff1 = rmax_1 - rmin_1;
    rdiff2 = rmax_2 - rmin_2;
      
    if(ldiff2.x*ldiff2.y*ldiff2.z+ rdiff2.x*rdiff2.y*rdiff2.z <
       ldiff1.x*ldiff1.y*ldiff1.z+ rdiff1.x*rdiff1.y*rdiff1.z){
      rdiff1 = rdiff2;
      ldiff1 = ldiff2;
#endif
      min_1 = min_2;
      max_1 = max_2;
      rmin_1 = rmin_2;
      rmax_1 = rmax_2;
      dist1 = dist2;
      ax_count_1 = ax_count_2;
      // swap container
      LocalHolder = LocalCopy;
      LocalCopy = LocalCopy2;
      LocalCopy2 = LocalHolder;
    }

    // sort the other
    std::sort(LocalCopy2,LocalCopy2+total,CompareX);
    GetMinMax(min_2,max_2,LocalCopy2,0,total>>1,vecs);
    GetMinMax(rmin_2,rmax_2,LocalCopy2,total>>1,total,vecs);
    dist2 = GetDist(min_2,max_2,rmin_2,rmax_2,&ax_count_2);

    // distance heuristic
#ifdef DISTANCE_HEURISTIC
    if(dist2 < dist1 && ax_count_2 >= ax_count_1){
#else
    ldiff2 = max_2 - min_2;
    rdiff2 = rmax_2 - rmin_2;
    if(ldiff2.x*ldiff2.y*ldiff2.z+ rdiff2.x*rdiff2.y*rdiff2.z <
    ldiff1.x*ldiff1.y*ldiff1.z+ rdiff1.x*rdiff1.y*rdiff1.z){
#endif
      min_1 = min_2;
      max_1 = max_2;
      rmin_1 = rmin_2;
      rmax_1 = rmax_2;
      LocalHolder = LocalCopy;
      LocalCopy = LocalCopy2;
      LocalCopy2 = LocalHolder;
    }

    KD* left = MakeTreeSmart(LocalCopy,total,0,total>>1,vecs);
    if((total>>1) > 1 ) left->SetBound((Primitive*)(new Box(min_1,max_1)));
    KD* right = MakeTreeSmart(LocalCopy,total,total>>1,total,vecs);
    if(total - (total>>1) > 1) right->SetBound((Primitive*)(new Box(rmin_1,rmax_1)));
    Self->AddSmall(left);
    Self->AddLarge(right);
    delete[] LocalCopy;
    delete[] LocalCopy2;
    }
  return Self;
}

int GetSAHBox(const std::vector<SAHObject*>& SList, glm::dvec3& lmin,glm::dvec3& lmax, glm::dvec3& rmin, glm::dvec3& rmax, double* Ratio,bool print = false){
  // calculate volume for each spliting
  int len = SList.size();
  double* volumes = new double[len+1];
  glm::dvec3* rmins = new glm::dvec3[len];
  glm::dvec3* rmaxs = new glm::dvec3[len];
  volumes[len] = 0;
  glm::dvec3 minP = glm::dvec3();
  glm::dvec3 maxP = glm::dvec3();
  glm::dvec3 diff;
  // get right side volume array
  for(int i = len-1; i>=0;i--){
    // init 
    if(i == len-1){
      minP.x = std::min(SList[i]->mP1.x,std::min(SList[i]->mP2.x,SList[i]->mP3.x));
      minP.y = std::min(SList[i]->mP1.y,std::min(SList[i]->mP2.y,SList[i]->mP3.y));
      minP.z = std::min(SList[i]->mP1.z,std::min(SList[i]->mP2.z,SList[i]->mP3.z));
      
      maxP.x = std::max(SList[i]->mP1.x,std::max(SList[i]->mP2.x,SList[i]->mP3.x));
      maxP.y = std::max(SList[i]->mP1.y,std::max(SList[i]->mP2.y,SList[i]->mP3.y));
      maxP.z = std::max(SList[i]->mP1.z,std::max(SList[i]->mP2.z,SList[i]->mP3.z));
    }else{
    
      minP.x = std::min(minP.x,std::min(SList[i]->mP1.x,std::min(SList[i]->mP2.x,SList[i]->mP3.x)));
      minP.y = std::min(minP.y,std::min(SList[i]->mP1.y,std::min(SList[i]->mP2.y,SList[i]->mP3.y)));
      minP.z = std::min(minP.z,std::min(SList[i]->mP1.z,std::min(SList[i]->mP2.z,SList[i]->mP3.z)));
      
      maxP.x = std::max(maxP.x,std::max(SList[i]->mP1.x,std::max(SList[i]->mP2.x,SList[i]->mP3.x)));
      maxP.y = std::max(maxP.y,std::max(SList[i]->mP1.y,std::max(SList[i]->mP2.y,SList[i]->mP3.y)));
      maxP.z = std::max(maxP.z,std::max(SList[i]->mP1.z,std::max(SList[i]->mP2.z,SList[i]->mP3.z)));
    }
    diff = maxP-minP;
    volumes[i] = diff.x*diff.y*diff.z;
    rmins[i] = minP;
    rmaxs[i] = maxP;
  }
  double total_volume = volumes[0];
  double lvolume = 0;
  double rvolume;
  double SAH,MinSAH;
  int index;
  // calculate SAH value and check for the smallest
  for(int i = 0, j = 1;i<len;i++,j++){
    // initialize
    rvolume = volumes[i]/total_volume;
    if(i == 0){
      minP.x = std::min(SList[i]->mP1.x,std::min(SList[i]->mP2.x,SList[i]->mP3.x));
      minP.y = std::min(SList[i]->mP1.y,std::min(SList[i]->mP2.y,SList[i]->mP3.y));
      minP.z = std::min(SList[i]->mP1.z,std::min(SList[i]->mP2.z,SList[i]->mP3.z));
      
      maxP.x = std::max(SList[i]->mP1.x,std::max(SList[i]->mP2.x,SList[i]->mP3.x));
      maxP.y = std::max(SList[i]->mP1.y,std::max(SList[i]->mP2.y,SList[i]->mP3.y));
      maxP.z = std::max(SList[i]->mP1.z,std::max(SList[i]->mP2.z,SList[i]->mP3.z));
      
    }else{
    
    minP.x = std::min(minP.x,std::min(SList[i]->mP1.x,std::min(SList[i]->mP2.x,SList[i]->mP3.x)));
    minP.y = std::min(minP.y,std::min(SList[i]->mP1.y,std::min(SList[i]->mP2.y,SList[i]->mP3.y)));
    minP.z = std::min(minP.z,std::min(SList[i]->mP1.z,std::min(SList[i]->mP2.z,SList[i]->mP3.z)));
    
    maxP.x = std::max(maxP.x,std::max(SList[i]->mP1.x,std::max(SList[i]->mP2.x,SList[i]->mP3.x)));
    maxP.y = std::max(maxP.y,std::max(SList[i]->mP1.y,std::max(SList[i]->mP2.y,SList[i]->mP3.y)));
    maxP.z = std::max(maxP.z,std::max(SList[i]->mP1.z,std::max(SList[i]->mP2.z,SList[i]->mP3.z)));
    }
    diff = maxP-minP;
    lvolume = diff.x*diff.y*diff.z/total_volume;
    SAH = j*lvolume + (len - j)*rvolume;
    if(SAH < MinSAH || i == 0) {
      index = i;
      lmin = minP;
      lmax = maxP;
      rmin = rmins[i+1];
      rmax = rmaxs[i+1]; 
      //if(rmin.x == rmin.y && rmin.y == rmin.z && rmin.z == 0) std::cout<<SAH<<","<<i<<","<<lvolume<<std::endl;
      MinSAH = SAH;
    }
    /*if(print)  std::cout<<j<<","<<lvolume<<","<<len-j<<","<<rvolume<<","<<SAH<<","<<MinSAH<<std::endl;*/
  }
  *Ratio = MinSAH;
  delete[] volumes;
  delete[] rmins;
  delete[] rmaxs;
  return index;
}

KD* MakeSAH(const SAHList& src,bool is_root){
  KD* Self = new KD();
  size_t realSize= src.mX.size();
  if(realSize <= 0) return nullptr;
  // If there's only 1 object, then it must be 1 single triangle.
  if( realSize == 1){
    Self->SetTRI(src.mX[0]);
  }else{
    glm::dvec3 min_1,max_1,min_2,max_2;
    glm::dvec3 rmin_1,rmax_1,rmin_2,rmax_2;
    double cost1,cost2;
    char ax_count_1,ax_count_2;
    // Root sets its own bounding volume.
    if(is_root){
      GetMinMax(min_1,max_1,src.mX);
      Self->SetBound((Primitive*)(new Box(min_1,max_1)));
    }
    // index_1 is the spliting index for the best partitioning axis.
    // It starts with X, index_2 is the Y or Z's splitting index.
    int index_1 = GetSAHBox(src.mX,min_1,max_1,rmin_1,rmax_1,&cost1);
    int index_2 = GetSAHBox(src.mY,min_2,max_2,rmin_2,rmax_2,&cost2);
    char axis = 'x';
    // Compare the cost and swap if necessary
    if(cost2 < cost1){
      min_1 = min_2;
      max_1 = max_2;
      rmin_1 = rmin_2;
      rmax_1 = rmax_2;
      cost1 = cost2;
      index_1 = index_2;
      axis = 'y';
    }
    // Update index_2 to be the splitting index of Z axis.
    index_2 = GetSAHBox(src.mZ,min_2,max_2,rmin_2,rmax_2,&cost2);
    // Compare the cost and swap if necessary.
    if(cost2 < cost1){
      min_1 = min_2;
      max_1 = max_2;
      rmin_1 = rmin_2;
      rmax_1 = rmax_2;
      cost1 = cost2;
      index_1 = index_2;
      axis = 'z';
    }
    
    // get 2 children copy
    SAHList LeftList = SAHList(src,0,index_1+1,axis);
    SAHList RightList = SAHList(src,index_1+1,realSize,axis);
    //std::cout<<"Guess 3"<<std::endl;
    KD* left = MakeSAH(LeftList);
    if(LeftList.mX.size()>1){
      left->SetBound((Primitive*)(new Box(min_1,max_1)));
    }
    KD* right = MakeSAH(RightList);
    if(RightList.mX.size()>1){
      right->SetBound((Primitive*)(new Box(rmin_1,rmax_1)));
    }
    Self->AddSmall(left);
    Self->AddLarge(right);
  }
  return Self;
}
