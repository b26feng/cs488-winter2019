#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "Triangle.hpp"
class SAHObject{
public:
  bool mSelected;
  glm::dvec3 mP1;
  glm::dvec3 mP2;
  glm::dvec3 mP3;
  glm::dvec3 mCenter;
  double mArea;
  
  SAHObject(Triangle* TR,std::vector<glm::dvec3>& src);
  SAHObject(const SAHObject* SAH);
};

class SAHList{
protected:
  bool mOriginalSAH;
public:
  std::vector<SAHObject*> mX;
  std::vector<SAHObject*> mY;
  std::vector<SAHObject*> mZ;
  double mArea;
  //std::vector<SAHObject*> mBucket;

  // Construct the original, the very first SAHList
  // mX,mY,mZ need to be cleared
  SAHList(Triangle** Ts,int len,std::vector<glm::dvec3>& src);
  // Construct the shallow copy from an original or non-original SAHList
  // mX,mY,mZ will eventually deleted in the original list.
  SAHList(const SAHList& SHList,int start, int end, char axis);
  ~SAHList();
};

