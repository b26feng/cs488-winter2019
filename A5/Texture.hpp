#pragma once
#include "Noise.hpp"

#include <glm/glm.hpp>
enum class TexType{
  Texture,
    Bump,
    Perlin
    };

class Texture{
protected:
  const int Size;
  const double epsilon;
  glm::dvec3 PCenter;
  double PR;
public:
  Texture(const int size,const double Epsilon);
  virtual ~Texture();
  virtual void PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal);
  virtual void SetData(const glm::dvec3& Center,const double radius);
  TexType Type;
  Noise* NoiseFunc;
};
