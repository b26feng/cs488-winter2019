#include "Random.hpp"

double RandRange(int x,int y, int precision){
  int interval = (y-x) * precision;
  int r = rand()%interval;
  double result = r/((double)precision) - x;
  return result;
}

unsigned int GrayCode(unsigned int n){
  return n^(n>>1);
}
