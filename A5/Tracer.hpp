#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <mutex>

#include "Config.hpp"
#include "PicTrans.hpp"
#include "GeometryNode.hpp"
#include "Light.hpp"
#include "Image.hpp"

class Tracer{
  void GetColor(const glm::dvec4& Intersect,const glm::dvec4& Normal,const glm::dvec4& EyeRay,glm::dvec3& color,const GeometryNode* SelfNode,SceneNode* Skip = nullptr,char RayColor = 'W');
  void BayerPixel(double x, double y,const glm::dvec4& EYE,glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn);
  void RecursiveTrace(const glm::dvec4& Origin,const glm::dvec4& Ray,const int RCount, GeometryNode* CurrentNode, glm::dvec3& color,char RayColor = 'W');

protected:
  virtual void SinglePixel(double x, double y,const glm::dvec4& EYE,glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn,bool* Skip=nullptr,char RayColor = 'W');
  
  virtual void SingleRay(glm::dvec4& Ray,const glm::dvec4& EYE, glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn,bool* Skip=nullptr,char RayColor = 'W');
#ifndef XTRANS
#else
  void Avg(std::vector<glm::dvec3>& Filter,glm::dvec3& Dest);
#endif
  PicTrans* PT;
  std::vector<SceneNode*> RootV;
  SceneNode* Root;
  size_t NodeCount;
  const glm::dvec3 Amb;
  const std::list<Light*> LGT;
  glm::dvec3 Background;
  
  uint R,G,B;
  double Apeture;
  int Range;
  double FocalLen;
  double FocalDist;
  double FPZ;
  //double RC,GC,BC;
  bool Blur;
  uint SampleNum;
  uint dim;
  double inter;
#ifndef XTRANS
#else
  char* Bayer;
#endif
  uint SSCount;

  const double ZERO;

  std::mutex m_mtx;
  size_t m_current_pixel;
  size_t m_total_pixel;
  size_t m_size_per_thread;
  GeometryNode* m_PrimLight;
public:
  Tracer(PicTrans* P,SceneNode* root,const glm::dvec3& ambient,const std::list<Light *>& lights);
  ~Tracer();
  virtual void Trace(Image&I,int ThreadNumber);
  virtual void RayTrace(Image& image);
};
