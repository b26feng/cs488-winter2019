// Winter 2019

#pragma once
#include "Light.hpp"
#include "Texture.hpp"
#include "SceneNode.hpp"

enum class MatType{
  None,
  Phong,
    Mirrow,
    Transparent,
    Bump,
    FresPhong
    };

class Material {
public:
  virtual ~Material();
  virtual void GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t) const;
  virtual glm::dvec3 GetColor() const;
  // Used for get the color under direct illumination
  virtual glm::dvec3 GetDLColor(const glm::dvec4& Inter,const glm::dvec4& N, const SceneNode* Light,SceneNode* Root) const;
  virtual bool IsLight();
  virtual int GetRC() const;
  virtual double GetRA() const;
  virtual void SetRC(int NewRC);
  virtual void GetReflect(glm::dvec4& InRay,const glm::dvec4& Normal);
  virtual void PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal);
  virtual double FresnellRate(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& Refraction,char RayColor);
  virtual void SetData(const:: glm::dvec3& Center,const double radius);
  // Return true if use the refraction intersection. NextRay will be modified
  // to the next ray to be traced.
  virtual bool GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NextRay);
  MatType Type;
  Texture* tex;
protected:
  int ReflectCount;
  double RAttenu;
  Material();
};
