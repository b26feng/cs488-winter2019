#include "Transparent.hpp"
#include "Random.hpp"


#include <iostream>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>

static const double PI = 3.14159265358979323846;

Transparent::Transparent(const glm::dvec3& ks,int rc,double anu,double shininess,double G,double RI):
  Mirrow(ks,rc,anu,shininess,G),
  RefrIndex(RI),
  RRate(1.50),
  GRate(1.52534),
  BRate(1.5469),
  Base(1.5)
{
  Type = MatType::Transparent;
  LimitAngle = asin(1.0/RI);
}

double Transparent::FresnellRate(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& Refraction,char RayColor)
{

  double n1,n2,F,RRIndex;
  glm::dvec4 RealNormal = Normal;
  switch(RayColor){
  case 'R':
    RRIndex = RRate;
    break;
  case 'G':
    RRIndex = GRate;
    break;
  case 'B':
    RRIndex = BRate;
    break;
  default:
    RRIndex = Base;
    break;
  }
  
  RRIndex *= RefrIndex/Base; 
  glm::dvec4 Ray = InRay;
  Ray.w = 0.0;

  // check if going in or out
  //bool GoIn = (tex == nullptr)?InRay.w >= 0: glm::dot(glm::dvec3(Ray),glm::dvec3(Normal)) < 0;
  bool GoIn = (glm::dot(Ray,Normal) <= 0);
  /*if(GoIn!=AnotherGoIn){
    std::cout<<GoIn<<","<<AnotherGoIn<<","<<InRay.w<<std::endl;
    std::cout<<glm::to_string(Ray)<<std::endl<<glm::to_string(Normal)<<std::endl;
    std::cout<<glm::dot(Ray,Normal)<<std::endl<<std::endl;
    }*/
 
  //bool GoIn = InRay.w>=0;
  if(GoIn){
    n1 = 1.0;
    n2 = RRIndex;
  }else{
    n2 = 1.0;
    n1 = RRIndex;
    RealNormal = -RealNormal;
  }
  /*
  // Pertub the normal
  if(Glossiness >= 0){
    x1 = RandRange(0,1,10000);
    x2 = RandRange(0,1,10000);
    polar = glm::acos(glm::pow((1-x1),1.0/(Glossiness+1)));
    azimuthal = 2*PI*x2;
    glm::dmat4 PolarM,AzimuM;
    
    PolarM = glm::rotate(glm::degrees(polar),glm::cross(glm::dvec3(Normal),glm::dvec3(InRay)));
    AzimuM = glm::rotate(glm::degrees(azimuthal),glm::dvec3(Normal));
    ImperfectN = AzimuM * PolarM * Normal;
    }*/
  
  double InAng = glm::angle(glm::normalize(Ray),-RealNormal);
  F  = AdvFres(n1,n2,InAng);
  if(GoIn && InRay.w <0){
    //std::cout<<F<<std::endl;
  }
  //std::cout<<LimitAngle<<std::endl;
  // if In Angle is larger than limit and going out, full internal reflection
  if((F!=F) || F > 1){
    //std::cout<<F<<","<<InAng<<std::endl;
    /*if(!GoIn){
      std::cout<<glm::degrees(InAng)<<std::endl;
      }*/
    return 1.0;
  }
  double RAng = glm::asin((n1/n2)*glm::sin(InAng));

  // get the perfect refraction ray
  Refraction = glm::refract(Ray,RealNormal,n1/n2);

  /*if(Refraction.x != Refraction.x){
    F = 1;
    }*/
  //std::cout<<"Fres:"<<glm::to_string(Ray)<<glm::to_string(RealNormal)<<glm::to_string(Refraction)<<GoIn<<","<<glm::dot(Ray,RealNormal)<<","<<F<<","<<n1<<"/"<<n2<<std::endl;
    //}
  // pertub
  /*
  if(Glossiness >= 0){
    x1 = RandRange(0,1,10000);
    x2 = RandRange(0,1,10000);
    polar = glm::acos(glm::pow((1-x1),1.0/(Glossiness+1)));
    azimuthal = 2*PI*x2;
    glm::dmat4 PolarM,AzimuM;
    
    PolarM = glm::rotate(glm::degrees(polar),glm::cross(glm::dvec3(Normal),glm::dvec3(InRay)));
    AzimuM = glm::rotate(glm::degrees(azimuthal),glm::dvec3(InRay));
    Refraction = AzimuM * PolarM * Refraction;
    }*/
  
  //Refraction.w = (InRay.w<0.0)? 0.0:-1.0;
  // Finally calculate Fresnell Rate

  //std::cout<<AdvFres(1.3,1,0)<<std::endl;
  return F;
  //return Fres(n1,n2,InAng,RAng);
}


double Transparent::Fres(double n1,double n2, double theta1, double theta2) const
{
  
  
  double FRs,FRt, F;
  
  FRs = glm::pow((n2*glm::cos(theta1)-n1*glm::cos(theta2))/(n2*glm::cos(theta1)+n1*glm::cos(theta2)),2);
  FRt = glm::pow((n1*glm::cos(theta2)-n2*glm::cos(theta1))/(n1*glm::cos(theta2)+n2*glm::cos(theta1)),2);
  F = (FRs+FRt)/2;
  return F;
}


double Transparent::AdvFres(double n1,double n2, double theta1) const
{
  double F,FRs,FRs1,FRt,FRt1,a,a2,b,b2,c;
  c = glm::pow(n2,2)-glm::pow(n1,2)*glm::pow(glm::sin(theta1),2);
  a2 = (1/(2*glm::pow(n1,2)))*(glm::sqrt(c*c)+c);
  a = glm::sqrt(a2);
  b2 = (1/(2*glm::pow(n1,2)))*
    (glm::sqrt(c*c)-c);
  b = glm::sqrt(b2);

  FRs1 = a*a + b*b +glm::pow(glm::cos(theta1),2);
  FRs = (FRs1-2*a*glm::cos(theta1))/(FRs1+2*a*glm::cos(theta1));
  // FRs = (a2+b2-2*a*glm::cos(theta1)+glm::pow(glm::cos(theta1),2))/(a2+b2+2*a*glm::cos(theta1)+glm::pow(glm::cos(theta1),2));
  FRt1 = a*a+b*b+glm::pow(glm::sin(theta1)*glm::tan(theta1),2);
  FRt = FRs*((FRt1-2*a*glm::sin(theta1)*glm::tan(theta1))/(FRt1+2*a*glm::sin(theta1)*glm::tan(theta1)));
  //FRt = FRs*((a2+b2-2*a*glm::sin(theta1)*glm::tan(theta1)+glm::pow(glm::sin(theta1),2)*glm::pow(glm::tan(theta1),2))/(a2+b2+2*a*glm::sin(theta1)*glm::tan(theta1)+glm::pow(glm::sin(theta1),2)*glm::pow(glm::tan(theta1),2)));
  //a = ()();
  F = (FRs+FRt)/2;
  return F;
}

void Transparent::GetReflect(glm::dvec4& InRay,const glm::dvec4&Normal){
  Material::GetReflect(InRay,Normal);
}

void Transparent::PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NewNormal)
{
  
  Material::PertubNormal(InRay,Normal,NewNormal);
  glm::dvec4& RealNormal = NewNormal;
  if(Glossiness >= 0){
    double polar,azimuthal,x1,x2;
    x1 = RandRange(0,1,10000);
    x2 = RandRange(0,1,10000);
    polar = glm::acos(glm::pow((1-x1),1.0/(Glossiness+1)));
    azimuthal = 2*PI*x2;
    glm::dmat4 PolarM,AzimuM;
    
    PolarM = glm::rotate(glm::degrees(polar),glm::cross(glm::dvec3(RealNormal),glm::dvec3(InRay)));
    AzimuM = glm::rotate(glm::degrees(azimuthal),glm::dvec3(RealNormal));
    NewNormal = AzimuM * PolarM * RealNormal;
  }
}

bool Transparent::GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NextRay){
  double r1 = RandRange(0,1,100);
  double FresCoeff = 0;
  glm::dvec4 RefractRay = glm::dvec4(0);
  FresCoeff = FresnellRate(InRay,Normal,RefractRay,'W');
  if(r1 <= FresCoeff){
    NextRay = InRay;
    GetReflect(NextRay,Normal);
    return false;
  }else{
    NextRay = RefractRay;
    return true;
  }
}
