// Winter 2019

#include "GeometryNode.hpp"
#include "Material.hpp"
#include <iostream>
#include <glm/gtx/string_cast.hpp>
//---------------------------------------------------------------------------------------
GeometryNode::GeometryNode(
	const std::string & name, Primitive *prim, Material *mat )
	: SceneNode( name )
	, m_material( mat )
	, m_primitive( prim )
{
	m_nodeType = NodeType::GeometryNode;
}

void GeometryNode::setMaterial( Material *mat )
{
	// Obviously, there's a potential memory leak here.  A good solution
	// would be to use some kind of reference counting, as in the 
	// C++ shared_ptr.  But I'm going to punt on that problem here.
	// Why?  Two reasons:
	// (a) In practice we expect the scene to be constructed exactly
	//     once.  There's no reason to believe that materials will be
	//     repeatedly overwritten in a GeometryNode.
	// (b) A ray tracer is a program in which you compute once, and 
	//     throw away all your data.  A memory leak won't build up and
	//     crash the program.

	m_material = mat;
}

void GeometryNode::PrintType()
{
  std::cout<<m_name<<":";
  switch(m_primitive->Type){
  case PType::NSphere:
    std::cout<<"NSphere |";
    break;
  case PType::NBox:
    std::cout<<"NBox |";
    break;
  case PType::Sphere:
    std::cout<<"Sphere |";
    break;
  case PType::Cube:
    std::cout<<"Cube |";
    break;
  case PType::Primitive:
    std::cout<<"Primitive |";
    break;
  case PType::Plane:
    std::cout<<"Plane |";
    break;
  default:
    std::cout<<"Unknown |";
    break;
  }
  m_primitive->Print();
  for(SceneNode* i: children){
    i->PrintType();
  }
}

void GeometryNode::PreProcess(std::vector<SceneNode*>& R,SceneNode* Parent){
  R.emplace_back(this);
  /*
  if(m_name=="plane"){
    for(int i = 0;i<4;i++){
      std::cout<<glm::to_string(trans[i])<<std::endl;
    }
  }*/
  //std::cout<<m_name<<":"<<m_nodeId<<std::endl;
  
  if(Parent!=nullptr){
    //TRMat = TMat * RMat;
    TRMat = trans * Parent->trans;
  }else{
    TRMat = trans;
  }
    //TMat *= Parent->TMat;
    //RMat *= Parent->RMat;
    //TRMat *= Parent->TRMat;
    //SMat *= Parent->SMat;
    //trans *= Parent->trans;
    /*
    trans = Parent->trans * trans;
    invtrans = glm::inverse(trans);
  }
  if(m_name=="plane"){
    for(int i = 0;i<4;i++){
      std::cout<<glm::to_string(trans[i])<<std::endl;
    }
    }*/
  //m_primitive->UpdatePos(TRMat,SMat,trans,invtrans);
  m_primitive->UpdatePos(TRMat);
  
  //m_primitive->TMat = TMat;
  //m_primitive->RMat = RMat;
  
  for(SceneNode* i:children){
    i->PreProcess(R,this);
  }
  //if(m_primitive->Type == PType::NSphere){
    //Bump* theMat = ((Bump*)m_material);
  glm::dvec4 cands;
  m_primitive->GetCenterSize(cands);
  glm::dvec3 pcenter = glm::dvec3(cands);
  m_material->SetData(pcenter,cands.w);
					
  
}

/*
void GeometryNode::GetPhongColor(glm::dvec4& EYE,glm::dvec4& Ray,glm::dvec3& color,bool& init,const std::list<Light*>& LGT,double* t,std::vector<SceneNode*>& R,size_t NCount)
{
  glm::dvec4 Normal = glm::dvec4();
  glm::dvec4 Intersect = glm::dvec4();
  double DIF;
  //glm::dvec4 ZERO = glm::dvec4(0.0f,0.0f,0.0f,0.0f);
  double newT = m_primitive->GetIntersect(EYE,Ray,Normal);
  PhongMaterial* PM;
  if(newT > 0 && (newT < *t || init == false)){
    init = true;
    if(m_primitive->Type == PType::Sphere){
      std::cout<<"NBfore:"<<glm::to_string(Normal)<<std::endl;
    }
    Normal = glm::normalize(Normal);
    Intersect = Intersect;
    *t = newT;
    PM = (PhongMaterial*) m_material;
    color = PM->m_kd;
    glm::dvec4 L;
    color.x = color.y = color.z = 0.0f;
    for(Light* i: LGT){
      // calculate shadow first
      bool Shadow = false;
      L = glm::dvec4(i->position,1.0f) - Intersect;
      for(uint c = 0; c<NCount;c++){
	if(R[c]->m_nodeType != NodeType::GeometryNode) continue;
	GeometryNode* G = static_cast<GeometryNode*>(R[c]);
	glm::dvec4 N = glm::dvec4();
	glm::dvec4 IN = glm::dvec4();
	double ST = G->m_primitive->GetIntersect(Intersect,L,N);
	Shadow = (ST>0);
	if(Shadow) break;
      }
      if(Shadow) continue;
      // difuse light
      if(m_primitive->Type == PType::Sphere){
	std::cout<<"N:"<<glm::to_string(Normal)<<std::endl;
      }
      double Dist = glm::length(L);
      double DIF = glm::dot(glm::normalize(L),Normal);
      double Fall =  i->falloff[0] +i->falloff[1]*Dist+i->falloff[2]*Dist*Dist;
      PM = (PhongMaterial*) m_material;
      if(DIF > 0){
	color.x += (DIF* PM->m_kd.x  * (i->colour.x))/Fall;
	color.y += (DIF* PM->m_kd.y  * (i->colour.y))/Fall;
	color.z +=(DIF* PM->m_kd.z * (i->colour.z))/Fall;
      }
      // specular light
      
      L = Intersect -  glm::dvec4(i->position,1.0f);
      L = glm::reflect(L,-Normal);
      //L = -L;
      DIF = -glm::dot(glm::normalize(L),glm::normalize(Ray));
      DIF = glm::pow(DIF,PM->m_shininess);
      if(DIF>0){
	color.x += (DIF* PM->m_ks.x  * (i->colour.x));
	color.y += (DIF* PM->m_ks.y  * (i->colour.y));
	color.z += (DIF* PM->m_ks.z  * (i->colour.z));
      }
      
    }
    
  }else{
  }
}
*/
void GeometryNode::GetMaterial(const glm::dvec4& EYE,const glm::dvec4& Ray,glm::dvec4& Normal,bool* init,double* t,SceneNode** Hit,SceneNode* Skip,char RayColor)
{
  glm::dvec4 MyEye = invtrans * EYE;
  glm::dvec4 MyRay = invtrans * Ray;
  SceneNode* FirstHit = *Hit;
  for(SceneNode* i:children){
    i->GetMaterial(MyEye,MyRay,Normal,init,t,Hit,Skip,RayColor);
  }

  // if i'm the skip
  GeometryNode* GNode = (GeometryNode*) Skip;
  if(GNode != this){
    // check with my primitive
    glm::dvec4 N = glm::dvec4();
    double NewT = m_primitive->GetIntersect(MyEye,MyRay,N);
    //if(NewT > 0){
    /*
      std::cout<<"EYE:"<<MyEye.x<<","<<MyEye.y<<","<<MyEye.z<<std::endl;
      std::cout<<"Ray:"<<MyRay.x<<","<<MyRay.y<<","<<MyRay.z<<std::endl;
      std::cout<<NewT<<std::endl;*/
      //}
    //std::cout<<m_name<<":"<<NewT<<std::endl;
    if(NewT > 0 && (*init == false || NewT < *t)){
      *t = NewT;
      Normal = N;
      *init = true;
      *Hit = this;
    }
    if(*Hit != FirstHit){
      // modify normal
      if(m_primitive->Type != PType::Sphere && m_primitive->Type != PType::NSphere){
	Normal = trans * Normal;
      }else{
	Normal = RMat *  glm::inverse(SMat) * Normal;
      }
    }
  }
}


GeometryNode::~GeometryNode(){
  //if(m_material!=nullptr) delete m_material;
  if(m_primitive!=nullptr) delete m_primitive;
}
