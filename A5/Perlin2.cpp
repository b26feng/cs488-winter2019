#include "Perlin2.hpp"

Perlin2::Perlin2():
  Noise(1)
{
  Type = NoiseType::Perlin2;
  for(int i = 0; i<512;i++){
    p[i] = permutation[i%256];
  }

  /*
  Gradients[0] = glm::normalize(glm::dvec2(1.0,2.0));
  Gradients[1] = glm::normalize(glm::dvec2(2.0,1.0));
  Gradients[2] = glm::normalize(glm::dvec2(-1.0,2.0));
  Gradients[3] = glm::normalize(glm::dvec2(-2.0,1.0));
  Gradients[4] = glm::normalize(glm::dvec2(-1.0,-2.0));
  Gradients[5] = glm::normalize(glm::dvec2(-2.0,-1.0));
  Gradients[6] = glm::normalize(glm::dvec2(1.0,-2.0));
  Gradients[7] = glm::normalize(glm::dvec2(2.0,-1.0));
  */

  
  Gradients[0] = glm::dvec2(1.0,2.0);
  Gradients[1] = glm::dvec2(2.0,1.0);
  Gradients[2] = glm::dvec2(-1.0,2.0);
  Gradients[3] = glm::dvec2(-2.0,1.0);
  Gradients[4] = glm::dvec2(-1.0,-2.0);
  Gradients[5] = glm::dvec2(-2.0,-1.0);
  Gradients[6] = glm::dvec2(1.0,-2.0);
  Gradients[7] = glm::dvec2(2.0,-1.0);
  
  /*
  Gradients[0] = glm::dvec2(1.0,1.0);
  Gradients[1] = glm::dvec2(1.0,-1.0);
  Gradients[2] = glm::dvec2(-1.0,1.0);
  Gradients[3] = glm::dvec2(-1.0,-1.0);
  Gradients[4] = glm::dvec2(1.0,1.0);
  Gradients[5] = glm::dvec2(1.0,-1.0);
  Gradients[6] = glm::dvec2(-1.0,1.0);
  Gradients[7] = glm::dvec2(-1.0,-1.0);
  */
}

// finish this tmr
double Perlin2::noise(const glm::dvec4& in){

  // find the grid
  // get the integer part
  int x,y;
  double dx,dy;
  dx = in.x/2;
  dy = in.y/2;
  x = (int)in.x/2;
  y = (int)in.y/2;
  x&=255;
  y&=255;
  
  // get the double part
  dx = dx-((int)dx);
  dy = dy-((int)dy);
  glm::dvec2 pos = glm::dvec2(dx+x,dy+y);
  //dx -= x;
  //dy -= y;
  
  // get 4 pseudo random number to determine 4 gradient vectors on conor
  int h1,h2,h3,h4;
  h1 = Hash(x,y);
  h2 = Hash(x,y+1);
  h3 = Hash(x+1,y);
  h4 = Hash(x+1,y+1);

  glm::dvec2 g1,g2,g3,g4;
  g1 = Gradients[h1&7];
  g2 = Gradients[h2&7];
  g3 = Gradients[h3&7];
  g4 = Gradients[h4&7];

  // get four dots
  double dot1,dot2,dot3,dot4,u,v;
  dot1 = glm::dot(g1,glm::dvec2(pos.x-x,pos.y-y));
  dot2 = glm::dot(g2,glm::dvec2(pos.x-x,pos.y-y-1));
  dot3 = glm::dot(g3,glm::dvec2(pos.x-x-1,pos.y-y));
  dot4 = glm::dot(g4,glm::dvec2(pos.x-x-1,pos.y-y-1));

  // get fade
  u = fade(dx);
  v = fade(dy);
  // interpolate, using fade value of the double part of x,y as t
  return lerp(v,lerp(u,dot1,dot3),lerp(u,dot2,dot4));
}

double Perlin2::fade(double t) const{
  return 6*glm::pow(t,5)-15*glm::pow(t,4)+10*glm::pow(t,3);
}

double Perlin2::lerp(double t,double a,double b) const{
  return a+t*(b-a);
}

int Perlin2::Hash(int x,int y,int z)const{
  return p[p[p[x]+y]+z];
}
