// Winter 2019

#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <time.h>
#include <chrono>

#include "A5.hpp"
#include "Perlin2.hpp"
#include "ColorPrint.hpp"
#include "Random.hpp"

void A4_Render(
	       // What to render  
	       SceneNode * root,

	       // Image to write to, set to a given width and height  
	       Image & image,

	       // Viewing parameters  
	       const glm::vec3 & eye,
	       const glm::vec3 & view,
	       const glm::vec3 & up,
	       double fovy,

	       // Lighting parameters  
	       const glm::vec3 & ambient,
	       const std::list<Light *> & lights
	       ) {
  auto start = std::chrono::high_resolution_clock::now();
  RED();
  root->PrintType();
  DEFAULT();
  glm::dvec3 deye = glm::dvec3(eye);
  glm::dvec3 dview = glm::dvec3(view);
  glm::dvec3 dup = glm::dvec3(up);
  PicTrans RayMat = PicTrans(deye,dview,dup,fovy,image);
  RayMat.GetM();
  //Tracer TRC{&RayMat,root,ambient,lights};
  PathTracer TRC{&RayMat,root,ambient,lights};
  //std::cout<<glm::to_string(Res)<<std::endl;
  // Fill in raytracing code here...
  std::cout << "Calling A4_Render(\n" <<
    "\t" << *root <<
    "\t" << "Image(width:" << image.width() << ", height:" << image.height() << ")\n"
    "\t" << "eye:  " << glm::to_string(eye) << std::endl <<
    "\t" << "view: " << glm::to_string(view) << std::endl <<
    "\t" << "up:   " << glm::to_string(up) << std::endl <<
    "\t" << "fovy: " << fovy << std::endl <<
    "\t" << "ambient: " << glm::to_string(ambient) << std::endl <<
    "\t" << "lights{" << std::endl;

  for(const Light * light : lights) {
    std::cout << "\t\t" <<  *light << std::endl;
  }
  std::cout << "\t}" << std::endl;
  std:: cout <<")" << std::endl;

  auto end = std::chrono::high_resolution_clock::now();
  std::cout<<"Parse Time:"<<std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()<<std::endl;
  start = std::chrono::high_resolution_clock::now();

#ifndef PERLIN_TEST
  TRC.Trace(image,8);
  //TRC.Trace(image,1);
#else
  Perlin2* P = new Perlin2();
  int W,H;
  W = image.width();
  H = image.height();
  double of1,of2,nval;
  of1 = of2 = 0.1;
  glm::dvec4 pos;
  for(int i = 0; i< W;i++){
    for(int j = 0; j<H; j++){
      of1 = RandRange(0,1,1000);
      of2 = RandRange(0,1,1000);
      pos = glm::dvec4(j/32.0,i/32.0,0,0);
      //std::cout<<of1<<","<<of2<<std::endl;
      //pos = glm::dvec4((j/8.0)+of1,(i/8.0)+of2,0,0);
      nval = P->noise(pos);
      //std::cout<<nval<<std::endl;
      image(j,i,0) = image(j,i,1) = image(j,i,2) = P->noise(pos);
    }
  }
  delete P;
#endif
  end = std::chrono::high_resolution_clock::now();
  std::cout<<"Render Time:"<<std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()<<std::endl;
	
  /*
    for (uint y = 0; y < h; ++y) {
    for (uint x = 0; x < w; ++x) {
    // Red: 
    image(x, y, 0) = (double)1.0;
    // Green: 
    image(x, y, 1) = (double)1.0;
    // Blue: 
    image(x, y, 2) = (double)1.0;
    }
    }
  */
  // Delete all lights
  for(Light *l: lights){
    delete l;
  }
}
