// Winter 2019

#pragma once

#include "SceneNode.hpp"
#include "Primitive.hpp"
#include "Material.hpp"
#include "PhongMaterial.hpp"
#include "Light.hpp"

class GeometryNode : public SceneNode {
public:
  GeometryNode( const std::string & name, Primitive *prim, 
		Material *mat = nullptr );
  virtual ~GeometryNode();
  void setMaterial( Material *material );
  virtual void PrintType() override;
  virtual void PreProcess(std::vector<SceneNode*>& R,SceneNode* Parent) override;
  
  //void GetPhongColor(glm::dvec4& EYE,glm::dvec4& Ray,glm::dvec3& color,bool& init,const std::list<Light*>& LGT,double* t,std::vector<SceneNode*>& R,size_t NCount);
  virtual void GetMaterial(const glm::dvec4& EYE,const glm::dvec4& Ray,glm::dvec4& Normal,bool* init,double* t,SceneNode** Hit,SceneNode* Skip=nullptr,char RayColor='W') override;
  
  Material *m_material;
  Primitive *m_primitive;
};
