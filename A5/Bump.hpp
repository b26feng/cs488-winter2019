#pragma once

#include "Texture.hpp"


class Bump: public Texture{
public:
  Bump(const int size,const double Epsilon);

  // here the NewNormal is the intersection
  virtual void PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NewNormal) override;
  
protected:
  
  void ProduceNormal(const glm::dvec4& Normal,const double u,const double v, glm::dvec4& NewNormal) const;
  double GetTan(const double u1, const double v1,const double u2,const double v2) const;
};
