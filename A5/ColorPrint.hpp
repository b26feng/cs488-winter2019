#pragma once

#define ESC 27
#define RED() printf("%c[31m",ESC);
#define YELLOW() printf("%c[33m",ESC);
#define PURPLE() printf("%c[35m",ESC);
#define DEFAULT() printf("%c[39;49m",ESC)
