// Winter 2019

#include "Material.hpp"

Material::Material():
  ReflectCount{0},
  RAttenu(0.3)
{
  Type = MatType::None;
  tex= nullptr;
}

void Material::GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t) const{
}

glm::dvec3 Material::GetColor() const{
  return glm::dvec3(0);
}

glm::dvec3 Material::GetDLColor(const glm::dvec4& Inter,const glm::dvec4& N,const SceneNode* Light,SceneNode* Root) const{
  return glm::dvec3(0);
}

bool Material::IsLight(){
  return false;
}

int Material::GetRC() const{
  return ReflectCount;
}

double Material::GetRA() const { return RAttenu; }

void Material::SetRC(int NewRC){
  ReflectCount = NewRC;
}

void Material::GetReflect(glm::dvec4& InRay,const glm::dvec4& Normal)
{
  InRay = glm::reflect(InRay,Normal);
}

double Material::FresnellRate(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& Refraction,char RayColor){
  return 1.0;
}

void Material::PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal, glm::dvec4& NewNormal)
{
  if(tex!=nullptr){
    tex->PertubNormal(InRay,Normal,NewNormal);
  }else{
    NewNormal = Normal;
  }
}


void Material:: SetData(const:: glm::dvec3& Center,const double radius){
  if(tex!=nullptr){
    tex->SetData(Center,radius);
  }
}

bool Material::GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NextRay){
  NextRay = glm::dvec4(0);
  return false;
}


Material::~Material()
{
  //if(tex!=nullptr) delete tex;
}
