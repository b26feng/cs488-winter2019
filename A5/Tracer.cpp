#include "Tracer.hpp"
#include "GeometryNode.hpp"
#include "PhongMaterial.hpp"
#include "ColorPrint.hpp"
#include "Random.hpp"


#include <iostream>
#include <thread>
#include <functional>
#include <glm/gtx/string_cast.hpp>

#include <tgmath.h>

Tracer::Tracer(PicTrans* P,SceneNode* root, const glm::dvec3& ambient,const std::list<Light *>& lights):
  PT{P},
  Amb{ambient},
  LGT{lights},
  ZERO{0.001}
{
  NodeCount = root->Count();
  RootV = std::vector<SceneNode*>(0);
  root->PreProcess(RootV,nullptr);
  Root = root;
  Background = glm::dvec3(0.0f,0.0f,0.3f);

  m_PrimLight = nullptr;
  // Get 1 light source
  GeometryNode* LightSource = nullptr;
  for(SceneNode* n:root->children){
    LightSource = (GeometryNode*) n;
    if(LightSource && LightSource->m_material->IsLight()){
      m_PrimLight = LightSource;
      break;
    }
  }
  if(m_PrimLight) std::cout<<m_PrimLight->m_name<<std::endl;
  
  m_current_pixel = m_total_pixel = 0;
  m_size_per_thread = 64;
  // init bayer sequence for super sampling
  dim = 3;
  inter = 1.0/dim;
#ifndef XTRANS
#else
  Bayer = new char[dim*dim];
#endif
  
#ifndef XTRANS
#else
  R = 6;
  G = 24;
  B = 6;
  
  char Filter[36] = {'G','B','G','G','R','G',
	   'G','R','G','G','B','G',
	   'G','B','G','G','R','G',
	   'G','R','G','G','B','G',
	   'G','B','G','G','R','G',
	   'G','R','G','G','B','G'};
  
  for(int i = 0;i<dim*dim;i++){
    Bayer[i] = Filter[i];
    //XFilter.emplace_back(glm::dvec3(0));
  }

  
#endif
  /*
  Bayer[0] = 'R';
  Bayer[1] = 'G';
  Bayer[2] = 'G';
  Bayer[3] = 'B';
  */
  // depth of field
#ifndef DOF
  Apeture = -2;
#else
  Apeture = 30;
#endif
  FocalLen = 50;
  FocalDist = 1400;
  SampleNum = 256;

  FPZ = (PT->eye).z - FocalDist;
  
  Range = round(FocalLen/Apeture);


  // soft shadow
  SSCount = 100;

  //srand(time(NULL));
  srand(1);
}

void Tracer::Trace(Image& I,int ThreadNumber){
  m_total_pixel = I.height() * I.width();
  std::thread* Table = new std::thread[ThreadNumber*ThreadNumber];
  if(ThreadNumber > 1){
    for(int i = 0 ; i<ThreadNumber;i++){
      Table[i] = std::thread(&Tracer::RayTrace,this,std::ref(I));
    }
    for(int i = 0;i<ThreadNumber*ThreadNumber;i++){
      Table[i].join();
    }
  }else{
    RayTrace(I);
  }
  delete[] Table;
}

void Tracer::SingleRay(glm::dvec4& Ray,const glm::dvec4& EYE,glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn,bool* Skip,char RayColor){

  glm::dvec4 Normal,Inter,RInter,PNormal,RefRay = Ray;
  glm::dvec3 RFLcolor,RFRcolor;
  SceneNode* HitNode = nullptr;
  GeometryNode* GNode,*BackUp;
  bool IsPlane,IsMirrow,Init;
  IsPlane = IsMirrow = Init = false;
  int RCount;
  double AccRA,t,ReflectT,RefractT;

  // check intersection
  Root->GetMaterial(EYE,Ray,Normal,&Init,&t,&HitNode);
  
  // if hit anything
  if(Init){
    Normal = glm::normalize(Normal);
    // get Intersection
    ReflectT = t-ZERO;
    RefractT = t+ZERO;
    Inter = EYE + glm::dvec4((ReflectT*Ray.x),(ReflectT*Ray.y),(ReflectT*Ray.z),0.0f);
    RInter = EYE + glm::dvec4((RefractT*Ray.x),(RefractT*Ray.y),(RefractT*Ray.z),0.0f);
    GNode = (GeometryNode*) HitNode;
    BackUp = GNode;
    if(*Primary){
      double Ratio = FPZ/Inter.z;
      // this is to get the intersection with the focal plane
      FocalIn = EYE + glm::dvec4(Inter.x*Ratio,Inter.y*Ratio,Inter.z*Ratio,0.0f);
      //FocalIn = Inter;
    }

    MatType mtype = GNode->m_material->Type;

    PNormal = EYE + glm::dvec4((t*Ray.x),(t*Ray.y),(t*Ray.z),0.0f);
    GNode->m_material->PertubNormal(Ray,Normal,PNormal);

    // check if go in or out
    
    IsMirrow = (mtype == MatType::Mirrow || mtype == MatType::Transparent); 
    if(!(GNode->m_material->Type == MatType::FresPhong)){
      GetColor(Inter,PNormal, Ray,color,GNode,nullptr,RayColor);
    }
    
    
    // Determin the weight of reflection and refraction
    double WRFL,WRFR;
    WRFL = WRFR = 0;
    glm::dvec4 RefractRay = glm::dvec4(0.0);
    WRFL = GNode->m_material->FresnellRate(RefRay,PNormal,RefractRay,RayColor);
    RFLcolor = glm::dvec3(0.0f);
    RFRcolor = glm::dvec3(0.0f);
    // Reflection
    
    GNode->m_material->GetReflect(Ray,PNormal);
    RecursiveTrace(Inter,Ray,0,GNode,RFLcolor,RayColor);
    RFLcolor.x *= WRFL;
    RFLcolor.y *= WRFL;
    RFLcolor.z *= WRFL;
    
    // Refraction
    
    if(WRFL < 1.0){
      
	WRFR = 1.0 - WRFL;
      // if fresnell phong
      if(GNode->m_material->Type == MatType::FresPhong){
	GetColor(RInter,PNormal,RefractRay,RFRcolor,GNode,nullptr,RayColor);
      }else{
      
	RecursiveTrace(RInter,RefractRay,0,GNode,RFRcolor,RayColor);
      }
	RFRcolor.x *= WRFR;
	RFRcolor.y *= WRFR;
	RFRcolor.z *= WRFR;
    }
    color += RFRcolor + RFLcolor;
    if(!IsMirrow){
      color += Amb;
    }
  }else{
    color = Background;
  }
}

void Tracer::RecursiveTrace(const glm::dvec4& Origin,const glm::dvec4& Ray,const int RCount,GeometryNode* CurrentNode,glm::dvec3& color,char RayColor){
  SceneNode* HitNode;
  GeometryNode* GNode;
  double AccRA = CurrentNode->m_material->GetRA();
  glm::dvec3 Rcolor = glm::dvec3(0.0);
  int Limit = CurrentNode->m_material->GetRC();
  bool Skip = Limit-RCount == 1;
  bool Water = CurrentNode->m_material->Type == MatType::Transparent;
  Water = Water && CurrentNode->m_primitive->Type == PType::Plane;
  glm::dvec4 PNormal,RefRay = Ray;
  double WRFL,WRFR;
  WRFL = WRFR = 0.0;
  if(Limit - RCount > 0){
    bool Init = false;
    double t,ReflectT,RefractT;
    t = ReflectT = RefractT = 0.0;
    glm::dvec4 RFLInter,RFRInter,RFRRay,RealRay;
    glm::dvec3 RFRcolor = glm::dvec3(0.0);
    glm::dvec3 RFLcolor = glm::dvec3(0.0);
    glm::dvec4 Normal = glm::dvec4(0.0);
    // setup rays to indicate whether in air or material
    RealRay = Ray;
    RealRay.w = 0.0;
    // skip itself when the last time tracing
    if(Skip){
      Root->GetMaterial(Origin,RealRay,Normal,&Init,&t,&HitNode,CurrentNode);
    }else{
      Root->GetMaterial(Origin,RealRay,Normal,&Init,&t,&HitNode);
    }
    GNode = (GeometryNode*)HitNode;
      
    // if hit something, get its color
    if(Init){
      Normal = glm::normalize(Normal);
      // if the ray is from inside the object to outside
      //if(Ray.w < 0 && GNode == CurrentNode) Normal = - Normal;
      
      // get intersections for refract and reflect
      ReflectT = t - ZERO;
      RefractT = t + ZERO;
      RFLInter = Origin + glm::dvec4((ReflectT*Ray.x),(ReflectT*Ray.y),(ReflectT*Ray.z),0.0f);
      RFRInter = Origin + glm::dvec4((RefractT*Ray.x),(RefractT*Ray.y),(RefractT*Ray.z),0.0f);


      // Get Pertubed Normal, prepar PNormal as intersection, bump mappping need this
      PNormal = Origin + glm::dvec4((t*Ray.x),(t*Ray.y),(t*Ray.z),0.0f);
      GNode->m_material->PertubNormal(RealRay,Normal,PNormal);

      if(!(GNode->m_material->Type == MatType::FresPhong)){
	if(Skip){
	  GetColor(RFLInter,PNormal,Ray,Rcolor,GNode,CurrentNode,RayColor);
	}else{
	  
	  GetColor(RFLInter,PNormal,Ray,Rcolor,GNode,nullptr,RayColor);
	}
      }
      // Get ambient if not a mirrow Color
      MatType mtype = GNode->m_material->Type;
      if(mtype != MatType::Mirrow && mtype != MatType::Transparent){
	Rcolor += Amb;
      }
      // Get Fresnell Coefficient
      WRFL = GNode->m_material->FresnellRate(RefRay,PNormal,RFRRay,RayColor);

      // Trace for Next Reflection
      
      GNode->m_material->GetReflect(RealRay,PNormal);
      RealRay.w = Ray.w;
      RFLcolor = glm::dvec3(0.0);
      RFRcolor = glm::dvec3(0.0);
      
      RecursiveTrace(RFLInter,RealRay,RCount+1,GNode,RFLcolor,RayColor);
	
      RFLcolor.x *= WRFL;
      RFLcolor.y *= WRFL;
      RFLcolor.z *= WRFL;
      
      
      // Refraction
      
      if(WRFL < 1.0){
	WRFR = 1.0 - WRFL;
	if(GNode->m_material->Type == MatType::FresPhong){
	  GetColor(RFRInter,PNormal,RFRRay,RFRcolor,GNode,nullptr,RayColor);
	}else{
	  RecursiveTrace(RFRInter,RFRRay,RCount+1,GNode,RFRcolor,RayColor);
	}
	
	RFRcolor.x *= WRFR;
	RFRcolor.y *= WRFR;
	RFRcolor.z *= WRFR;
      }

      
     
      
      Rcolor += RFLcolor + RFRcolor;
      //if(Rcolor == glm::dvec3(0.0) && CurrentNode->m_material->GetRC() - RCount == 1 && Ray.w < 0.0) Rcolor = glm::dvec3(0.0,1.0,0.0);
    }else{
      // if didn't hit anything ,get background color
      Rcolor = Background;
    }
  }
  Rcolor.x *= AccRA;
  Rcolor.y *= AccRA;
  Rcolor.z *= AccRA;
  if(RefRay.w < 0 && Water){
    //std::cout<<WRFL<<","<<RCount<<std::endl;
    //color += glm::dvec3(0,0.359375,0.68359375)+Amb;
    color += Amb;
  }else{
    color += Rcolor;
  }
}

void Tracer::SinglePixel(double x,double y,const glm::dvec4& EYE,glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn,bool* Skip,char RayColor){
  glm::dvec4 Screen,Ray;
  // get a ray
#ifdef CHECK
  if(CHECK){
#endif
  Screen = PT->M*glm::dvec4(x,y,0.0f,1.0f);
  Ray =  Screen-EYE;
  
  SingleRay(Ray,EYE,color,Primary,FocalIn,Skip,RayColor);
  
#ifdef CHECK
  }
  if(CHECK) color = glm::dvec3(1.0,0.0,0.0);
#endif
}

void Tracer::BayerPixel(double x,double y,const glm::dvec4& EYE,glm::dvec3& color,bool* Primary,glm::dvec4& FocalIn){
#ifndef XTRANS
  glm::dvec3 AccColor = glm::dvec3(0.0f);
  //glm::dvec3 AccColor += color;
#else
  double RC,GC,BC;
  std::vector<glm::dvec3> XFilter;
  glm::dvec3 avgcolor;
  RC=GC=BC=0.0;
#endif
  for(uint i = 0;i<dim;i++){
    for(uint j = 0; j<dim;j++){
      color = glm::dvec3(0.0f);
      //SinglePixel(x+(i *inter),y+(j*inter),EYE,color,Primary,FocalIn,RayColor);
#ifndef XTRANS
       SinglePixel(x+(i*inter),y+(j*inter),EYE,color,Primary,FocalIn);
       AccColor += color;
      
#else
       SinglePixel(x+(i*inter),y+(j*inter),EYE,color,Primary,FocalIn,Bayer[i*dim+j]);
       //XFilter.emplace_back(color.x);
      
      switch(Bayer[i*dim+j]){
      case 'R':
	RC += color.x;
	break;
      case 'G':
	GC += color.y;
	break;
      case 'B':
	BC += color.z;
	break;
      }
     
#endif
    }
  }
#ifndef XTRANS
  color= AccColor;
  color.x/=(dim*dim);
  color.y/=(dim*dim);
  color.z/=(dim*dim);
#else
  //Avg(XFilter,avgcolor);
  //color = avgcolor;
  color.x = RC/R;
  color.y = GC/G;
  color.z = BC/B;
  
#endif
}

void Tracer::RayTrace(Image& image){
  size_t h = image.height();
  size_t w = image.width();
  glm::dvec3 AccColor,color;
  glm::dvec4 EYE,FakeEYE,FakeRay,FocalIn;
  bool Primary,Blur;
  EYE = glm::dvec4(PT->eye,1.0f);
  //double W,H,HW,HH;
  //HW=HH = (-(PT->Dist))*glm::tan(glm::radians(PT->fovy/2));
  //W = H = 2*HW;
  while(true){
    // Get the current index and reserve the pixels for current thread.
    // Update the current index.
    m_mtx.lock();
    if(m_current_pixel == m_total_pixel - 1){
      m_mtx.unlock();
      break;
    }
    size_t start = m_current_pixel;
    uint loops = ((m_total_pixel-1-start)>m_size_per_thread)? m_size_per_thread:(m_total_pixel-1-start);
    m_current_pixel+=loops;
    m_mtx.unlock();
    
    // Convert index to x,y
    for(int i = 0;i<loops;i++){
      uint x = (start+i)/w;
      uint y = (start+i)%w;
      
      //
      //if(CHECK) { Check = true; } else { Check = false; }
    
      Primary = Apeture > 0;
      color = glm::dvec3(0.0f);
#ifndef ANTI_ALIAS
      SinglePixel(x,y,EYE,color,&Primary,FocalIn);
#else
      //RC=GC=BC=0.0f;
      BayerPixel(x,y,EYE,color,&Primary,FocalIn);
#endif
      if(Primary){
	//std::cout<<glm::to_string(FocalIn)<<std::endl;
	Primary = false;
	int XOff,YOff;
	AccColor = color;
	for(uint i = 0;i<SampleNum;i++){
	  color = glm::dvec3(0.0f);
	
	  color = glm::dvec3(0.0f);
	  XOff = rand()%Range + 1;
	  XOff = (XOff %2)? -XOff:XOff;
	  YOff = rand()%Range + 1;
	  YOff = (YOff %2)? -YOff:YOff;
	  FakeEYE = glm::dvec4(EYE.x+XOff,EYE.y+YOff,EYE.z,1.0f);
	  //std::cout<<glm::to_string(EYE)<<std::endl;
	  //std::cout<<glm::to_string(FakeEYE)<<std::endl;
	  FakeRay = FocalIn - FakeEYE;
	  
	  //std::cout<<glm::to_string(glm::normalize(FakeRay)-glm::normalize(RecordRay))<<std::endl;
	  SingleRay(FakeRay,FakeEYE,color,&Primary,FocalIn);
	  //SinglePixel(x-2+i,y-2+j,EYE,color);
	  AccColor += color;
	}
	color.x = AccColor.x / (SampleNum + 1);
	color.y = AccColor.y / (SampleNum + 1);
	color.z = AccColor.z / (SampleNum + 1);
      }
      /*if(Check){
	color = glm::dvec3(1.0f,0.0f,0.0f);
	}*/
      image(x,y,0) = color.x;
      image(x,y,1) = color.y;
      image(x,y,2) = color.z;
      //SingleRay(x,y,EYE,color);
      // Get the world coordinate of current pixel, and calculate ray
      //FS = glm::dvec4(HH-H*((double)(h-x))/h,HW-W*((double)y)/w,PT->Dist,1.0f);
    
    }
    //Background.x += 0.002f;
    //Background.z += 0.001f;
  }
}

void  Tracer::GetColor(const glm::dvec4& Intersect,const glm::dvec4& Normal,const glm::dvec4& EyeRay,glm::dvec3& color,const GeometryNode* SelfNode,SceneNode* Skip,char RayColor){
  bool HitObj = false;
  double t = 0;
  glm::dvec4 SecNormal,Ray,ObjIn;
  SceneNode* HitNode;
  GeometryNode* GNode;
  Material* m = SelfNode->m_material;
  for(Light* L: LGT){
    // test if primary hits light source
    Ray = glm::dvec4(L->position,1.0f) - Intersect;
    //std::cout<< "Origin:"<<Intersect.x<<","<<Intersect.y<<","<<Intersect.z<<std::endl;
    //std::cout<<"Shade Ray:"<<Ray.x<<","<<Ray.y<<","<<Ray.z<<std::endl;
    Root->GetMaterial(Intersect,Ray,SecNormal,&HitObj,&t,&HitNode,Skip,RayColor);
    //std::cout<<t<<","<<HitObj<<std::endl;
    GNode = (HitObj)? (GeometryNode*)HitNode:nullptr;
    // test for infinit hit
    if(HitObj) HitObj = (t<1 && (GNode != SelfNode || t>0.0001));
    // test for transparent obj
    if(HitObj) HitObj = (GNode->m_material->Type != MatType::Transparent);
    //
    // if soft shadow
    if((L->xrange > 0 || L->yrange > 0 || L->zrange >0)){
      glm::dvec3 offset;
      int hits = 0;
      //bool set = false;
      glm::dvec3 accColor = glm::dvec3(0.0);
      double ratio;
      for(int i = 0; i<SSCount;i++){
	t = 0;
	HitObj = false;
	
	// get random point within the light source box
	//offset = glm::dvec3(RandRange(0,L->xrange,1),RandRange(0,L->yrange,1),RandRange(0,L->zrange,10));
	offset = glm::dvec3(RandRange(0,L->xrange,1),RandRange(0,L->yrange,1),0);
	
	// test if hit
	Ray = glm::dvec4(L->position+offset,1.0f) - Intersect;
	Root->GetMaterial(Intersect,Ray,SecNormal,&HitObj,&t,&HitNode,Skip,RayColor);
	//HitObj = (HitObj)? t<(1+0.0001):HitObj;
	// test for infinit hit
	if(HitObj) HitObj = (t<1 && (GNode != SelfNode || t>0.0001));
	// test for transparent obj
	if(HitObj) HitObj = (GNode->m_material->Type != MatType::Transparent);
    
	m->GetColor(EyeRay,Intersect,Ray,Normal,accColor,L,HitObj,t);
      }
      color += glm::dvec3(accColor.x/((double)SSCount),accColor.y/((double)SSCount),accColor.z/((double)SSCount));
    }else{
      
      //if(HitObj) HitObj = glm::length(Intersect+glm::dvec4(Ray.x*t,Ray.y*t,Ray.z*t,0.0f))<glm::length(Ray);
      // prevent self hitting
      m->GetColor(EyeRay,Intersect,Ray,Normal,color,L,HitObj,t);
    }
    HitObj = false;
    t = 0;
  }
}

#ifndef XTRANS
#else
void Tracer::Avg(std::vector<glm::dvec3>& Filter,glm::dvec3& Dest){
  // interpolate green for red and blue
  double up1,down1,up2,down2;
  for(int i = 0; i<6;i++){
    // interpolation of green
    Filter[1+i*6].y = (Filter[0+i*6].y + Filter[2+i*6].y)/2;
    Filter[4+i*6].y = (Filter[3+i*6].y + Filter[5+i*6].y)/2;
    // interpolation of red or blue
    if(Bayer[1+i*6] == 'R'){
      if(((1+(i-1)*6) < 0)){
	up1 = up2 = 0;
      }else{
	up1 = Filter[1+(i-1)*6].z;
	up2 = Filter[4+(i-1)*6].x;
      }
      if((1+(i+1)*6) > 36){
	down1 = down2 = 0;
      }else{
	down1 = Filter[1+(i+1)*6].z;
	down2 = Filter[4+(i+1)*6].x;
      }
      Filter[1+i*6].z = (up1+down1)/2;
      Filter[4+i*6].x = (up2+down2)/2;
    }else{
      if(((1+(i-1)*6) < 0)){
	up1 = up2 = 0;
      }else{
	up1 = Filter[1+(i-1)*6].x;
	up2 = Filter[4+(i-1)*6].z;
      }
      if((1+(i+1)*6) > 36){
	down1 = down2 = 0;
      }else{
	down1 = Filter[1+(i+1)*6].x;
	down2 = Filter[4+(i+1)*6].z;
      }
      Filter[1+i*6].x = (up1+down1)/2;
      Filter[4+i*6].z = (up2+down2)/2;
    }
  }
  // interpolate green
  for(int i = 0;i<6;i++){
    for(int j = 0;j < 6;j++){
      if(Bayer[i*6+j] != 'G') continue;
      switch(j){
      case 0:
	  Filter[i*6+j].x = Filter[i*6+j+1].x/2;
	  Filter[i*6+j].z = Filter[i*6+j+1].z/2;
	  break;
      case 2:
	Filter[i*6+j].x = (2.0/3) * Filter[i*6+j-1].x;
	Filter[i*6+j].z = (2.0/3) * Filter[i*6+j-1].z;
	Filter[i*6+j].x = (1.0/3) * Filter[i*6+j+2].x;
	Filter[i*6+j].z = (1.0/3) * Filter[i*6+j+2].z;
	break;
      case 3:
	Filter[i*6+j].x = (2.0/3) * Filter[i*6+j+1].x;
	Filter[i*6+j].z = (2.0/3) * Filter[i*6+j+1].z;
	Filter[i*6+j].x = (1.0/3) * Filter[i*6+j-2].x;
	Filter[i*6+j].z = (1.0/3) * Filter[i*6+j-2].z;
	break;
      case 5:
	Filter[i*6+j].x = Filter[i*6+j-1].x/2;
	Filter[i*6+j].z = Filter[i*6+j-1].z/2;
      }
      
    }
  }
  glm::dvec3 finalcolor = glm::dvec3(0);
  for(int i = 0;i<36;i++){
    finalcolor += Filter[i];
  }
  finalcolor.x /= 36;
  finalcolor.y /= 36;
  finalcolor.z /= 36;
  Dest = finalcolor;
}

#endif

Tracer::~Tracer(){
#ifndef XTRANS
#else
  delete[] Bayer;
#endif
}
