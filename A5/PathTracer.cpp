#include "PathTracer.hpp"
#include "Random.hpp"
#include "Constant.hpp"

#include <thread>
#include <functional>
#include <iostream>

//#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>
PathTracer::PathTracer(PicTrans* P,SceneNode* root, const glm::dvec3& ambient,const std::list<Light *>& lights):
  Tracer(P,root,ambient,lights)
{
  m_SkyLight = glm::dvec3(0.6,0.6,1.0);
  m_size_per_thread = 64;
}

void PathTracer::Trace(Image& I,int ThreadNumber){
  m_total_pixel = I.height() * I.width();
  std::thread* Table = new std::thread[ThreadNumber];
  if(ThreadNumber > 0){
    for(int i = 0; i< ThreadNumber; i++){
      Table[i] = std::thread(&PathTracer::RayTrace,this,std::ref(I));
    }
    for(int i = 0; i<ThreadNumber;i++){
      Table[i].join();
    }
  }else{
    RayTrace(I);
  }
  delete[] Table;
}

void PathTracer::RayTrace(Image& image){
  size_t h = image.height();
  size_t w = image.width();
  glm::dvec3 AccColor,color;
  glm::dvec4 EYE,FakeEYE,FakeRay,FocalIn;
  bool Primary,Blur;
  EYE = glm::dvec4(PT->eye,1.0f);
  //double W,H,HW,HH;
  //HW=HH = (-(PT->Dist))*glm::tan(glm::radians(PT->fovy/2));
  //W = H = 2*HW;
  while(true){

    m_mtx.lock();
    if(m_current_pixel == m_total_pixel - 1){
      m_mtx.unlock();
      break;
    }
    size_t start = m_current_pixel;
    uint loops = ((m_total_pixel - 1 - start) > m_size_per_thread)? m_size_per_thread:(m_total_pixel-1-start);
    m_current_pixel += loops;
    m_mtx.unlock();

    for(int i = 0; i<loops; i++){
      uint x = (start+i)/w;
      uint y = (start+i)%w;    
      //if(CHECK) { Check = true; } else { Check = false; }
      
      Primary = Apeture > 0;
      color = AccColor = glm::dvec3(0.0f);
#ifndef ANTI_ALIAS
      // Anti Alias
      int threshold = 8;
      double interval = 1.0/threshold;
      // Do the adaptive check. If none of the 4
      // corner hits any non-light object, don't do further
      // sampling.
      
      
      for(int i = 0;i<threshold;i++){
	for(int j = 0;j<threshold;j++){
	  SinglePixel(x+i*interval,y+j*interval,EYE,color,&Primary,FocalIn);
	  AccColor += color;
	}
      }
      color = AccColor * glm::dvec3(1.0/((threshold)*(threshold)));
	//color = 0.5 * color;
      
	//color = AccColor * glm::dvec3(1.0/4);
      
#else
      //RC=GC=BC=0.0f;
      BayerPixel(x,y,EYE,color,&Primary,FocalIn);
#endif
      if(Primary){
	//std::cout<<glm::to_string(FocalIn)<<std::endl;
	Primary = false;
	int XOff,YOff;
	AccColor = color;
	for(uint i = 0;i<SampleNum;i++){
	  color = glm::dvec3(0.0f);
	  
	  color = glm::dvec3(0.0f);
	  XOff = rand()%Range + 1;
	  XOff = (XOff %2)? -XOff:XOff;
	  YOff = rand()%Range + 1;
	  YOff = (YOff %2)? -YOff:YOff;
	  FakeEYE = glm::dvec4(EYE.x+XOff,EYE.y+YOff,EYE.z,1.0f);
	  //std::cout<<glm::to_string(EYE)<<std::endl;
	  //std::cout<<glm::to_string(FakeEYE)<<std::endl;
	  FakeRay = FocalIn - FakeEYE;
	    
	  //std::cout<<glm::to_string(glm::normalize(FakeRay)-glm::normalize(RecordRay))<<std::endl;
	  SingleRay(FakeRay,FakeEYE,color,&Primary,FocalIn);
	  //SinglePixel(x-2+i,y-2+j,EYE,color);
	  AccColor += color;
	}
	color.x = AccColor.x / (SampleNum + 1);
	color.y = AccColor.y / (SampleNum + 1);
	color.z = AccColor.z / (SampleNum + 1);
      }
      image(x,y,0) = color.x;
      image(x,y,1) = color.y;
      image(x,y,2) = color.z;
    }
    //Background.x += 0.002f;
    //Background.z += 0.001f;
  }
}

void PathTracer::SingleRay(glm::dvec4& Ray, const glm::dvec4& EYE, glm::dvec3& color,bool* Primary, glm::dvec4& FocalIn,bool* Skip, char RayColor){
  glm::dvec4 Normal,Next,Inter,RInter,PNormal,RefRay = Ray;
  glm::dvec3 axis,u,RFLcolor,RFRcolor=glm::dvec3(0);
  SceneNode* HitNode = nullptr;
  GeometryNode* GNode;
  //;
  bool IsPlane,IsMirrow,Init,Light;
  IsPlane = IsMirrow = Init = false;
  int RCount;
  double AccRA,t,ReflectT,RefractT,r1,deg3;

  // check intersection
  Root->GetMaterial(EYE,Ray,Normal,&Init,&t,&HitNode);
  
  // if hit anything
  if(Init){
    Normal = glm::normalize(Normal);
    // get Intersection
    ReflectT = t-ZERO;
    RefractT = t+ZERO;
    Inter = EYE + glm::dvec4((ReflectT*Ray.x),(ReflectT*Ray.y),(ReflectT*Ray.z),0.0f);
    RInter = EYE + glm::dvec4((RefractT*Ray.x),(RefractT*Ray.y),(RefractT*Ray.z),0.0f);
    GNode = (GeometryNode*) HitNode;

    if(*Primary){
      double Ratio = FPZ/Inter.z;
      // this is to get the intersection with the focal plane
      FocalIn = EYE + glm::dvec4(Inter.x*Ratio,Inter.y*Ratio,Inter.z*Ratio,0.0f);
      //FocalIn = Inter;
    }

    // Get pertubed normal
    PNormal = EYE + glm::dvec4((t*Ray.x),(t*Ray.y),(t*Ray.z),0.0f);
    GNode->m_material->PertubNormal(Ray,Normal,PNormal);
    // Get Self Color
    color = GNode->m_material->GetColor();
    bool UseRefract= GNode->m_material->GetNextRay(Ray,PNormal,Next);
    double weight = 1;
    //Next.w = 0;
    Light = GNode->m_material->IsLight();
    // Get the next Ray to trace if it's not a light.
    if(!Light){
#ifndef PATH
      glm::dvec3 dir_color = glm::dvec3(0);
      
      dir_color = GNode->m_material->GetDLColor(Inter,PNormal,m_PrimLight,Root);
#ifdef DLONLY
      color = dir_color;
#endif
#endif

#ifndef DLONLY
      if(UseRefract){
	color = weight * color * PathTrace(Next,RInter,0);
      }else if(GNode->m_material->Type == MatType::Phong){
#ifdef PATH
	color = weight * color *  PathTrace(Next,Inter,0);
#else
	color = weight * color *  PathTrace(Next,Inter,0) + dir_color;
#endif
	// Mirrow and Glass with reflective ray
      } else {
	color = weight * color * PathTrace(Next,Inter,0);
      }
#endif 
    }
    //color =glm::dvec3(RFRcolor.x/threshold,RFRcolor.y/threshold,RFRcolor.z/threshold);
    
    //AccRA = GNode->m_material->GetRA();
    //color = color * AccRA;
    //MatType mtype = GNode->m_material->Type;

    //
    /*
    // check if go in or out
    if(PNormal.w < 0){
      PNormal.w = 0;
      RefRay.w = -1;
    }
    
    IsMirrow = (mtype == MatType::Mirrow || mtype == MatType::Transparent); 
    if(!(GNode->m_material->Type == MatType::FresPhong)){
      GetColor(Inter,PNormal, Ray,color,GNode,nullptr,RayColor);
    }
    
    
    // Determin the weight of reflection and refraction
    double WRFL,WRFR;
    WRFL = WRFR = 0;
    glm::dvec4 RefractRay = glm::dvec4(0.0);
    WRFL = GNode->m_material->FresnellRate(RefRay,PNormal,RefractRay,RayColor);
    RFLcolor = glm::dvec3(0.0f);
    RFRcolor = glm::dvec3(0.0f);
    // Reflection
    
    GNode->m_material->GetReflect(Ray,PNormal);
    RecursiveTrace(Inter,Ray,0,GNode,RFLcolor,RayColor);
    RFLcolor.x *= WRFL;
    RFLcolor.y *= WRFL;
    RFLcolor.z *= WRFL;
    
    // Refraction
    
    if(WRFL < 1.0){
      
	WRFR = 1.0 - WRFL;
      // if fresnell phong
      if(GNode->m_material->Type == MatType::FresPhong){
	GetColor(RInter,PNormal,RefractRay,RFRcolor,GNode,nullptr,RayColor);
      }else{
      
	RecursiveTrace(RInter,RefractRay,0,GNode,RFRcolor,RayColor);
      }
	RFRcolor.x *= WRFR;
	RFRcolor.y *= WRFR;
	RFRcolor.z *= WRFR;
    }
    color += RFRcolor + RFLcolor;
    if(!IsMirrow){
      color += Amb;
    }
    */
  }else{
    // Sky Light
    deg3 = 0.5*(Ray.y + 1.0);
    color = glm::dvec3(1-deg3) + glm::dvec3(0.5*deg3,0.7*deg3,deg3);
    if(Skip) *Skip = true;
  }
}

glm::dvec3 PathTracer::PathTrace(glm::dvec4& Ray, const glm::dvec4& EYE, int Count){

  GeometryNode* GNode;
  bool Light, Init = false;
  SceneNode* HitNode = nullptr;
  double t,ReflectT,RefractT,r1,RoulletteProb,deg,RA;
  glm::dvec4 Inter,RInter,Normal,Next,PNormal,RefRay = Ray;
  glm::dvec3 color,self_color,axis,u,dir_color;
  //;
  // Check Intersection.
  Root->GetMaterial(EYE,Ray,Normal,&Init,&t,&HitNode);

  // If Hit
  if(Init){
    // Russian Roullette
    // Get the roullette threshold
    GNode = (GeometryNode*) HitNode;
    Light = GNode->m_material->IsLight();
    self_color = GNode->m_material->GetColor();
    
    // Get Pertubed Normal.
    PNormal = EYE + glm::dvec4((t*Ray.x),(t*Ray.y),(t*Ray.z),0.0f);
    Normal = glm::normalize(Normal);
    GNode->m_material->PertubNormal(Ray,Normal,PNormal);
    
    // Calculate Reflection and Refraction intersection
    ReflectT = t-ZERO;
    RefractT = t+ZERO;
    Inter = EYE + glm::dvec4(ReflectT*Ray.x,ReflectT*Ray.y,ReflectT*Ray.z,0.0);
    RInter = EYE + glm::dvec4((RefractT*Ray.x),(RefractT*Ray.y),(RefractT*Ray.z),0.0f);
    /*
    if(!Light){
#ifndef PATH
      dir_color = GNode->m_material->GetDLColor(Inter,PNormal,m_PrimLight,Root);
#endif
    }else return self_color;
    */
    if(Light) return self_color;
    
    RA = GNode->m_material->GetRA();
    //RA = 1;
    
    
    /*
     * Global Illumination
     */
    RoulletteProb = std::max(std::max(self_color.x,self_color.y),self_color.z);
    

    // r1 = RandRange(0,1,100);
    //if(Count > 6 && GNode->m_material->Type!=MatType::Transparent){
    if(Count > 6){
      
      //if(r1 > RoulletteProb){
	//std::cout<<"GG"<<std::endl;
 

#ifdef BDPT
      return dir_color + RA * LightTrace(Inter,m_PrimLight);
#else
#ifndef PATH
      dir_color = GNode->m_material->GetDLColor(Inter,PNormal,m_PrimLight,Root);
      return RA * dir_color;
#endif
      return RA * self_color;
#endif
      //return RA * self_color * LightTrace(Inter,m_PrimLight);
      //return glm::dvec3(0);
	//}
	//}
      //return RA * self_color;
    }
    
    // Get the real SelfColor
    color = RA * self_color;
    bool UseRefract= GNode->m_material->GetNextRay(Ray,PNormal,Next);
    double weight = 1;
    //Next.w = 0;
    // Trace the next ray
    if(!Light){
      // If it's glass and use refractive ray.
      if(UseRefract){
	color = weight * color *  PathTrace(Next,RInter,Count);
	// If it's glass or mirrow using reflective ray.
      }else if(GNode->m_material->Type == MatType::Phong){
#ifndef PATH
	dir_color = GNode->m_material->GetDLColor(Inter,PNormal,m_PrimLight,Root);
	color = weight * color *  PathTrace(Next,Inter,Count+1) + dir_color;
#else
	color = weight * color *  PathTrace(Next,Inter,Count+1);
#endif	
      }else{
	color = weight * color *  PathTrace(Next,Inter,Count);
      }
    }
    
    return color;
    
  }else{
    // If the Ray is NaN, select a default light
    if(Ray.y != Ray.y){
      deg = 0.5*(0.5+1.0);
    }else{
      deg = 0.5*(Ray.y + 1.0);
    }
    color = glm::dvec3(1-deg) + glm::dvec3(0.5*deg,0.7*deg,deg);
    return color;
  }
}

glm::dvec3 PathTracer::LightTrace(const glm::dvec4& Inter,GeometryNode* L){
  glm::dvec4 LightSource,LightNormal,HitNormal,PNormal;
  glm::dvec4 Ray,UnitRay,RefrInter,ReflInter,Next;
  glm::dvec3 self_color,color,dir_color;
  double TargetT,ActualT,ReflectT,RefractT;
  SceneNode* HitNode = nullptr;
  bool Init = false;
 
  m_PrimLight->m_primitive->GetRandomPoint(LightSource,LightNormal);
  
  
  Ray = UnitRay = Inter-LightSource;
  UnitRay = glm::normalize(UnitRay);
  TargetT = Ray.x/UnitRay.x;
  LightSource = LightSource + ZERO * UnitRay;
  // Check if the point hits any other points
  Root->GetMaterial(LightSource,UnitRay,HitNormal,&Init,&ActualT,&HitNode);

  //Get Self Color
  self_color = L->m_material->GetRA() * L->m_material->GetColor();
    
  // If hit something before hiting Inter
  // Trace the light
  if(Init && (ActualT < TargetT - ZERO)){
    
    
    // Get Intersection
    HitNormal = glm::normalize(HitNormal);
    ReflectT = ActualT-ZERO;
    RefractT = ActualT + ZERO;
    ReflInter = LightSource + glm::dvec4(ReflectT) * UnitRay;
    RefrInter = LightSource + glm::dvec4(RefractT) * UnitRay;
    GeometryNode* GNode = (GeometryNode*) HitNode;
    
    // Perturb normal, Get Direct Illum Color, Generate next ray
    PNormal = LightSource + glm::dvec4(ActualT)*UnitRay;
    GNode->m_material->PertubNormal(UnitRay,HitNormal,PNormal);
    //dir_color = GNode->m_material->GetDLColor(ReflInter,PNormal,m_PrimLight,Root);
    if(GNode->m_material->GetNextRay(UnitRay,PNormal,Next)){
      color  = LightPath(Inter,Next,HitNormal,RefrInter,0);
    }else{
      color = LightPath(Inter,Next,HitNormal,ReflInter,0);
    }
    color = color * GNode->m_material->GetColor();
    // If Phong material, return direct lighting 
    if(GNode->m_material->Type == MatType::Phong){
      dir_color = GNode->m_material->GetDLColor(ReflInter,PNormal,m_PrimLight,Root);
      return dir_color + color;
    }
    
    return color;
    
    //return self_color;
    //std::cout<<ActualT<<","<<TargetT<<std::endl;
    //return glm::dvec3(0);
  }else{
    //std::cout<<ActualT<<","<<TargetT<<std::endl;
    
    return self_color;
  }
  
}

glm::dvec3 PathTracer::LightPath(const glm::dvec4& Target,const glm::dvec4& Normal,glm::dvec4& Ray,glm::dvec4& Inter,int Count){
  glm::dvec4 LightSource,LightNormal,HitNormal,PNormal;
  glm::dvec4 TargetRay,UnitRay,RefrInter,ReflInter,Next;
  glm::dvec3 dir_color,self_color,color;
  double TargetT,ActualT,t,ReflectT,RefractT,RA,RProb,r1;
  GeometryNode* GNode;
  //;
  SceneNode* HitNode = nullptr;
  bool Init, Init2, Light, UseRefract;
  Init = Init2 = false;
  // Trace next
  Root->GetMaterial(Inter,Ray,HitNormal,&Init,&t,&HitNode);
  
  // If hit anything 
  // Trace the light
  if(Init){
    
    //Get Self Color
    GNode = (GeometryNode*) HitNode;
    RA = GNode->m_material->GetRA();
    self_color =  GNode->m_material->GetColor();
    Light = GNode->m_material->IsLight();

    HitNormal = glm::normalize(HitNormal);
    ReflectT = ActualT-ZERO;
    RefractT = ActualT+ZERO;
    ReflInter = Inter + glm::dvec4(ReflectT) * UnitRay;
    RefrInter = Inter + glm::dvec4(RefractT) * UnitRay;

    PNormal = Inter + glm::dvec4(ActualT)*UnitRay;
    GNode->m_material->PertubNormal(UnitRay,HitNormal,PNormal);  
    
    
    // Check if this is a Phong material, so that we can connect with it.
    if(GNode->m_material->Type == MatType::Phong){
      TargetRay = UnitRay = ReflInter - Target;
      UnitRay = glm::normalize(UnitRay);
      TargetT = TargetRay.x/UnitRay.x;
      
      // Check if this point can be connected 
      Root->GetMaterial(ReflInter,UnitRay,HitNormal,&Init2,&ActualT,&HitNode);
      // If didn't hit anything, connect
      if(!(Init && ActualT > ZERO && ActualT < TargetT - ZERO)){
	return GNode->m_material->GetDLColor(ReflInter,PNormal,m_PrimLight,Root);
      }
    }

    // If reached path max
    if(Count == 3){
      // if phong ,return color other wise keep tracing
      if(GNode->m_material->Type == MatType::Phong){
	return GNode->m_material->GetDLColor(ReflInter,PNormal,m_PrimLight,Root);
      }
    }
    // Get self color
    color = RA * GNode->m_material->GetColor();
    
    UseRefract = GNode->m_material->GetNextRay(UnitRay,PNormal,Next);
    double weight = 1;
    // If it's not light
    if(!Light){
      // If it's refractive, use refractive intersection
      if(UseRefract){
	color = color * LightPath(Target,HitNormal,Next,RefrInter,Count);
	// If it's phong ,count in direct illumination.
      } else if(GNode->m_material->Type == MatType::Phong){
	color = LightPath(Target,HitNormal,Next,ReflInter,Count+1) + GNode->m_material->GetDLColor(ReflInter,PNormal,m_PrimLight,Root);
	// other wise keep tracing.
      } else {
	color =  LightPath(Target,HitNormal,Next,ReflInter,Count);
      }
    }
    return color;
  }
  return glm::dvec3(0);
}

PathTracer::~PathTracer(){}
