#pragma once

#include "Triangle.hpp"
#include <glm/glm.hpp>

bool Compare(Triangle*  t1,Triangle*  t2);

bool CompareY(Triangle*  t1,Triangle*  t2);

bool CompareX(Triangle*  t1,Triangle*  t2);
