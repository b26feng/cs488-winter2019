#include <algorithm>
#include "SAH.hpp"

SAHObject::SAHObject(Triangle* TR, std::vector<glm::dvec3>& src):
  mSelected(false),
  mP1(src[TR->v1]),
  mP2(src[TR->v2]),
  mP3(src[TR->v3]),
  mCenter(TR->C)
{
  // calculate area;
  glm::dvec3 e1 = mP2 - mP1;
  glm::dvec3 e2 = mP3 - mP1;
  mArea = glm::length(glm::cross(e1,e2))/2;
}

SAHObject::SAHObject(const SAHObject* SAH):
  mSelected(SAH->mSelected),
  mP1(SAH->mP1),
  mP2(SAH->mP2),
  mP3(SAH->mP3),
  mCenter(SAH->mCenter),
  mArea(SAH->mArea)
{
}

bool ByX(SAHObject* s1, SAHObject* s2){
  return s1->mCenter.x < s2->mCenter.x;
}

bool ByY(SAHObject* s1, SAHObject* s2){
  return s1->mCenter.y < s2->mCenter.y;
}

bool ByZ(SAHObject* s1, SAHObject* s2){
  return s1->mCenter.z < s2->mCenter.z;
}

SAHList::SAHList(Triangle** Ts, int len, std::vector<glm::dvec3>& src)
{
  // Set this as original
  mOriginalSAH = true;
  // setup original
  SAHObject* SAH_Holder;
  for(int i = 0; i<len; i++){
    SAH_Holder = new SAHObject(Ts[i],src);
    mX.emplace_back(SAH_Holder);
    mY.emplace_back(SAH_Holder);
    mZ.emplace_back(SAH_Holder);
  }
  // sort base on different axis
  std::sort(mX.begin(),mX.end(),ByX);
  std::sort(mY.begin(),mY.end(),ByY);
  std::sort(mZ.begin(),mZ.end(),ByZ);

  // calculate total area
  mArea = 0;
  for(SAHObject* item: mX){
    mArea += item->mArea;
  }
}

SAHList::SAHList(const SAHList& SHList,int start, int end, char axis)
{
  // Set this as non original SAHList
  mOriginalSAH = false;
  // copy according to axis
  std::vector<SAHObject*> local_copy(end-start);
  switch(axis){
  case 'x':
    std::copy(SHList.mX.begin()+start,SHList.mX.begin()+end,local_copy.begin());
    mX = local_copy;
    break;
  case 'y':
    std::copy(SHList.mY.begin()+start,SHList.mY.begin()+end,local_copy.begin());
    mY = local_copy;
    break;
  case 'z':
    std::copy(SHList.mZ.begin()+start,SHList.mZ.begin()+end,local_copy.begin());
    mZ = local_copy;
    break;
  }

  for(SAHObject* item: local_copy){
    item->mSelected = true;
  }

  // copy without sorting agian but obtain sorted list
  if(axis != 'x'){
    for(SAHObject* item:SHList.mX){
      if(item->mSelected){
	mX.emplace_back(item);
      }
    }
  }

  if(axis != 'y'){
    for(SAHObject* item:SHList.mY){
      if(item->mSelected){
	mY.emplace_back(item);
      }
    }
  }

  if(axis !='z'){
    for(SAHObject* item:SHList.mZ){
      if(item->mSelected){
	mZ.emplace_back(item);
      }
    }
  }

  // calculate total area, reset selected flag
  mArea = 0;
  for(SAHObject* item: local_copy){
    mArea += item->mArea;
    item->mSelected = false;
  }
}

SAHList::~SAHList(){
  if(mOriginalSAH){
    for(SAHObject* item: mX){
      delete item;
    }
  }
}
