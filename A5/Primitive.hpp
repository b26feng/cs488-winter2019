// Fall 2018

#pragma once

#include <glm/glm.hpp>
#include <vector>

struct Triangle;
class SAHObject;

enum class PType{
  Primitive,
  Sphere,
  Cube,
  NSphere,
  NBox,
  Box,
  Tri,
  Plane
};
  

class Primitive {
protected:
  glm::dmat4 TMat;
  Primitive* Bound;
public:
  PType Type;
  Primitive();
  virtual ~Primitive();
  virtual void Print() const;

 
  // return the t for intersection
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal);

  //virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans);
  virtual void UpdatePos(glm::dmat4& TMat);
  virtual void GetCenterSize(glm::dvec4& CandS);

  virtual void GetRandomPoint(glm::dvec4& RandomPoint,glm::dvec4& Normal);
};



class NonhierSphere : public Primitive {
public:
  NonhierSphere(const glm::dvec3& pos, double radius);
  virtual ~NonhierSphere();
  virtual void Print() const override;
  // return the t for intersection
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal) override;

  virtual void GetCenterSize(glm::dvec4& CandS) override;
  //virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
public:
  glm::dvec3 m_pos;
  double m_radius;
};

class NonhierBox : public Primitive {
public:
  NonhierBox(const glm::dvec3& pos, double size);
  virtual ~NonhierBox();
  virtual void Print() const override;
  //virtual void UpdatePos(glm::dmat4& TMat,glm::dmat4& RMat,glm::dmat4& trans,glm::dmat4& invtrans) override;
  virtual void UpdatePos(glm::dmat4& TMat) override;
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal) override;

  virtual void GetCenterSize(glm::dvec4& CandS) override;
  
protected:
  glm::dvec3 m_pos;
  glm::dvec4* Vertices;
  glm::dvec4* Normals;
  double m_size;
};

class Sphere : public NonhierSphere {
public:
  Sphere();
};

class Cube : public NonhierBox {
public:
  Cube();
};

class Box : public Primitive{
protected:

  double positive_min(double a, double b);
  //double size;
public:
  
  glm::dvec3 m_min;
  glm::dvec3 m_max;
  glm::dvec3 m_pos;
  Box(const glm::dvec3& min,const glm::dvec3& max);
  virtual ~Box();
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal) override;
  virtual double IntersectTest(const glm::dvec4& Origin, const glm::dvec4& Ray);
  virtual void GetCenterSize(glm::dvec4& CandS) override;
};

class Tri : public Primitive{
protected:
  glm::dvec3 mE1;
  glm::dvec3 mE2;
public:
  glm::dvec3 mV1;
  glm::dvec4 mNormal;
  glm::dvec3 mV2;
  glm::dvec3 mV3;
  Tri(const Triangle* src,const std::vector<glm::dvec3>& vecs);
  Tri(const SAHObject* SAH);
  
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal) override;
  virtual void GetCenterSize(glm::dvec4& CandS) override;
};

class Plane: public Primitive{
private:
  
  glm::dvec3 Center;
  glm::dvec3 vec1;
  glm::dvec3 vec2;
  glm::dvec4 N;
  double size;
public:
  Plane(glm::dvec3 pos,glm::dvec3 v1,glm::dvec3 v2,double size);
  virtual double GetIntersect(const glm::dvec4& Origin, const glm::dvec4& Ray,glm::dvec4& Normal) override;
  virtual void GetCenterSize(glm::dvec4& CandS) override;
  virtual void GetRandomPoint(glm::dvec4& RandomPoint,glm::dvec4& Normal) override;
};
