#pragma once
#include "Tracer.hpp"

class PathTracer: public Tracer{
protected:
  virtual void SingleRay(glm::dvec4& Ray, const glm::dvec4& EYE,glm::dvec3& color,bool* Primary, glm::dvec4& FocalIn, bool* Skip=nullptr,char RayColor = 'W') override;
  virtual glm::dvec3 PathTrace(glm::dvec4& Ray, const glm::dvec4& EYE,int Count);
  virtual glm::dvec3 LightTrace(const glm::dvec4& Inter,GeometryNode* L);
  virtual glm::dvec3 LightPath(const glm::dvec4& Target, const glm::dvec4& Normal, glm::dvec4& Ray,glm::dvec4& Inter,int Count);
  
  glm::dvec3 m_SkyLight;
  int BCount = 0;
public:
  PathTracer(PicTrans* P, SceneNode* root,const glm::dvec3& ambient, const std::list<Light*>& lights);
  virtual ~PathTracer();
  virtual void Trace(Image& I,int ThreadNumber) override;
  virtual void RayTrace(Image& image) override;
};
