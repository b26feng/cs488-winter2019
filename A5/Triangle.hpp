#pragma once

#include <stdlib.h>
#include <glm/glm.hpp>

struct Triangle
{
  size_t v1;
  size_t v2;
  size_t v3;
  size_t n;
  glm::dvec3 C;
  glm::dvec4 N;
  
  Triangle( size_t pv1, size_t pv2, size_t pv3 )
    : v1( pv1 )
    , v2( pv2 )
    , v3( pv3 )
  {}
};
