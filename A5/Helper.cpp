#include "Helper.hpp"

bool Compare(Triangle*  t1,Triangle*  t2){
    
  glm::dvec3 p1 = t1->C;
  if(t2 == nullptr) return true;
  glm::dvec3 p2 = t2->C;
  return p1.z < p2.z;
  
  //return t1->C.z < t2->C.z;
}


bool CompareY(Triangle*  t1,Triangle*  t2){
  
  glm::dvec3 p1 = t1->C;
  if(t2 == nullptr) return true;
  glm::dvec3 p2 = t2->C;
  return p1.y<p2.y;
  
  return t1->C.y < t2->C.y;
}


bool CompareX(Triangle*  t1,Triangle*  t2){
  
  glm::dvec3 p1 = t1->C;
  if(t2 == nullptr) return true;
  glm::dvec3 p2 = t2->C;
  return p1.x<p2.x;
  
  return t1->C.x < t2->C.x;
}


