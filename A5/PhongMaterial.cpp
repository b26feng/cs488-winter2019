// Winter 2019

#include "PhongMaterial.hpp"
#include "Random.hpp"
#include "Constant.hpp"
#include "GeometryNode.hpp"

#include <iostream>
#include <glm/glm.hpp>
PhongMaterial::PhongMaterial(
	const glm::vec3& kd, const glm::vec3& ks, double shininess )
  : m_kd(glm::dvec3(kd))
  , m_ks(glm::dvec3(ks))
  , m_shininess(shininess)
{
  Type = MatType::Phong;
  ReflectCount = 0;
  RAttenu = 0.9;
  tex = nullptr;
}

void PhongMaterial:: GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t)const{
  if(!Hit){
    double Dist = glm::length(Ray);
    double DIF = glm::dot(glm::normalize(Ray),Normal);
    double Fall = L->falloff[0] +L->falloff[1]*Dist+L->falloff[2]*Dist*Dist;
    //DIF = (DIF + (DIF - 0.0f))/2;
    // difuse
    if(DIF > 0){
      color.x += (DIF* m_kd.x  * (L->colour.x))/Fall;
      color.y += (DIF* m_kd.y  * (L->colour.y))/Fall;
      color.z +=(DIF* m_kd.z * (L->colour.z))/Fall;
    }
    // specular
    
    glm::dvec4 SRay = Origin - glm::dvec4(L->position,1.0f);
    SRay = glm::reflect(SRay,-Normal);
    DIF = -glm::dot(glm::normalize(SRay),glm::normalize(EyeRay));
    DIF = glm::pow(DIF,m_shininess);
    //std::cout<<"Len:"<<glm::length(EyeRay)<<"|"<<glm::length(Normal)<<std::endl;
    //DIF = (DIF + (DIF - 0.0f))/2;
    if(DIF>0){
      color.x += (DIF* m_ks.x  * (L->colour.x));
      color.y += (DIF* m_ks.y  * (L->colour.y));
      color.z += (DIF* m_ks.z  * (L->colour.z));
    }
  }
}

glm::dvec3 PhongMaterial::GetDLColor(const glm::dvec4& Inter, const glm::dvec4& N,const SceneNode* Light,SceneNode* Root) const {
  SceneNode* NewHit;
  const GeometryNode* GLight = (GeometryNode*) Light; 
  glm::dvec3 dir_color = glm::dvec3(0);
  glm::dvec4 LSource,LNormal,DirRay,DirNormal;
  bool DInit = false;
  double DirT,RealT;

  // Get random point from light source.
  GLight->m_primitive->GetRandomPoint(LSource,LNormal);
  // Get the ray from light source to the intersection.
  DirRay = glm::normalize(Inter - LSource);
  RealT = (Inter-LSource).x/DirRay.x;
  DInit = false;

  // Intersection Check
  Root->GetMaterial(LSource,DirRay,DirNormal,&DInit,&DirT,&NewHit);
  // If nothing in between, do the diffuse color.
  if(!(DInit && DirT > ZERO && DirT < RealT - ZERO)){
    double cos_val = glm::dot(N,DirRay);
    if(cos_val > 0) return dir_color;
    dir_color = m_kd * GLight->m_material->GetColor() * (-cos_val);
    dir_color /= PI;
  }
  
  return dir_color;

}

glm::dvec3 PhongMaterial::GetColor() const{
  return m_kd;
}

bool PhongMaterial::IsLight(){
  return m_shininess < 12;
}

bool PhongMaterial::GetNextRay(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NextRay){
  // Get projection length, azemothal angle.
  double r1 = glm::cos(glm::radians(RandRange(0,90,100)));
  double deg = glm::radians(RandRange(0,360,100));
  // w is 3d Normal, UV cordinate perpendicular to w
  glm::dvec3 u,v,w;
  w = glm::dvec3(Normal);
  if((Normal.x <= 1+ ZERO && Normal.x >= 1-ZERO)||
     (Normal.x <= -1 + ZERO && Normal.x >= -1-ZERO)){
    u = glm::cross(glm::dvec3(0,1,0),w);
  }else{
    u = glm::cross(glm::dvec3(1,0,0),w);
  }
  v = glm::cross(u,w);
  NextRay = glm::dvec4(r1*(u*glm::cos(deg)+v*glm::sin(deg)),0)+Normal*(glm::sqrt(1-r1*r1));
  //NextRay.w = glm::sqrt(1-r1*r1)/2;
  //NextRay = glm::dvec4(r1*(u*glm::cos(deg)+v*glm::sin(deg)),2*glm::sqrt(1-r1*r1))+Normal*(glm::sqrt(1-r1*r1));
  //NextRay.w = 1.0;
  return false;
}


PhongMaterial::~PhongMaterial()
{}
