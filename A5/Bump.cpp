#include "Bump.hpp"
#include "Constant.hpp"

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <cmath>

Bump::Bump(const int size,const double Epsilon):
  Texture(size,Epsilon)
{
  Type = TexType::Bump;
}



void Bump::PertubNormal(const glm::dvec4& InRay,const glm::dvec4& Normal,glm::dvec4& NewNormal)
{
  const glm::dvec4 Intersection = NewNormal;
  double theta,phi,u,v;
  if(PR == 0){
    std::cout<<glm::to_string(PCenter)<<"|"<<PR<<std::endl;
  }
  theta = glm::acos((Intersection.z-PCenter.z)/PR);
  
  phi = glm::atan((Intersection.y-PCenter.y),(Intersection.x-PCenter.x));
  if(phi!=phi){
    std::cout<<glm::to_string(PCenter)<<"|"<<glm::to_string(Intersection)<<std::endl;
  }
  u = phi/(2*PI);
  v = (PI-theta)/PI;
  if(theta!=theta){
    std::cout<<glm::to_string(PCenter)<<"|"<<glm::to_string(Intersection)<<std::endl;
  }
  //if(u!=u || v!=v){
  //std::cout<<u<<","<<v<<","<<epsilon<<std::endl;
    //}
  //
  ProduceNormal(Normal,u*Size,v*Size,NewNormal);

  // if(Normal != NewNormal) std::cout<<glm::angle(Normal,NewNormal)<<std::endl;
}

void Bump::ProduceNormal(const glm::dvec4& Normal,const double u,const double v, glm::dvec4& NewNormal) const
{
  double xcos,ycos;
  
  xcos = GetTan(u-epsilon,v,u+epsilon,v);
  ycos = GetTan(u,v-epsilon,u,v+epsilon);
  /*if(ycos == 0.0){
    std::cout<<ycos<<std::endl;
  }*/
  glm::dmat4 XRot,YRot;
  glm::dvec3 y,x;
  glm::dvec3 xaxis,yaxis;
  x = glm::dvec3(1.0,0.0,0.0);
  y = glm::dvec3(0.0,1.0,0.0);
  int sign = (glm::dot(glm::dvec3(Normal),y) > 0)? -1:1;
  if(glm::dvec3(Normal) != y){
    xaxis = glm::cross(glm::dvec3(Normal),y);
    yaxis = glm::cross(glm::dvec3(Normal),xaxis);
  }else{
    yaxis = glm::cross(glm::dvec3(Normal),x);
    xaxis = glm::cross(glm::dvec3(Normal),yaxis);
  }
  NewNormal = Normal;
  if(xcos != 0.0){
    /*if(xcos > PI/2){
      xcos -= PI/2;
    }else if (xcos < -PI/2){
      xcos += -PI/2;
      }*/
    YRot = glm::rotate(glm::degrees(glm::acos(xcos)),yaxis);
    NewNormal = YRot*Normal;
  }
  if(ycos != 0.0){
    /*
    if(ycos > PI/2){
      ycos -= PI/2;
    }else if (ycos < -PI/2){
      ycos += -PI/2;
      }*/
    XRot = glm::rotate(glm::degrees(glm::acos(ycos)),xaxis);
    NewNormal = XRot*Normal;
  }
}

// basically gird like
// 1 0 1 0 1
// 0 1 0 1 0
// 1 0 1 0 1
// from 1 to 0 we get positive tangent, otherwise negative tangent
double Bump::GetTan(const double u1, const double v1,const double u2,const double v2) const
{
  double t,dBack,dForward;
  int iBack,iForward;
  // check for y direction
  if(u1 == u2){
    dBack = v1;
    dForward=v2;
    iBack = round(dBack);
    iForward = round(dForward);
    //std::cout<<dBack<<","<<dForward<<" "<<std::endl;
    if((dBack<iBack && dForward>iForward) || (dBack < iForward && dForward > iForward)){
      
      //int left = (dBack>iBack)? iBack%2:(iBack-1)%2;
      //int right = (dForward>iForward)? iForward%2:(iForward-1)%2;
      // int sign = (((int)(u1*Size))%2)? 1:-1;
      double left = glm::sin(iBack);
      double right = glm::sin(iForward);
      
      return ((left-right)/(dBack-dForward));
    }
  }else{/*
    dBack = u1;
    dForward= u2;
    iBack = round(dBack);
    iForward = round(dForward);
    if((dBack<iBack && dForward>iForward) || (dBack < iForward && dForward > iForward)){
      //int left = (dBack>iBack)? iBack%2:(iBack-1)%2;
      //int right = (dForward>iForward)? iForward%2:(iForward-1)%2;
      //int sign = (((int)(u1*Size))%2)? 1:-1;
      double left = glm::sin(dBack);
      double right = glm::sin(dForward);
      return ((left-right)/(dBack-dForward));
      }*/
  }
  return 0.0;
}
