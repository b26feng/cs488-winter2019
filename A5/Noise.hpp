#pragma once

#include <glm/glm.hpp>
enum class NoiseType{
  Noise,
    Perlin2,
    Perlin3
    };

class Noise{
protected:
  const int Size;
public:
  Noise(const int size);
  virtual double noise(const glm::dvec4& in);

  
  NoiseType Type;
};
