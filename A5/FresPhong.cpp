#include "FresPhong.hpp"
//#include <iostream>

FresPhong::FresPhong(const glm::dvec3& kd,int rc,double anu,double shininess,double G,double RI):
  Transparent(kd,rc,anu,shininess,G,RI),
  kd(kd)
{
  Type = MatType::FresPhong;
}

void FresPhong::GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t)const
{
  //std::cout<<Hit<<std::endl;
  if(!Hit){
    double Dist = glm::length(Ray);
    double DIF = glm::dot(glm::normalize(Ray),Normal);
    double Fall = L->falloff[0] +L->falloff[1]*Dist+L->falloff[2]*Dist*Dist;
    //DIF = (DIF + (DIF - 0.0f))/2;
    // difuse
    if(DIF > 0){
      color.x += (DIF* kd.x  * (L->colour.x))/Fall;
      color.y += (DIF* kd.y  * (L->colour.y))/Fall;
      color.z +=(DIF* kd.z * (L->colour.z))/Fall;
    }
    // specular
  }
}

