#pragma once
#include "Transparent.hpp"

class FresPhong: public Transparent{
protected:
  glm::dvec3 kd;
public:
  FresPhong(const glm::dvec3& kd,int rc,double anu,double shininess,double G,double RI);
  // pass in the refraction ray
  virtual void GetColor(const glm::dvec4& EyeRay,const glm::dvec4& Origin,const glm::dvec4& Ray,const glm::dvec4& Normal,glm::dvec3& color,const Light* L,bool Hit,double t) const override;
};
