// Winter 2019

#pragma once

#include <glm/glm.hpp>
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include <vector>


class CubeDrawer
{
public:
  CubeDrawer(size_t);
  ~CubeDrawer();

  void InitCubeDrawer();
  
  size_t GetCubeCount() const;

  // assumes whole maze sits between 0~16
  int GetE() const;
  int GetX(int index) const;
  int GetZ(int index) const;
  int GetLX(int index) const;
  int GetWZ(int index) const;
  int GetAX() const;
  int GetAY() const;
  bool HasChanged() const;
  bool AvatarChanged() const;
  GLuint GetAVAO() const;
  GLuint* GetVAOArray() const;
  GLuint* GetVBOArray() const;
  GLuint* GetEBOArray() const;
  glm::mat4 GetTransform() const;
  void SetAllArray(GLuint* VAO,GLuint* VBO, GLuint* EBO);
  void SetCubes(std::vector<int>& CVec);
  void MoveAvatar(unsigned int x, unsigned int y);
  void SetAvatar(int x, int y);
  void Higher();
  void Lower();
  void Draw(ShaderProgram& m_shader);
  void DrawAvatar();
  void DrawAvatar(ShaderProgram& m_shader);
  void CreateRoundAvatar(float H,float R, int E,ShaderProgram& m_shader);
  void ResetHight();
  void ResetAvatar();
  void LagTrans();
  void EnableShader();
  void DisableShader();
  void SetShaderUniform(glm::mat4& P,glm::mat4& V,glm::mat4& W);
  void SetShaderLoc(glm::mat4&);
  void SetViewPort(glm::vec3&);
  void SetColour(float *);
  void DrawShadedAv();
  //void SetSize(size_t S);
private:
  void DrawBall(float R,int E,glm::vec3** Vertices, int** Sequence);
  
  float AvatarBaseCube[24] = { 0.05f,0.9f,0.05f,
			       0.05f,0.9f,0.95f,
			       0.95f,0.9f,0.05f,
			       0.95f,0.9f,0.95f,
			       
			       0.95f,0.0f,0.95f,
			       0.05f,0.0f,0.95f,
			       0.05f,0.0f,0.05f,
			       0.95f,0.0f,0.05f,};
  
  float AvatarCube[24] = { 0.05f,0.9f,0.05f,
			   0.05f,0.9f,0.95f,
			   0.95f,0.9f,0.05f,
			   0.95f,0.9f,0.95f,

			   0.95f,0.0f,0.95f,
			   0.05f,0.0f,0.95f,
			   0.05f,0.0f,0.05f,
			   0.95f,0.0f,0.05f,};
  
  float UnitCube[24] =  { 0.0f,1.0f,0.0f,
			  0.0f,1.0f,1.0f,
			  1.0f,1.0f,0.0f,
			  1.0f,1.0f,1.0f,
			  
			  1.0f,0.0f,1.0f,
			  0.0f,0.0f,1.0f,
			  0.0f,0.0f,0.0f,
			  1.0f,0.0f,0.0f,};

  float MaskVertices[24] =  { 0.0f,1.0f,0.0f,
			  0.0f,1.0f,1.0f,
			  1.0f,1.0f,0.0f,
			  1.0f,1.0f,1.0f,
			  
			  1.0f,0.0f,1.0f,
			  0.0f,0.0f,1.0f,
			  0.0f,0.0f,0.0f,
			  1.0f,0.0f,0.0f,};
  
  int CubeSequence[13] = {0,1,2,3,4,1,5,0,6,2,7,3,4};

  int AvatarX = 0;
  int AvatarY = 0;
  float XLag;
  float XDiff;
  float YLag;
  float YDiff;
  int EdgeCount;
  
  float HightOffset;
  size_t CubeCount;

  bool Changed = false;
  bool AChanged = false;
  
  // VAO,VBO,EBO arrays
  GLuint *VAOs;
  GLuint *VBOs;
  GLuint *EBOs;

  GLuint AVAO;
  GLuint AVBO;
  GLuint AEBO;
  glm::mat4 Transform;

  // Shader
  ShaderProgram LShader;
  GLint PU;
  GLint VU;
  GLint MU;
  GLint TU;
  GLint CU;
  GLint VPU;
  glm::mat4 T;
  // (x,z,lx,wz) for each cube
  // actual size is size/4
  std::vector<int> Cubes;
};
