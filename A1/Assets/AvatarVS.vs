#version 330

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform mat4 T;
uniform vec3 ViewPort;
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 NVec;
out vec3 NormalVec;
out vec3 Loc;
out vec3 Camera;
void main() {
	gl_Position = P * V * M * T * vec4(position, 1.0);
	NormalVec = NVec;
	Camera = ViewPort;
	Loc = vec3(M * T * vec4(position,1.0));
}
