#version 330

uniform vec3 OColor;
in vec3 NormalVec;
in vec3 Loc;
in vec3 Camera;
out vec4 fragColor;

void main() {
     vec3 norm = normalize(NormalVec);
     vec3 lightDirection = normalize(vec3(0.0f,7.0f,0.0f) - Loc);
     float LLD = length(lightDirection);
     float BrightCos = max(dot(norm,lightDirection),0.0f);
     vec3 Col1 = BrightCos * vec3(1.0f,1.0f,1.0f);

     vec3 ViewDir = normalize(Camera - Loc);
     vec3 REF = reflect(-lightDirection,norm);
     float ViewCos = max(dot(ViewDir,REF),0.0f);
     float Reflection = pow(ViewCos,32);
     vec3 ReflectLight = 0.3 * Reflection * vec3(1.0f,1.0f,1.0f);
     
     vec3 Result =  (ReflectLight + Col1) * OColor;
     fragColor = vec4(Result, 1 ); 
}
