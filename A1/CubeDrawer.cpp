#include "CubeDrawer.hpp"
#include "cs488-framework/GlErrorCheck.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/CS488Window.hpp"

#include <iostream>
#include <unistd.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

#define ESC 27
#define RED() printf("%c[31m",ESC);
#define YEL() printf("%c[33m",ESC);
#define GRE() printf("%c[32m",ESC);
#define DEFAULT() printf("%c[39;49m",ESC);

CubeDrawer::CubeDrawer(size_t count):
  HightOffset(1.0f),
  CubeCount(count),
  VAOs(nullptr),
  VBOs(nullptr),
  EBOs(nullptr)
{}

CubeDrawer::~CubeDrawer()
{
  delete[] VAOs;
  delete[] VBOs;
  delete[] EBOs;
}

void CubeDrawer::InitCubeDrawer()
{
  glGenVertexArrays(1,&AVAO);
  glGenBuffers(1,&AVBO);
  glGenBuffers(1,&AEBO);

  LShader.generateProgramObject();
  RED();
  LShader.attachVertexShader("./Assets/AvatarVS.vs");
  YEL();
  LShader.attachFragmentShader("./Assets/AvatarFS.fs");
  GRE();
  LShader.link();
  DEFAULT();
  PU = LShader.getUniformLocation("P");
  VU = LShader.getUniformLocation("V");
  MU = LShader.getUniformLocation("M");
  TU = LShader.getUniformLocation("T");
  CU = LShader.getUniformLocation("OColor");
  VPU = LShader.getUniformLocation("ViewPort");
  
}

int CubeDrawer::GetE() const { return EdgeCount; }

size_t CubeDrawer::GetCubeCount() const { return CubeCount; }

int CubeDrawer::GetX(int index) const
{
  if(index < 0 || index >= CubeCount) return -1;
  return Cubes[index*4];
}

int CubeDrawer::GetZ(int index) const
{
  if(index < 0 || index >= CubeCount) return -1;
  return Cubes[index*4+1];
}

int CubeDrawer::GetLX(int index) const
{
  if(index < 0 || index >= CubeCount) return -1;
  return Cubes[index*4+2];
}

int CubeDrawer::GetWZ(int index) const
{
  if(index < 0 || index >= CubeCount) return -1;
  return Cubes[index*4+3];
}

int CubeDrawer::GetAX()const { return AvatarX; }
int CubeDrawer::GetAY()const { return AvatarY; }

bool CubeDrawer::HasChanged() const { return Changed; }
bool CubeDrawer::AvatarChanged() const { return AChanged; }

GLuint CubeDrawer::GetAVAO() const { return AVAO; }

GLuint* CubeDrawer::GetVAOArray() const
{
  return VAOs;
}


GLuint* CubeDrawer::GetVBOArray() const
{
  return VBOs;
}

GLuint* CubeDrawer::GetEBOArray() const
{
  return EBOs;
}

glm::mat4 CubeDrawer::GetTransform() const { return Transform; }

void CubeDrawer::SetAllArray(GLuint* VAO,GLuint* VBO, GLuint* EBO)
{
  if(VAOs != nullptr){
    delete[] VAOs;
    delete[] VBOs;
    delete[] EBOs;
  }
  
  VAOs = VAO;
  VBOs = VBO;
  EBOs = EBO;
  Changed == true;
}

void CubeDrawer::MoveAvatar(unsigned int x,unsigned int y){
  
  XDiff = ((int)x)-AvatarX;
  YDiff = ((int)y)-AvatarY;
  AvatarX = x;
  AvatarY = y;
  YLag = XLag = 1.0f;
  AChanged = true;
}

void CubeDrawer::SetAvatar(int x,int y){
  AvatarX = x;
  AvatarY = y;
  XDiff=YDiff=YLag=XLag=0.0f;
  AChanged = true;
}


void CubeDrawer::SetCubes(std::vector<int>& CVec){
  Cubes = CVec;
  CubeCount = CVec.size()/4;
  Changed = true;
}

void CubeDrawer::Higher(){
  HightOffset += 0.1f;
  Changed = true;
}

void CubeDrawer::Lower(){
  // no lower than 0.0
  HightOffset -= 0.1;
  if(HightOffset < 0.1f) HightOffset = 0.1f;
  Changed = true;
}

void CubeDrawer::Draw(ShaderProgram& m_shader){

  // if init or theMaze has been changed, regenerate cubes and VAO,VBO,EBO
  int X,Z,LX,WZ;
  for(int i = 0; i<CubeCount;i++){
    // Generate all Vertices
    X = GetX(i);
    Z= GetZ(i);
    LX = GetLX(i);
    WZ = GetWZ(i);
    for(int j = 0; j <8; j++){
      int idx = j*3;
      UnitCube[idx] = MaskVertices[idx]*LX+X;
      UnitCube[idx+1] = MaskVertices[idx+1]*HightOffset;
      UnitCube[idx+2] = MaskVertices[idx+2]*WZ + Z;
    }
    
    glBindVertexArray( VAOs[i] );
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOs[i]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(CubeSequence),CubeSequence,GL_STATIC_DRAW);
    
    
    glBindBuffer( GL_ARRAY_BUFFER, VBOs[i] );
    glBufferData( GL_ARRAY_BUFFER, 8*3*sizeof(float),
		  UnitCube,GL_STATIC_DRAW );
    
    GLint posAttrib = m_shader.getAttribLocation( "position" );
    glEnableVertexAttribArray( posAttrib );
    glVertexAttribPointer( posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );
    
    
    glBindVertexArray( 0 );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

    CHECK_GL_ERRORS;
    
    //DrawAvatar(m_shader);
    //CreateRoundAvatar(4.0f,4.0f,6,m_shader);
    Changed = false;
  }
}

void CubeDrawer::ResetHight(){
  HightOffset = 1.0f;
  Changed = true;
}

void CubeDrawer::DrawAvatar(ShaderProgram& m_shader){
  int idx;
  for(int i = 0; i<8;i++){
    idx = i*3;
    AvatarCube[idx] = AvatarBaseCube[idx]+AvatarY;
    AvatarCube[idx+2] = AvatarBaseCube[idx+2]+AvatarX;
  }

  glBindVertexArray(AVAO);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,AEBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(CubeSequence),CubeSequence,GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, AVBO);
  glBufferData(GL_ARRAY_BUFFER,8*3*sizeof(float),AvatarCube,GL_STATIC_DRAW);

  GLint posAttrib = m_shader.getAttribLocation("position");
  glEnableVertexAttribArray( posAttrib);
  glVertexAttribPointer(posAttrib,3,GL_FLOAT, GL_FALSE,0,nullptr);

  glBindVertexArray( 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

  CHECK_GL_ERRORS;
  
  AChanged = false;
}

void CubeDrawer::ResetAvatar()
{
  for(int i = 0; i< 24; i++){
    AvatarCube[i] = AvatarBaseCube[i];
  }
  AvatarX = AvatarY = 0;
  Transform = glm::translate(glm::mat4(),glm::vec3(0.5f,0.45f,0.5f));
  AChanged = true;
}

void CubeDrawer::CreateRoundAvatar(float H,float R,int E,ShaderProgram& m_shader)
{
  EdgeCount = E;
  glm::vec3* Vertices;
  int* Sequence;
  DrawBall(R,E,&Vertices, &Sequence);
  // bind it to buffers
  //T_uniform = m_shader.getUnifromLocation("T");
  //Transform = glm::translate(mat4(),glm::vec3(0.0f,H,0.0f));

  glBindVertexArray(AVAO);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,AEBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(int)*((E-1)*E+1),Sequence,GL_STATIC_DRAW);
  
  glBindBuffer(GL_ARRAY_BUFFER, AVBO);
  glBufferData(GL_ARRAY_BUFFER,sizeof(glm::vec3)*(E*(E/2 -1) + 2)*2,Vertices,GL_STATIC_DRAW);
  
  //GLint posAttrib = m_shader.getAttribLocation("position");
  // set Vertices
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE,0,nullptr);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE,0,(void *)(sizeof(glm::vec3)*(E*(E/2 -1) + 2)));
  
  glBindVertexArray( 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
  
  CHECK_GL_ERRORS;
  delete [] Vertices;
  delete [] Sequence;
  
  Transform = glm::translate(glm::mat4(),glm::vec3(0.5f,0.45f,0.5f));

  AChanged = false;
}

void CubeDrawer::DrawBall(float R,int E,
			  glm::vec3** Vertices,int** Sequence)
{
  // Get total vertices
  int VCount = E*(E/2 -1) + 2;

  // remember to delete it
  glm::vec3 *AllV = new glm::vec3[2*VCount];

  
  //Setup Top and Bottom
  AllV[0].x=AllV[0].z = AllV[VCount-1].x=AllV[VCount-1].z = 0.0f;
  AllV[0].y = R;
  AllV[VCount-1].y = -R;

  int ArcLen = E/2-1;
  float RotationUnit = (2*3.14159265358979323846f) / E;
  for(int i = 1; i<VCount-1;i+=ArcLen){
    // Drawing the first arc
    if(i == 1){
      for(int j = 0; j<ArcLen;j++){
	AllV[i+j] = glm::rotateZ(AllV[i+j-1],RotationUnit);
      }
      // rotate the first arc
    }else{
      for(int j=0; j<ArcLen; j++){
	AllV[i+j] = glm::rotateY(AllV[i+j-ArcLen],RotationUnit);
      }
    }
  }
  // normal vectors
  glm::vec3 Center(0.0f,0.0f,0.0f);
  for(int i = VCount; i<2*VCount;i++){
    AllV[i] = AllV[i-VCount] - Center;
  }
  
  int *Seq = new int[E*(E-1) + 1];
  Seq[0] = 0;
  int Cur = 1;
  int total = ArcLen*E;
  for(int i = 0 ; i<E;i++){
    // odd draw up, even draw down
    if(i%2){
      for(int j = ArcLen; j>=1; j--){
	Seq[Cur] = ((i+1)*ArcLen+j)%total;
	Seq[Cur+1] = i*ArcLen+j;
	Cur+=2;
      }
      Seq[Cur] = 0;
      Cur++;
    }else{
      for(int j = 1; j<=ArcLen;j++){
	Seq[Cur] = i*ArcLen+j;
	Seq[Cur+1] = (i+1)*ArcLen+j;
	Cur+=2;
      }
      Seq[Cur] = VCount-1;
      Cur++;
    }
  }
  *Vertices = AllV;
  *Sequence = Seq;
}

void CubeDrawer::LagTrans()
{
  
  
  Transform = glm::translate(glm::mat4(),glm::vec3((float)AvatarY-(YDiff*YLag)+0.5f,0.45f,(float)AvatarX-(XDiff*XLag)+0.5f));
  if(XLag<=0.0f && YLag<=0.0f){
    XLag=YLag=0.0f;
    AChanged = false;
  }else{
    XLag-=0.1f;
    YLag-=0.1f;
  }
}

void CubeDrawer::EnableShader() { LShader.enable(); }
void CubeDrawer::DisableShader() { LShader.disable();}

void CubeDrawer::SetShaderUniform(glm::mat4& P,glm::mat4& V, glm::mat4& W)
{
  glUniformMatrix4fv(PU,1,GL_FALSE, value_ptr(P));
  glUniformMatrix4fv(VU,1,GL_FALSE, value_ptr(V));
  glUniformMatrix4fv(MU,1,GL_FALSE, value_ptr(W));
}

void CubeDrawer::SetShaderLoc(glm::mat4 &T){
  if(AChanged) LagTrans();
  glUniformMatrix4fv(TU,1,GL_FALSE, value_ptr(T*Transform));
}

void CubeDrawer::SetColour(float *AvCol)
{
  glUniform3f(CU,AvCol[0],AvCol[1],AvCol[2]);
}

void CubeDrawer::DrawShadedAv()
{
  glBindVertexArray(AVAO);
  glDrawElements(GL_TRIANGLE_STRIP,EdgeCount*(EdgeCount-1)+1,GL_UNSIGNED_INT,0);
}

void CubeDrawer::SetViewPort(glm::vec3& ViewPort)
{
  glUniform3fv(VPU,1,value_ptr(ViewPort));
}
