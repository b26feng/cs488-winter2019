// Winter 2019

#include "A1.hpp"
#include "cs488-framework/GlErrorCheck.hpp"

#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include <imgui/imgui.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;
using namespace std;


static const size_t DIM = 16;
static const float PI = 3.14159265358979323846f;
static const int CubeSequence[] = {0,1,2,3,4,1,5,0,6,2,7,3,4};

static const float Wall[] = {0.0f,0.0f,0.0f};
static const float Avat[] = {0.9f,0.0f,0.0f};
static const float Flor[] = {0.9f,0.9f,1.0f};

static const vec3 x_axis(1.0f,0.0f,0.0f);
static const vec3 y_axis(0.0f,1.0f,0.0f);
static const vec3 MoveMaze(8.0f,0.0f,8.0f);
static const vec3 ResetMaze(-8.0f,0.0f,-8.0f);

#define ESC 27
#define RED() printf("%c[31m",ESC);
#define DEFAULT() printf("%c[39;49m",ESC);

//----------------------------------------------------------------------------------------
// Constructor
A1::A1()
  : current_col( 0 ),
    DefaultPerspecDeg(30.0f),
    theMaze(Maze(DIM)),
    CDrawer(CubeDrawer(0)),
    ShiftPressed(false),
    Record(false),
    Sign(1),
    Deg(0.0f),
    RotationStart(vec3(0.0f)),
    RotationEnd(vec3(0.0f)),
    LawVec(vec3(0.0f)),
    ColorPtr(nullptr)
{
	colour[0] = WaCol[0] = AvCol[0] = FlCol[0] = 0.0f;
	colour[1] = WaCol[1] = AvCol[1] = FlCol[1] = 0.0f;
	colour[2] = WaCol[2] = AvCol[2] = FlCol[2] = 0.0f;
	CurrentPerspecDeg = DefaultPerspecDeg;
}

//----------------------------------------------------------------------------------------
// Destructor
A1::~A1()
{}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A1::init(){

	// Initialize random number generator
	int rseed=getpid();
	srandom(rseed);
	// Print random number seed in case we want to rerun with
	// same random numbers
	cout << "Random number seed = " << rseed << endl;
	

	// DELETE FROM HERE...
	// ...TO HERE
	
	// Set the background colour.
	glClearColor( 0.3, 0.5, 0.7, 1.0 );

	// Build the shader
	m_shader.generateProgramObject();
	m_shader.attachVertexShader(
		getAssetFilePath( "VertexShader.vs" ).c_str() );
	m_shader.attachFragmentShader(
		getAssetFilePath( "FragmentShader.fs" ).c_str() );
	m_shader.link();

	// Set up the uniforms
	P_uni = m_shader.getUniformLocation( "P" );
	V_uni = m_shader.getUniformLocation( "V" );
	M_uni = m_shader.getUniformLocation( "M" );
	// try to do translation/transformation
	T_uni = m_shader.getUniformLocation( "T" );
	
	col_uni = m_shader.getUniformLocation( "colour" );

	//init CubeDrawer
	CDrawer.InitCubeDrawer();
	CDrawer.CreateRoundAvatar(0.45f,0.45f,256,m_shader);
	AvCol[0] = 0.9f;
	AvCol[1] = AvCol[2] = 0.0f;
	initGrid();
	initFloor();
	
	// Set up initial view and projection matrices (need to do this here,
	// since it depends on the GLFW window being set up correctly).
	view = glm::lookAt( 
		glm::vec3( 0.0f, 2.*float(DIM)*2.0*M_SQRT1_2, float(DIM)*2.0*M_SQRT1_2 ),
		//glm::vec3( 0.0f, 0.0f, 0.0f ),
		glm::vec3( 0.0f, 0.0f, 0.0f ),
		//glm::vec3( 0.0f, 1.0f, 0.0f ),
		glm::vec3( 0.0f, 1.0f, 0.0f));

	proj = glm::perspective(
				//glm::radians( 10.0f),
				// place the camera closer with a smaller value
				glm::radians(CurrentPerspecDeg ),
 		float( m_framebufferWidth ) / float( m_framebufferHeight ),
		1.0f, 1000.0f );
}

void A1::initGrid()
{
	size_t sz = 3 * 2 * 2 * (DIM+3);

	float *verts = new float[ sz ];
	size_t ct = 0;
	for( int idx = 0; idx < DIM+3; ++idx ) {
		verts[ ct ] = -1;
		verts[ ct+1 ] = 0;
		verts[ ct+2 ] = idx-1;
		verts[ ct+3 ] = DIM+1;
		verts[ ct+4 ] = 0;
		verts[ ct+5 ] = idx-1;
		// DEBUG
		//cout<<verts[ct]<<","<<verts[ct+1]<<","<<verts[ct+2]<<" ";
		//cout<<verts[ct+3]<<","<<verts[ct+4]<<","<<verts[ct+5]<<endl;
		

		ct += 6;
		verts[ ct ] = idx-1;
		verts[ ct+1 ] = 0;
		verts[ ct+2 ] = -1;
		verts[ ct+3 ] = idx-1;
		verts[ ct+4 ] = 0;
		verts[ ct+5 ] = DIM+1;
		// DEBUG
		//cout<<verts[ct]<<","<<verts[ct+1]<<","<<verts[ct+2]<<" ";
		//cout<<verts[ct+3]<<","<<verts[ct+4]<<","<<verts[ct+5]<<endl;
		ct += 6;
	}

	// Create the vertex array to record buffer assignments.
	glGenVertexArrays( 1, &m_grid_vao );
	glBindVertexArray( m_grid_vao );

	// Create the cube vertex buffer
	glGenBuffers( 1, &m_grid_vbo );
	glBindBuffer( GL_ARRAY_BUFFER, m_grid_vbo );
	glBufferData( GL_ARRAY_BUFFER, sz*sizeof(float),
		verts, GL_STATIC_DRAW );

	// Specify the means of extracting the position values properl
	GLint posAttrib = m_shader.getAttribLocation( "position" );
	glEnableVertexAttribArray( posAttrib );
	glVertexAttribPointer( posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

	// Reset state to prevent rogue code from messing with *my* 
	// stuff!
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	// OpenGL has the buffer now, there's no need for us to keep a copy.
	delete [] verts;

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A1::appLogic()
{
  // try to draw a unit block
  
  
  // if init or theMaze has been changed, regenerate cubes and VAO,VBO,EBO
  int Count = CDrawer.GetCubeCount();
  if(theMaze.HasChanged() || CDrawer.HasChanged()){
    vector<int> Cubes;
    theMaze.GetCubes(Cubes);
    CDrawer.SetCubes(Cubes);
    Count = CDrawer.GetCubeCount();
    GLuint* VAO = new GLuint [Count];
    GLuint* VBO = new GLuint [Count];
    GLuint* EBO = new GLuint [Count];

    // create vertex array, vertex buffer, elembuffer
    glGenVertexArrays(Count, VAO);
    glGenBuffers(Count, VBO);
    glGenBuffers(Count, EBO);

    CDrawer.SetAllArray(VAO,VBO,EBO);
    CDrawer.Draw(m_shader);
  }
    
  // Place per frame, application logic here ...
  // Create the vertex array to record buffer assignments.
  //glGenVertexArrays( 1, &b_unit_vao );
  //glBindVertexArray( b_unit_vao );

  // Create element buffer object
  //glGenBuffers(1,&b_unit_ebo);
  //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, b_unit_ebo);
  //glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(CubeSequence),CubeSequence,GL_STATIC_DRAW);
  
  // Create the cube vertex buffer
  //glGenBuffers( 1, &b_unit_vbo );
  //glBindBuffer( GL_ARRAY_BUFFER, b_unit_vbo );
  //glBufferData( GL_ARRAY_BUFFER, (2*12)*3*sizeof(float),
  //		UnitCube,GL_STATIC_DRAW );
  
  // Specify the means of extracting the position values properl
  //GLint posAttrib = m_shader.getAttribLocation( "position" );
  //glEnableVertexAttribArray( posAttrib );
  //glVertexAttribPointer( posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );
  
  // Reset state to prevent rogue code from messing with *my* 
  // stuff!
  //glBindVertexArray( 0 );
  //glBindBuffer( GL_ARRAY_BUFFER, 0 );
  //glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

    theMaze.Printed();

    // OpenGL has the buffer now, there's no need for us to keep a copy.
    //delete [] verts;
    CHECK_GL_ERRORS;
    
    // Persistence logic
    if(!Record){
      if(25*Deg>(1.0/180)){
	Deg*=0.99;
	
	T *= glm::translate(mat4(),MoveMaze);
	T *= glm::rotate(mat4(),(2*Deg * Sign) ,y_axis);
	T *= glm::translate(mat4(),ResetMaze);
      }
    }
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A1::guiLogic()
{
	// We already know there's only going to be one window, so for 
	// simplicity we'll store button states in static local variables.
	// If there was ever a possibility of having multiple instances of
	// A1 running simultaneously, this would break; you'd want to make
	// this into instance fields of A1.
	static bool showTestWindow(false);
	static bool showDebugWindow(true);

	ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
	float opacity(0.5f);

	ImGui::Begin("Debug Window", &showDebugWindow, ImVec2(100,100), opacity, windowFlags);
		if( ImGui::Button( "Quit Application" ) ) {
			glfwSetWindowShouldClose(m_window, GL_TRUE);
		}

		// Eventually you'll create multiple colour widgets with
		// radio buttons.  If you use PushID/PopID to give them all
		// unique IDs, then ImGui will be able to keep them separate.
		// This is unnecessary with a single colour selector and
		// radio button, but I'm leaving it in as an example.

		// Prefixing a widget name with "##" keeps it from being
		// displayed.

		ImGui::PushID( 0 );
		ImGui::ColorEdit3( "##Colour", colour );
		ImGui::SameLine();
		if( ImGui::RadioButton( "##Col", &current_col, 0 ) ) {
		  ColorPtr = WaCol;/*
		  WaCol[0] = colour[0];
		  WaCol[1] = colour[1];
		  WaCol[2] = colour[2];*/
		}
		
		ImGui::SameLine();
		if(ImGui::RadioButton("##AVT", &current_col,0) ){
		  ColorPtr = AvCol;
		  /*CFlag = 'A';
		  AvCol[0] = colour[0];
		  AvCol[1] = colour[1];
		  AvCol[2] = colour[2];
		  */}
		
		ImGui::SameLine();
		if(ImGui::RadioButton("##FLR", &current_col,0)){
		  ColorPtr = FlCol;/*
		  CFlag = 'F';
		 
		  FlCol[0] = colour[0];
		  FlCol[1] = colour[1];
		  FlCol[2] = colour[2];*/
		}
		ImGui::PopID();
		if(ColorPtr!=nullptr){
		  ColorPtr[0] = colour[0];
		  ColorPtr[1] = colour[1];
		  ColorPtr[2] = colour[2];
		}
/*
		// For convenience, you can uncomment this to show ImGui's massive
		// demonstration window right in your application.  Very handy for
		// browsing around to get the widget you want.  Then look in 
		// shared/imgui/imgui_demo.cpp to see how it's done.
		if( ImGui::Button( "Test Window" ) ) {
			showTestWindow = !showTestWindow;
		}
*/

		ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

	ImGui::End();

	if( showTestWindow ) {
		ImGui::ShowTestWindow( &showTestWindow );
	}
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A1::draw()
{
  // Create a global transformation for the model (centre it).
  mat4 W;
  W = glm::translate( W, vec3( -float(DIM)/2.0f, 0, -float(DIM)/2.0f ) );
	
  m_shader.enable();
  glEnable( GL_DEPTH_TEST );
	
  glUniformMatrix4fv( P_uni, 1, GL_FALSE, value_ptr( proj ) );
  glUniformMatrix4fv( V_uni, 1, GL_FALSE, value_ptr( view ) );
  glUniformMatrix4fv( M_uni, 1, GL_FALSE, value_ptr( W ) );
  // try to do transform
  glUniformMatrix4fv( T_uni, 1, GL_FALSE, value_ptr(T));

  // Draw floor
  glBindVertexArray( floor_vao);
  glUniform3f( col_uni,FlCol[0],FlCol[1],FlCol[2]);
  glDrawArrays(GL_TRIANGLE_STRIP,0,4);
  
  // Just draw the grid for now.
  glBindVertexArray( m_grid_vao );
  glUniform3f( col_uni, 1, 1, 1 );
  glDrawArrays( GL_LINES, 0, (3+DIM)*4 );
  // playing around with it glDrawArrays( GL_LINES, 2, (3+DIM)*4 );

  // Draw the cubes
  int Count = CDrawer.GetCubeCount();
  GLuint* VAO = CDrawer.GetVAOArray();
  for(int i = 0; i<Count;i++){
    glBindVertexArray( VAO[i] );
    glUniform3f(col_uni,WaCol[0],WaCol[1],WaCol[2]);
    glDrawElements(GL_TRIANGLE_STRIP,13,GL_UNSIGNED_INT,0);
  }
  // Draw Avatar
  m_shader.disable();

  CDrawer.EnableShader();
  CDrawer.SetShaderUniform(proj,view,W);
  glm::vec3 Camera = glm::vec3( 0.0f, 2.*float(DIM)*2.0*M_SQRT1_2, float(DIM)*2.0*M_SQRT1_2 );
  CDrawer.SetViewPort(Camera);
  CDrawer.SetShaderLoc(T);
  CDrawer.SetColour(AvCol);
  CDrawer.DrawShadedAv();/*
  if(CDrawer.AvatarChanged()) CDrawer.LagTrans();
  GLuint AvatarVAO = CDrawer.GetAVAO();
  int EdgeCount = CDrawer.GetE();
  glBindVertexArray(AvatarVAO);
  glUniform3f(col_uni,AvCol[0],AvCol[1],AvCol[2]);
  glUniformMatrix4fv( T_uni, 1, GL_FALSE, value_ptr(T * CDrawer.GetTransform()));
  glDrawElements(GL_TRIANGLE_STRIP,EdgeCount*(EdgeCount-1)+1,GL_UNSIGNED_INT,0);
			 */
  CDrawer.DisableShader();
  // Highlight the active square.
  //m_shader.disable();

  // Restore defaults
  glBindVertexArray( 0 );

  CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A1::cleanup()
{}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A1::cursorEnterWindowEvent (
		int entered
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A1::mouseMoveEvent(double xPos, double yPos) 
{
	bool eventHandled(false);

	if (!ImGui::IsMouseHoveringAnyWindow()) {
	  // record start location
	  if(Record == false){
	    RotationStart.x = (float)xPos;
	    RotationStart.z = (float)yPos;
	  }else if(Record == true){
	    RotationEnd.x = (float)xPos;
	    RotationEnd.z = (float)yPos;

	    // get rotation center
	    int w,h;
	    glfwGetWindowSize(m_window,&w,&h);
	    Center.x = h/2.0f;
	    Center.y = 0.0f;
	    Center.z = w/2.0f;

	    // Convert Dots into Vectors
	    
	    RotationStart = RotationStart-Center;
	    RotationEnd = RotationEnd - Center;
	    LawVec.x = RotationStart.z;
	    LawVec.z = - RotationStart.x;

	    // by comparing the cos of end to LawScalar, we know the sign
	    float LawScalar = glm::dot(LawVec,RotationEnd);
	    Sign = (LawScalar>0)? 1:-1;

	    float COS = glm::dot(RotationStart,RotationEnd)/(glm::length(RotationStart)*glm::length(RotationEnd));
	    Deg = glm::acos(COS);

	    T *= glm::translate(mat4(),MoveMaze);
	    T *= glm::rotate(mat4(),(Deg * Sign) ,y_axis);
	    T *= glm::translate(mat4(),ResetMaze);

	    RotationStart.x = (float)xPos;
	    RotationStart.z = (float)yPos;
	  }
	  // in reverse order, move center of maze to y axis
	  // then rotate
	  // then get it back to original place
	  /*
	  if(Deg>0){
	  }*/
	  //cout<<xPos<<","<<yPos<<endl;
	  // Put some code here to handle rotations.  Probably need to
	  // check whether we're *dragging*, not just moving the mouse.
	  // Probably need some instance variables to track the current
	  // rotation amount, and maybe the previous X position (so 
	  // that you can rotate relative to the *change* in X.
	}

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A1::mouseButtonInputEvent(int button, int actions, int mods) {
	bool eventHandled(false);
	
	if (!ImGui::IsMouseHoveringAnyWindow()) {
	  if(button == GLFW_MOUSE_BUTTON_LEFT && actions == GLFW_PRESS){
	    Record = true;
	  }
	  if(button == GLFW_MOUSE_BUTTON_LEFT && actions == GLFW_RELEASE){
	    Record = false;
	  }
	  
		// The user clicked in the window.  If it's the left
		// mouse button, initiate a rotation.
	}

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A1::mouseScrollEvent(double xOffSet, double yOffSet) {
	bool eventHandled(false);

	// Zoom in or out.W

	// set upper and lower bound
	CurrentPerspecDeg -= yOffSet;
	if(CurrentPerspecDeg < 0){
	  CurrentPerspecDeg = 0;
	}else if(CurrentPerspecDeg > 180){
	  CurrentPerspecDeg = 180;
	}

	// set perspective
	proj = glm::perspective(
				//glm::radians( 10.0f),
				// place the camera closer with a smaller value
				glm::radians(CurrentPerspecDeg ),
				float( m_framebufferWidth ) / float( m_framebufferHeight ),
				1.0f, 1000.0f );
	eventHandled = true;
	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A1::windowResizeEvent(int width, int height) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A1::keyInputEvent(int key, int action, int mods) {
	bool eventHandled(false);
	int AX,AY,offsetX,offsetY;
	offsetX=offsetY=0;
	size_t theDim = theMaze.getDim();
	// Fill in with event handling code...
	if( action == GLFW_PRESS ) {
	  // Respond to some key events.
	  switch (key){
	  case GLFW_KEY_Q:
	    glfwSetWindowShouldClose(m_window, GL_TRUE);
	    break;
	  case GLFW_KEY_R:
	    Reset();
	    break;
	  case GLFW_KEY_D:
	    ReDig();
	    break;
	  case GLFW_KEY_SPACE:
	    CDrawer.Higher();
	    break;
	  case GLFW_KEY_BACKSPACE:
	    CDrawer.Lower();
	    break;
	  case GLFW_KEY_LEFT:
	    offsetY=-1;
	    break;
	  case GLFW_KEY_RIGHT:
	    offsetY=1;
	    break;
	  case GLFW_KEY_UP:
	    offsetX=-1;
	    break;
	  case GLFW_KEY_DOWN:
	    offsetX=1;
	    break;
	  case GLFW_KEY_LEFT_SHIFT:
	  case GLFW_KEY_RIGHT_SHIFT:
	    ShiftPressed = true;
	    break;
	  }
	}else if(action == GLFW_RELEASE){
	  switch(key){
	  case GLFW_KEY_LEFT_SHIFT:
	  case GLFW_KEY_RIGHT_SHIFT:
	    ShiftPressed = false;
	    break;
	  }
	}
	// Try to move avatar
	if(offsetX!=0 || offsetY!=0){
	  AX = CDrawer.GetAX();
	  AY = CDrawer.GetAY();
	  if(!((AX==-1 && offsetX<0) || (AY==-1 && offsetY<0)) &&
	     !((AX==theDim && offsetX>0) || (AY==theDim && offsetY>0))){
	    AX+=offsetX;
	    AY+=offsetY;
	    if(AX>=0 && AX<theDim && AY>=0 && AY<theDim &&
	       theMaze.getValue(AX,AY)){
	      // if with shift, we delete walls
	      if(ShiftPressed){
		theMaze.setValue(AX,AY,0);
		CDrawer.MoveAvatar(AX,AY);
	      }
	    }else{
	      CDrawer.MoveAvatar(AX,AY);
	    }
	    offsetX=offsetY=0;
	    
	  }
	}
	eventHandled = true;
	
	return eventHandled;
}

void A1::Reset(){
  CurrentPerspecDeg = DefaultPerspecDeg;
  proj = glm::perspective(
			  //glm::radians( 10.0f),
			  // place the camera closer with a smaller value
			  glm::radians(CurrentPerspecDeg ),
			  float( m_framebufferWidth ) / float( m_framebufferHeight ),
			  1.0f, 1000.0f );
  Deg = 0.0f;
  Sign = 1;
  T = glm::translate(mat4(),MoveMaze);
  T *= glm::rotate(mat4(),(2*Deg * Sign) ,y_axis);
  T *= glm::translate(mat4(),ResetMaze);
  CDrawer.ResetHight();

  // Reset Colors:

  ColorPtr = nullptr;
  
  FlCol[0]=Flor[0];
  FlCol[1]=Flor[1];
  FlCol[2]=Flor[2];

  AvCol[0]=Avat[0];
  AvCol[1]=Avat[1];
  AvCol[2]=Avat[2];

  WaCol[0]=Wall[0];
  WaCol[1]=Wall[1];
  WaCol[2]=Wall[2];

  theMaze.reset();
  CDrawer.SetAvatar(0,0);

}

void A1::initFloor(){

  size_t S = 3*2*2;
  
  float Vertices[] = {-1.0f,0.0f,-1.0f,
		      DIM+1,0.0f,-1.0f,
		      -1.0f,0.0f,DIM+1,
		      DIM+1,0.0f,DIM+1
  };
  FlCol[0]=Flor[0];
  FlCol[1]=Flor[1];
  FlCol[2]=Flor[2];
  // Create the vertex array to record buffer assignments.
  glGenVertexArrays( 1, &floor_vao );
  glBindVertexArray( floor_vao );

  // Create the cube vertex buffer
  glGenBuffers( 1, &floor_vbo );
  glBindBuffer( GL_ARRAY_BUFFER, floor_vbo );
  glBufferData( GL_ARRAY_BUFFER, S*sizeof(float),
		Vertices, GL_STATIC_DRAW );

  // Specify the means of extracting the position values properl
  GLint posAttrib = m_shader.getAttribLocation( "position" );
  glEnableVertexAttribArray( posAttrib );
  glVertexAttribPointer( posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr );

  // Reset state to prevent rogue code from messing with *my* 
  // stuff!
  glBindVertexArray( 0 );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

  CHECK_GL_ERRORS;
}


void A1::ReDig()
{
  //Maze m(DIM);
  theMaze.digMaze();
  theMaze.printMaze();
  CDrawer.SetAvatar(0,theMaze.getEntry());
  
  vector<int> Cubes;
  theMaze.GetCubes(Cubes);
  CDrawer.SetCubes(Cubes);
}

