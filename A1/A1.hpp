// Winter 2019

#pragma once

#include <glm/glm.hpp>

#include "cs488-framework/CS488Window.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/ShaderProgram.hpp"

#include "maze.hpp"
#include "CubeDrawer.hpp"

class A1 : public CS488Window {
public:
	A1();
	virtual ~A1();

protected:
  virtual void init() override;
  virtual void appLogic() override;
  virtual void guiLogic() override;
  virtual void draw() override;
  virtual void cleanup() override;
  virtual void Reset();
  virtual void ReDig();
  virtual bool cursorEnterWindowEvent(int entered) override;
  virtual bool mouseMoveEvent(double xPos, double yPos) override;
  virtual bool mouseButtonInputEvent(int button, int actions, int mods) override;
  virtual bool mouseScrollEvent(double xOffSet, double yOffSet) override;
  virtual bool windowResizeEvent(int width, int height) override;
  virtual bool keyInputEvent(int key, int action, int mods) override;

private:
  void initGrid();
  void initFloor();
  //void DrawFloor();
  // Fields related to the shader and uniforms.
  ShaderProgram m_shader;
  GLint P_uni; // Uniform location for Projection matrix.
  GLint V_uni; // Uniform location for View matrix.
  GLint M_uni; // Uniform location for Model matrix.
  GLint T_uni; // Unifrom location for Transform Matrix
  GLint col_uni;   // Uniform location for cube colour.
  
  // Fields related to grid geometry.
  GLuint m_grid_vao; // Vertex Array Object
  GLuint m_grid_vbo; // Vertex Buffer Object

  // Blocks
  GLuint floor_vao;
  GLuint floor_vbo;

  
  // Matrices controlling the camera and projection.
  glm::mat4 proj;
  glm::mat4 view;

  // Matrix controlling the model
  glm::mat4 T;

  Maze theMaze;
  CubeDrawer CDrawer;
  // zoom in zoom out
  const float DefaultPerspecDeg;
  float CurrentPerspecDeg;


  // moving avatar, shift
  bool ShiftPressed;
  // Rotation
  bool Record;
  int Sign;
  float Deg;
  glm::vec3 RotationStart;
  glm::vec3 RotationEnd;
  glm::vec3 LawVec;
  glm::vec3 Center;

  // Colours
  float colour[3];
  float WaCol[3];
  float AvCol[3];
  float FlCol[3];
  float* ColorPtr;
  int current_col;
};
