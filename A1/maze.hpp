// Winter 2019

#pragma once

#include <vector>

class Maze
{
public:
  Maze( size_t dim );
  ~Maze();
  
  void reset();
  
  size_t getDim() const;

  int getValue( int x, int y ) const;
  int getEntry() const;

  bool HasChanged()const;
  void GetCubes(std::vector<int> &Cubes);

  void setValue( int x, int y, int h );
  void Printed();
  
  void digMaze();
  void printMaze(); // for debugging
  // reset to start date, not fully reset
  void ResetMaze();
  
private:
  size_t m_dim;
  int *m_values;
  int *backup;
  bool Changed;
  void recDigMaze(int r, int c);
  int numNeighbors(int r, int c);
  void GetLargeColumns(std::vector<int>& Cubes) const;
  void GetLargeRows(std::vector<int>& Cubes) const;
};
