// Winter 2019

#pragma once

#include "SceneNode.hpp"

class GeometryNode : public SceneNode {
public:
  GeometryNode(
	       const std::string & meshId,
	       const std::string & name
	       );

  Material material;

  // Mesh Identifier. This must correspond to an object name of
  // a loaded .obj file.
  void draw(const ShaderProgram& S,const glm::mat4& V, BatchInfoMap& BM) const override;
  void updateUniforms(const ShaderProgram& S, const glm::mat4& V) const;

  std::string meshId;
};
