-- Simple Scene:
-- An extremely simple scene that will render right out of the box
-- with the provided skeleton code.  It doesn't rely on hierarchical
-- transformations.

-- Create the top level root node named 'root'.
rootNode = gr.node('root')

Torso = gr.mesh("torso","torso")
Torso:scale(0.005, 0.004, 0.005)
Torso:rotate('y', 90.0)
Torso:translate(0.225, -0.6, -0.3)
Torso:set_material(gr.material({0.2,0.2,0.2}, {1.0,1.0,1.0}, 10.0))
rootNode:add_child(Torso)

-- Create sholder joints
--TChest = gr.mesh('sphere', 'leftShoulder')
TChest = gr.joint('TChest',{-15,0,15},{0,0,0})
--TChest:scale(0.1,0.1,0.1)
TChest:translate(0.0,0.0,-0.3)
--TChest:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
Torso:add_child(TChest)

-- Create a GeometryNode with MeshId = 'cube', and name = 'torso'.
-- MeshId's must reference a .obj file that has been previously loaded into
-- the MeshConsolidator instance within A3::init().
cubeMesh = gr.mesh('chest', 'chest')
cubeMesh:scale(0.005, 0.005, 0.005)
cubeMesh:rotate('y', 90.0)
cubeMesh:translate(0.0, 0.3, 0.0)
cubeMesh:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
TChest:add_child(cubeMesh)

-- Create sholder joints
--THip = gr.mesh('sphere', 'leftShoulder')
THip = gr.joint('THip',{-15,0,15},{0,0,0})
--THip:scale(0.1,0.1,0.1)
THip:translate(0.0,-1,0.0)
--THip:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
Torso:add_child(THip)

Hip = gr.mesh('hip','hip')
Hip:scale(0.005, 0.005, 0.005)
Hip:rotate('y', 90.0)
Hip:translate(0.0, -0.45, -0.3)
Hip:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
THip:add_child(Hip)

-- Add the cubeMesh GeometryNode to the child list of rootnode


-- Create sholder joints
--LeftShoulder = gr.mesh('sphere', 'leftShoulder')
LeftShoulder = gr.joint('leftShoulder',{-45,0,45},{0,0,0})
--LeftShoulder:scale(0.1,0.1,0.1)
LeftShoulder:translate(0.5, 0.0, -0.3)
--LeftShoulder:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
cubeMesh:add_child(LeftShoulder)

--RightShoulder = gr.mesh('sphere', 'rightShoulder'
RightShoulder = gr.joint('rightShoulder',{-45,0,45},{0,0,0})
--RightShoulder:scale(0.1,0.1,0.1)
RightShoulder:translate(-0.5, 0.0, -0.3)
--RightShoulder:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
cubeMesh:add_child(RightShoulder)

-- Create neck joint
Neck = gr.joint('neck',{-15,0,15},{-90,0,90})
--Neck:scale(0.1,0.1,0.1)
Neck:translate(0.0,0,-0.3)
--Neck:set_material(gr.material({0.0,0.4,0.4},{0.8,0.8,0.8},50))
cubeMesh:add_child(Neck)

-- Create arms

ULL = gr.mesh('arm','UpLeftLimb')
ULL:scale(0.005,0.005,0.005)
ULL:rotate('y',90)
ULL:translate(1.2,0,-0.55)
ULL:set_material(gr.material({1.0,1.0,1.0},{1.0,1.0,1.0},50.0))
LeftShoulder:add_child(ULL)

--LEL = gr.mesh('sphere','leftElbow')
LEL = gr.joint('leftElbow',{-45,0,45},{0,0,0})
--LEL:scale(0.1,0.1,0.1)
LEL:translate(0.85,-0.9,-0.335)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
ULL:add_child(LEL)

LFJ = gr.mesh('elbow','leftFakeElbow')
LFJ:scale(0.005,0.005,0.005)
LFJ:rotate('y',90)
LFJ:translate(0.85,-0.9,-0.335)
LFJ:set_material(gr.material({0.2,0.2,0.2},{0.5,0.5,0.5},10.0))
LEL:add_child(LFJ)

LLL = gr.mesh('limb','LowerLeftLimb')
LLL:scale(0.004,0.004,0.004)
LLL:rotate('y',90)
LLL:translate(0.9,-0.8,-0.48)
LLL:set_material(gr.material({1.0,1.0,1.0},{1.0,1.0,1.0},50.0))
LEL:add_child(LLL)

URL = gr.mesh('arm','UpRightLimb')
URL:scale(0.005,0.005,0.005)
URL:rotate('y',-90)
URL:translate(-1.2,0,-0.1)
URL:set_material(gr.material({1.0,1.0,1.0},{1.0,1.0,1.0},50.0))
RightShoulder:add_child(URL)

--REL = gr.mesh('sphere','rightElbow')
REL = gr.joint('rightElbow',{-45,0,45},{0,0,0})
--REL:scale(0.1,0.1,0.1)
REL:translate(-0.85,-0.9,-0.335)
--REL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
URL:add_child(REL)


RFJ = gr.mesh('elbow','rightFakeElbow')
RFJ:scale(0.005,0.005,0.005)
RFJ:rotate('y',-90)
RFJ:translate(-0.85,-0.9,-0.335)
RFJ:set_material(gr.material({0.2,0.2,0.2},{0.5,0.5,0.5},10.0))
REL:add_child(RFJ)

RLL = gr.mesh('limb','LowerRightLimb')
RLL:scale(0.004,0.004,0.004)
RLL:rotate('y',-90)
RLL:translate(-0.9,-0.8,-0.2)
RLL:set_material(gr.material({1.0,1.0,1.0},{1.0,1.0,1.0},50.0))
REL:add_child(RLL)

--BSJ = gr.mesh('sphere','leftElbow')
BSJ = gr.joint('bsj',{-7.5,0,7.5},{0,0,0})
--BSJ:scale(0.1,0.1,0.1)
BSJ:translate(0,-1.3,-0.465)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
Hip:add_child(BSJ)
 
BS = gr.mesh('bshield','backshield')
BS:scale(0.005,0.005,0.005)
BS:rotate('y',90)
BS:translate(-0,0,0)
BS:set_material(gr.material({1.0,1.0,1.0},{1.0,1.0,1.0},50.0))
BSJ:add_child(BS)



--ULTJ = gr.mesh('sphere','ultj')
ULTJ = gr.joint('urtj',{-45,0,45},{0,0,0})
--ULTJ:scale(0.1,0.1,0.1)
ULTJ:translate(0.1,-1.3,-0.335)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
Hip:add_child(ULTJ)

ULT = gr.mesh('tigh','UpLeftTigh')
ULT:scale(0.005,0.005,0.005)
ULT:rotate('y',90);
ULT:translate(0.0,0.2,0.075)
ULT:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
ULTJ:add_child(ULT)

--URTJ = gr.mesh('sphere','urtj')
URTJ = gr.joint('urtj',{-45,0,45},{0,0,0})
--URTJ:scale(0.1,0.1,0.1)
URTJ:translate(-0.1,-1.3,-0.335)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
Hip:add_child(URTJ)

URT = gr.mesh('tigh','UpRightTigh')
URT:scale(0.005,0.005,0.005)
URT:rotate('y',90);
URT:translate(-0.8,0.2,0.075)
URT:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
URTJ:add_child(URT)

--RSJ = gr.mesh('sphere','RSJ')
RSJ = gr.joint('RSJ',{-90,0,90},{0,0,0})
--RSJ:scale(0.1,0.1,0.1)
RSJ:translate(-0.6,-1.3,-0.335)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
URT:add_child(RSJ)

RS = gr.mesh('sshield','RS')
RS:scale(0.005,0.005,0.005)
RS:rotate('y',-90);
RS:translate(0,0.2,-0.6)
RS:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
RSJ:add_child(RS)

--LSJ = gr.mesh('sphere','LSJ')
LSJ = gr.joint('LSJ',{-90,0,90},{0,0,0})
--LSJ:scale(0.1,0.1,0.1)
LSJ:translate(0.6,-1.3,-0.335)
--LEL:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
ULT:add_child(LSJ)

LS = gr.mesh('sshield','LS')
LS:scale(0.005,0.005,0.005)
LS:rotate('y',90);
LS:translate(0,0.2,0)
LS:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
LSJ:add_child(LS)



-- Create a GeometryNode with MeshId = 'sphere', and name = 'head'.
sphereMesh = gr.mesh('head', 'head')
sphereMesh:scale(0.008, 0.008, 0.008)
sphereMesh:rotate('y', 90.0)
sphereMesh:translate(0.0, 0.3, 0.0)
sphereMesh:set_material(gr.material({1.0, 1.0, 1.0}, {1.0,1.0,1.0}, 50.0))


-- Add the sphereMesh GeometryNode to the child list of rootnode.
Neck:add_child(sphereMesh)


--LRJ = gr.mesh('sphere','LRJ')
LRJ = gr.joint('LRJ',{-7.5,0,7.5},{0,0,0})
--LRJ:scale(0.1,0.1,0.1)
LRJ:translate(0.3,-0.4,-0.75)
--LRJ:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
cubeMesh:add_child(LRJ)

LR = gr.mesh('rocket','LR')
LR:scale(0.005,0.005,0.005)
LR:rotate('x',45);
LR:translate(-0.7,-0.4,-1.1)
LR:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
LRJ:add_child(LR)


--RRJ = gr.mesh('sphere','RRJ')
RRJ = gr.joint('RRJ',{-7.5,0,7.5},{0,0,0})
--RRJ:scale(0.1,0.1,0.1)
RRJ:translate(-0.3,-0.4,-0.75)
--RRJ:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
cubeMesh:add_child(RRJ)

RR = gr.mesh('rocket','RR')
RR:scale(0.005,0.005,0.005)
RR:rotate('x',45);
RR:translate(-1.3,-0.4,-1.1)
RR:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
RRJ:add_child(RR)

--ShieldJ = gr.mesh('sphere','ShieldJ')
ShieldJ = gr.joint('ShieldJ',{-180,0,180},{0,0,0})
--ShieldJ:scale(0.1,0.1,0.1)
ShieldJ:translate(1.4,-1.2,-0.35)
--ShieldJ:set_material(gr.material({0.0, 0.4, 0.4}, {0.8, 0.8, 0.8}, 50.0))
LLL:add_child(ShieldJ)

Shield = gr.mesh('shield','Shield')
Shield:scale(0.005,0.005,0.005)
Shield:rotate('y',-90);
Shield:translate(1.6,0,0)
Shield:set_material(gr.material({1.0,1.0,1.0}, {1.0,1.0,1.0}, 50.0))
ShieldJ:add_child(Shield)




-- Return the root with all of it's childern.  The SceneNode A3::m_rootNode will be set
-- equal to the return value from this Lua script.

rootNode:translate(0.0,1.0,-5.0)


return rootNode
