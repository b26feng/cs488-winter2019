#version 330

struct LightSource {
    vec3 position;
    vec3 rgbIntensity;
};

in VsOutFsIn {
	vec3 position_ES; // Eye-space position
	vec3 normal_ES;   // Eye-space normal
	LightSource light;
} fs_in;


out vec4 fragColour;

struct Material {
    vec3 kd;
    vec3 ks;
    float shininess;
};
uniform Material material;

uniform float modelIdColor;
uniform int Picking;


// Ambient light intensity for each RGB component.
uniform vec3 ambientIntensity;


void main() {
     fragColour = vec4(modelIdColor,0.0,0.0,1.0);
}
