// Winter 2019

#include "SceneNode.hpp"

#include "cs488-framework/MathUtils.hpp"

#include <iostream>
#include <sstream>
using namespace std;

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>

using namespace glm;


// Static class variable
unsigned int SceneNode::nodeInstanceCount = 0;


//---------------------------------------------------------------------------------------
SceneNode::SceneNode(const std::string& name)
  : m_name(name),
    m_nodeType(NodeType::SceneNode),
    trans(mat4()),
    isSelected(false),
    m_nodeId(nodeInstanceCount++),
    Parent(nullptr)
{
  TMatrix = glm::mat4();
  RMatrix = glm::mat4();
}

//---------------------------------------------------------------------------------------
// Deep copy
SceneNode::SceneNode(const SceneNode & other)
  : m_nodeType(other.m_nodeType),
    m_name(other.m_name),
    trans(other.trans),
    invtrans(other.invtrans),
    initTrans(other.initTrans),
    initInv(other.initInv)
{
  for(SceneNode * child : other.children) {
    this->children.push_front(new SceneNode(*child));
  }
}

//---------------------------------------------------------------------------------------
SceneNode::~SceneNode() {
  for(SceneNode * child : children) {
    delete child;
  }
}

//---------------------------------------------------------------------------------------
void SceneNode::set_transform(const glm::mat4& m) {
  trans = m;
  invtrans = glm::inverse(m);
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_transform() const {
  return trans;
}

//---------------------------------------------------------------------------------------
const glm::mat4& SceneNode::get_inverse() const {
  return invtrans;
}

//---------------------------------------------------------------------------------------
void SceneNode::add_child(SceneNode* child) {
  children.push_back(child);
}

//---------------------------------------------------------------------------------------
void SceneNode::remove_child(SceneNode* child) {
  children.remove(child);
}

//---------------------------------------------------------------------------------------
void SceneNode::rotate(char axis, float angle) {
  vec3 rot_axis;

  switch (axis) {
  case 'x':
    rot_axis = vec3(1,0,0);
    break;
  case 'y':
    rot_axis = vec3(0,1,0);
    break;
  case 'z':
    rot_axis = vec3(0,0,1);
    break;
  default:
    break;
  }
  mat4 rot_matrix = glm::rotate(degreesToRadians(angle), rot_axis);
  trans = rot_matrix * trans;
  invtrans = glm::inverse(trans);
}

//---------------------------------------------------------------------------------------
void SceneNode::scale(const glm::vec3 & amount) {
  trans = glm::scale(amount) * trans;
  invtrans = glm::inverse(trans);
}

//---------------------------------------------------------------------------------------
void SceneNode::translate(const glm::vec3& amount) {
  trans = glm::translate(amount) * trans;
  invtrans = glm::inverse(trans);
}


// child need to overwrite this
void SceneNode::draw(const ShaderProgram& S,const glm::mat4& V,BatchInfoMap& BM)const {
  
  for(SceneNode* i:children){
    i->draw(S,V,BM);
  }
}

// setup parent
int SceneNode::setParentAndInitTrans(SceneNode* P){
  this->Parent = P;
  int NodeCount = 1;
  
  //SceneNode* Root = this;
  //while(Root->Parent != nullptr){
  //  Root = Root->Parent;
  //}
  
  //trans = (P == nullptr)? trans: trans * Root->initTrans;
  //invtrans = (P == nullptr)? invtrans: invtrans * Root->initInv;
  initTrans = trans;
  initInv = invtrans;
  //PShader = Pick;
  for(SceneNode* i: children){
    NodeCount += i->setParentAndInitTrans(this);
  }
  return NodeCount;
}


void SceneNode::InitJoint(){
  for(SceneNode* i: children){
    i->InitJoint();
  }
}

void SceneNode::RotateAllChild(const glm::mat4& M){
  trans = M * trans;
  invtrans = glm::inverse(trans);
  for(SceneNode* i: children){
    i->RotateAllChild(M);
  }
}

void SceneNode::ApplyRootTranslate(glm::mat4& T){
  if(Parent != nullptr){
    trans = T * trans;
    invtrans = glm::inverse(trans);
  }
  for(SceneNode* i: children){
    i->ApplyRootTranslate(T);
  }
}

void SceneNode::SetPicking(bool Picking){
  Pick = Picking;
  for(SceneNode* i: children){
    i->SetPicking(Picking);
  }
}

void SceneNode::SetNodeArray(SceneNode** Arr){
  Arr[m_nodeId] = this;
  for(SceneNode *i: children){
    i->SetNodeArray(Arr);
  }
}

void SceneNode::RTranslate(const glm::vec3& amount){
  glm::mat4 TMat = glm::translate(amount);
  RTranslate(TMat);
}

void SceneNode::RTranslate(const glm::mat4& TMat){
  TMatrix = TMat * TMatrix;
  trans = TMat * trans;
  invtrans = glm::inverse(trans);
  for(SceneNode* i: children){
    i->RTranslate(TMat);
  }
}

void SceneNode::RRotate(const glm::mat4& RMat){
  RMatrix = RMatrix * RMat;
  glm::mat4 SelfRot = trans * RMat * invtrans;
  RotateAllChild(SelfRot);
  /*
  for(SceneNode* i: children){
    i->RRotate(RMat);
    }*/
}

void SceneNode::FullReset(){
  this->trans = initTrans;
  this->invtrans = initInv;
  JMode = Pick = isSelected = false;
  
  for(SceneNode* i:children){
    i->FullReset();
  }
}

void SceneNode::SetJMode(bool J){
  JMode = J;
  for(SceneNode* i:children){
    i->SetJMode(J);
  }
}

//---------------------------------------------------------------------------------------
int SceneNode::totalSceneNodes() const {
  return nodeInstanceCount;
}


//---------------------------------------------------------------------------------------
std::ostream & operator << (std::ostream & os, const SceneNode & node) {

  //os << "SceneNode:[NodeType: ___, name: ____, id: ____, isSelected: ____, transform: ____"
  switch (node.m_nodeType) {
  case NodeType::SceneNode:
    os << "SceneNode";
    break;
  case NodeType::GeometryNode:
    os << "GeometryNode";
    break;
  case NodeType::JointNode:
    os << "JointNode";
    break;
  }
  os << ":[";

  os << "name:" << node.m_name << ", ";
  os << "id:" << node.m_nodeId;
  os << "]";

  return os;
}
