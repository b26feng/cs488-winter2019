// Winter 2019

#include "GeometryNode.hpp"
#include "JointNode.hpp"
#include "cs488-framework/GlErrorCheck.hpp"
#include "cs488-framework/OpenGLImport.hpp"
#include "cs488-framework/MathUtils.hpp"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>


//---------------------------------------------------------------------------------------
GeometryNode::GeometryNode(
		const std::string & meshId,
		const std::string & name
)
	: SceneNode(name),
	  meshId(meshId)
{
	m_nodeType = NodeType::GeometryNode;
}

void GeometryNode::draw(const ShaderProgram& S, const glm::mat4& V,BatchInfoMap& BM) const{

  /*
  std::cout<<glm::to_string(trans[0])<<std::endl;
  std::cout<<glm::to_string(trans[1])<<std::endl;
  std::cout<<glm::to_string(trans[2])<<std::endl;
  std::cout<<glm::to_string(trans[3])<<std::endl;
  std::cout<<std::endl;*/

  //std::cout<<m_name<<"'s parent is :"<<Parent->m_name<<std::endl;

  S.enable();
  GLint location = S.getUniformLocation("Picking");
  if(Pick){
    glUniform1i(location,1);
  }else{
    glUniform1i(location,0);
  }
  S.disable();

  updateUniforms(S,V);
  BatchInfo batchInfo = BM[meshId];
  // render the mesh
  S.enable();
  glDrawArrays(GL_TRIANGLES,batchInfo.startIndex,batchInfo.numIndices);
  S.disable();

  for(SceneNode* i: children){
    i->draw(S,V,BM);
  }
  
}

void GeometryNode::updateUniforms(const ShaderProgram& S,const glm::mat4& V) const{
  S.enable();
  {
    GLint location = S.getUniformLocation("ModelView");
    float BaseFloat = 0.0f;
    glm::mat4 modelView = V * trans;
    glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(modelView));
    CHECK_GL_ERRORS;
  
    //-- Set NormMatrix:
    location = S.getUniformLocation("NormalMatrix");
    glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelView)));
    glUniformMatrix3fv(location, 1, GL_FALSE, value_ptr(normalMatrix));
    CHECK_GL_ERRORS;

    // -- Set Picking Color
    location = S.getUniformLocation("modelIdColor");
    glm::vec3 PColor = glm::vec3(0.0f);
    if(isSelected){
      PColor.z = 0.5f;
      PColor.y = m_nodeId/50.0f;
    }else{
      PColor.x = m_nodeId/50.0f;
    }
    glUniform3fv(location,1,glm::value_ptr(PColor));
    CHECK_GL_ERRORS;
  
  
    //-- Set Material values:
    location = S.getUniformLocation("material.kd");
    glm::vec3 kd;
    if(isSelected && JMode){
      kd = glm::vec3(1.0f,0.8f,0.8f);
    }else{
      kd = material.kd;
    }
    glUniform3fv(location, 1, value_ptr(kd));
    CHECK_GL_ERRORS;
    location = S.getUniformLocation("material.ks");
    glm::vec3 ks = material.ks;
    glUniform3fv(location, 1, value_ptr(ks));
    CHECK_GL_ERRORS;
    location = S.getUniformLocation("material.shininess");
    glUniform1f(location, material.shininess);
    CHECK_GL_ERRORS;
  }
  S.disable();
}


/*
void GeometryNode::JointRotation(){
  for(SceneNdoe* i: children){
    i->JointRotation();
  }
  
  if(Parent!=nullptr){
    JointNode* P = static_cast<JointNode*>(Parent);
    glm::mat4 Rot;
    trans = P->invtrans * trans;
    Rot = glm::rotate(degreesToRadians((float)(P->XDeg)),glm::vec3(0,0,1));
    Rot *= glm::rotate(degreesToRadians((float)(P->YDeg)),glm::vec3(0,1,0));
    trans = Rot * trans;
    trans = P->trans * trans;
  }
  invtrans = glm::inverse(trans);
  //std::cout<<m_name<<" has:";
  for(SceneNode* i: children){
    //std::cout<<i->m_name<<" ";
    i->JointRotation();
  }
  //std::cout<<std::endl;
  
}

*/
