// Winter 2019

#include "A3.hpp"
#include "scene_lua.hpp"
using namespace std;

#include "cs488-framework/GlErrorCheck.hpp"
#include "cs488-framework/MathUtils.hpp"
#include "GeometryNode.hpp"
#include <imgui/imgui.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <math.h>
#include <algorithm>

using namespace glm;

static bool show_gui = true;

const size_t CIRCLE_PTS = 48;


#define ESC 27
#define RED() printf("%c[31m",ESC);
#define YELLOW() printf("%c[33m",ESC);
#define PURPLE() printf("%c[35m",ESC);
#define DEFAULT() printf("%c[39;49m",ESC);

//----------------------------------------------------------------------------------------
// Constructor
A3::A3(const std::string & luaSceneFile)
	: m_luaSceneFile(luaSceneFile),
	  m_positionAttribLocation(0),
	  m_normalAttribLocation(0),
	  m_vao_meshData(0),
	  m_vbo_vertexPositions(0),
	  m_vbo_vertexNormals(0),
	  m_vao_arcCircle(0),
	  m_vbo_arcCircle(0)
{
  PJ = true;
  MouseStatus = 0;
  CZBF = (1<<3 | 1<<2);
  cmdStack.Back = 0;
  cmdStack.Cur = 0;
  cmdStack.Stack = vector<vector<JointNode*>>(0);
  cmdStack.Ops = vector<vector<vec2>>(0);
}

//----------------------------------------------------------------------------------------
// Destructor
A3::~A3()
{
  delete[] AllNodes;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, at program start.
 */
void A3::init()
{
	// Set the background colour.
	glClearColor(0.85, 0.85, 0.85, 1.0);

	createShaderProgram();

	glGenVertexArrays(1, &m_vao_arcCircle);
	glGenVertexArrays(1, &m_vao_meshData);
	//glGenVertexArrays(1, &m_vao_Picking); // for picking 
	enableVertexShaderInputSlots();

	processLuaSceneFile(m_luaSceneFile);

	// Load and decode all .obj files at once here.  You may add additional .obj files to
	// this list in order to support rendering additional mesh types.  All vertex
	// positions, and normals will be extracted and stored within the MeshConsolidator
	// class.
	unique_ptr<MeshConsolidator> meshConsolidator (new MeshConsolidator{
	    getAssetFilePath("cube.obj"),
	      getAssetFilePath("head.obj"),
	      getAssetFilePath("chest.obj"),
	      getAssetFilePath("arm.obj"),
	      getAssetFilePath("elbow.obj"),
	      getAssetFilePath("torso.obj"),
	      getAssetFilePath("hip.obj"),
	      getAssetFilePath("limb.obj"),
	      getAssetFilePath("tigh.obj"),
	      getAssetFilePath("bshield.obj"),
	      getAssetFilePath("sshield.obj"),
	      getAssetFilePath("rocket.obj"),
	      getAssetFilePath("shield.obj"),
	      
	      getAssetFilePath("sphere.obj"),
	      getAssetFilePath("suzanne.obj"),
	      getAssetFilePath("tri.obj"),
	      getAssetFilePath("invtri.obj"),
	      });


	// Acquire the BatchInfoMap from the MeshConsolidator.
	meshConsolidator->getBatchInfoMap(m_batchInfoMap);

	// Take all vertex data within the MeshConsolidator and upload it to VBOs on the GPU.
	uploadVertexDataToVbos(*meshConsolidator);

	mapVboDataToVertexShaderInputLocations();

	initPerspectiveMatrix();

	initViewMatrix();

	initLightSources();

	NodeCount = m_rootNode->setParentAndInitTrans(nullptr);
	AllNodes = new SceneNode* [NodeCount];
	m_rootNode->SetNodeArray(AllNodes);
	m_rootNode->SetPicking(false);
	m_rootNode->ApplyRootTranslate(m_rootNode->trans);
	m_rootNode->InitJoint();
	// record neck
	for(int i = 0;i<NodeCount;i++){
	  if(AllNodes[i]->m_name == "neck"){
	    Neck = static_cast<JointNode*>(AllNodes[i]);
	  }
	}

	// Exiting the current scope calls delete automatically on meshConsolidator freeing
	// all vertex data resources.  This is fine since we already copied this data to
	// VBOs on the GPU.  We have no use for storing vertex data on the CPU side beyond
	// this point.


}

//----------------------------------------------------------------------------------------
void A3::processLuaSceneFile(const std::string & filename) {
	// This version of the code treats the Lua file as an Asset,
	// so that you'd launch the program with just the filename
	// of a puppet in the Assets/ directory.
	// std::string assetFilePath = getAssetFilePath(filename.c_str());
	// m_rootNode = std::shared_ptr<SceneNode>(import_lua(assetFilePath));

	// This version of the code treats the main program argument
	// as a straightforward pathname.
	m_rootNode = std::shared_ptr<SceneNode>(import_lua(filename));
	if (!m_rootNode) {
		std::cerr << "Could Not Open " << filename << std::endl;
	}

	// setup parents
}

//----------------------------------------------------------------------------------------
void A3::createShaderProgram()
{
  m_shader.generateProgramObject();
  m_shader.attachVertexShader( getAssetFilePath("VertexShader.vs").c_str() );
  m_shader.attachFragmentShader( getAssetFilePath("FragmentShader.fs").c_str() );
  m_shader.link();

  // trackball
  m_shader_arcCircle.generateProgramObject();
  m_shader_arcCircle.attachVertexShader( getAssetFilePath("arc_VertexShader.vs").c_str() );
  m_shader_arcCircle.attachFragmentShader( getAssetFilePath("arc_FragmentShader.fs").c_str() );
  m_shader_arcCircle.link();

  // picking
  /*
  m_shader_picking.generateProgramObject();
  m_shader_picking.attachVertexShader( getAssetFilePath("VertexShader.vs").c_str());
  m_shader_picking.attachFragmentShader(getAssetFilePath("PickingFrag.fs").c_str());
  m_shader_picking.link();

  CShader = &m_shader;
  */
}

//----------------------------------------------------------------------------------------
void A3::enableVertexShaderInputSlots()
{
	//-- Enable input slots for m_vao_meshData:
	{
		glBindVertexArray(m_vao_meshData);

		// Enable the vertex shader attribute location for "position" when rendering.
		m_positionAttribLocation = m_shader.getAttribLocation("position");
		glEnableVertexAttribArray(m_positionAttribLocation);

		// Enable the vertex shader attribute location for "normal" when rendering.
		m_normalAttribLocation = m_shader.getAttribLocation("normal");
		glEnableVertexAttribArray(m_normalAttribLocation);

		CHECK_GL_ERRORS;
	}

	//-- enable input slots for picking

	//-- Enable input slots for m_vao_arcCircle:
	{
		glBindVertexArray(m_vao_arcCircle);

		// Enable the vertex shader attribute location for "position" when rendering.
		m_arc_positionAttribLocation = m_shader_arcCircle.getAttribLocation("position");
		glEnableVertexAttribArray(m_arc_positionAttribLocation);

		CHECK_GL_ERRORS;
	}

	
	// Restore defaults
	glBindVertexArray(0);
}

//----------------------------------------------------------------------------------------
void A3::uploadVertexDataToVbos (
		const MeshConsolidator & meshConsolidator
) {
	// Generate VBO to store all vertex position data
	{
		glGenBuffers(1, &m_vbo_vertexPositions);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexPositions);

		glBufferData(GL_ARRAY_BUFFER, meshConsolidator.getNumVertexPositionBytes(),
				meshConsolidator.getVertexPositionDataPtr(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}

	// Generate VBO to store all vertex normal data
	{
		glGenBuffers(1, &m_vbo_vertexNormals);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexNormals);

		glBufferData(GL_ARRAY_BUFFER, meshConsolidator.getNumVertexNormalBytes(),
				meshConsolidator.getVertexNormalDataPtr(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}

	// Generate VBO to store the trackball circle.
	{
		glGenBuffers( 1, &m_vbo_arcCircle );
		glBindBuffer( GL_ARRAY_BUFFER, m_vbo_arcCircle );

		float *pts = new float[ 2 * CIRCLE_PTS ];
		for( size_t idx = 0; idx < CIRCLE_PTS; ++idx ) {
			float ang = 2.0 * M_PI * float(idx) / CIRCLE_PTS;
			pts[2*idx] = cos( ang );
			pts[2*idx+1] = sin( ang );
		}

		glBufferData(GL_ARRAY_BUFFER, 2*CIRCLE_PTS*sizeof(float), pts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERRORS;
	}
}

//----------------------------------------------------------------------------------------
void A3::mapVboDataToVertexShaderInputLocations()
{
	// Bind VAO in order to record the data mapping.
	glBindVertexArray(m_vao_meshData);

	// Tell GL how to map data from the vertex buffer "m_vbo_vertexPositions" into the
	// "position" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexPositions);
	glVertexAttribPointer(m_positionAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Tell GL how to map data from the vertex buffer "m_vbo_vertexNormals" into the
	// "normal" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexNormals);
	glVertexAttribPointer(m_normalAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	//-- Unbind target, and restore default values:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;

	/*
	glBindVertexArray(m_vao_Picking);
	glBindBuffer(GL_ARRAY_BUFFER,m_vbo_vertexPositions);
	glVertexAttribPointer(m_pickingPosLoc, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vertexNormals);
	glVertexAttribPointer(m_pickingNormalLoc, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
	*/
	
	// Bind VAO in order to record the data mapping.
	glBindVertexArray(m_vao_arcCircle);

	// Tell GL how to map data from the vertex buffer "m_vbo_arcCircle" into the
	// "position" vertex attribute location for any bound vertex shader program.
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_arcCircle);
	glVertexAttribPointer(m_arc_positionAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	//-- Unbind target, and restore default values:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
void A3::initPerspectiveMatrix()
{
	float aspect = ((float)m_windowWidth) / m_windowHeight;
	m_perpsective = glm::perspective(degreesToRadians(60.0f), aspect, 0.1f, 100.0f);
}


//----------------------------------------------------------------------------------------
void A3::initViewMatrix() {
	m_view = glm::lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, -1.0f),
			vec3(0.0f, 1.0f, 0.0f));
}

//----------------------------------------------------------------------------------------
void A3::initLightSources() {
	// World-space position
	m_light.position = vec3(3.0f, 3.0f, 3.0f);
	m_light.rgbIntensity = vec3(1.0f); // light
}

//----------------------------------------------------------------------------------------
void A3::uploadCommonSceneUniforms() {
	m_shader.enable();
	{
		//-- Set Perpsective matrix uniform for the scene:
		GLint location = m_shader.getUniformLocation("Perspective");
		glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(m_perpsective));
		CHECK_GL_ERRORS;


		//-- Set LightSource uniform for the scene:
		{
			location = m_shader.getUniformLocation("light.position");
			glUniform3fv(location, 1, value_ptr(m_light.position));
			location = m_shader.getUniformLocation("light.rgbIntensity");
			glUniform3fv(location, 1, value_ptr(m_light.rgbIntensity));
			CHECK_GL_ERRORS;
		}

		//-- Set background light ambient intensity
		{
			location = m_shader.getUniformLocation("ambientIntensity");
			vec3 ambientIntensity(0.05f);
			glUniform3fv(location, 1, value_ptr(ambientIntensity));
			CHECK_GL_ERRORS;
		}
	}
	m_shader.disable();
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, before guiLogic().
 */
void A3::appLogic()
{
	// Place per frame, application logic here ...

	uploadCommonSceneUniforms();
}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after appLogic(), but before the draw() method.
 */
void A3::guiLogic()
{
	if( !show_gui ) {
		return;
	}

	static bool firstRun(true);
	if (firstRun) {
		ImGui::SetNextWindowPos(ImVec2(50, 50));
		firstRun = false;
	}

	static bool showDebugWindow(true);
	ImGuiWindowFlags windowFlags(ImGuiWindowFlags_AlwaysAutoResize);
	float opacity(0.5f);

	ImGui::Begin("Properties", &showDebugWindow, ImVec2(100,100), opacity,
			windowFlags);


		// Add more gui elements here here ...


		// Create Button, and check if it was clicked:
		if( ImGui::Button( "Quit Application" ) ) {
			glfwSetWindowShouldClose(m_window, GL_TRUE);
		}

		ImGui::Text( "Framerate: %.1f FPS", ImGui::GetIO().Framerate );

	ImGui::End();
}

//----------------------------------------------------------------------------------------
// Update mesh specific shader uniforms:
static void updateShaderUniforms(
		const ShaderProgram & shader,
		const GeometryNode & node,
		const glm::mat4 & viewMatrix
				 ) {

  shader.enable();
  {
    //-- Set ModelView matrix:
    GLint location = shader.getUniformLocation("ModelView");
    mat4 modelView = viewMatrix * node.trans;
    glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(modelView));
    CHECK_GL_ERRORS;

    //-- Set NormMatrix:
    location = shader.getUniformLocation("NormalMatrix");
    mat3 normalMatrix = glm::transpose(glm::inverse(mat3(modelView)));
    glUniformMatrix3fv(location, 1, GL_FALSE, value_ptr(normalMatrix));
    CHECK_GL_ERRORS;


    //-- Set Material values:
    location = shader.getUniformLocation("material.kd");
    vec3 kd = node.material.kd;
    glUniform3fv(location, 1, value_ptr(kd));
    CHECK_GL_ERRORS;
    location = shader.getUniformLocation("material.ks");
    vec3 ks = node.material.ks;
    glUniform3fv(location, 1, value_ptr(ks));
    CHECK_GL_ERRORS;
    location = shader.getUniformLocation("material.shininess");
    glUniform1f(location, node.material.shininess);
    CHECK_GL_ERRORS;

  }
  shader.disable();

}

//----------------------------------------------------------------------------------------
/*
 * Called once per frame, after guiLogic().
 */
void A3::draw() {

  if(CZBF & (1 << 2)){
    glEnable( GL_DEPTH_TEST );
  }else{
    glEnable(GL_CULL_FACE);
    if(CZBF & (1<<1) && CZBF & 1){
      glCullFace(GL_FRONT_AND_BACK);
    }else if(CZBF & (1<<1)){
      glCullFace(GL_BACK);
    }else if(CZBF & 1){
      glCullFace(GL_FRONT);
    }else{
      glDisable(GL_CULL_FACE);
      glDisable(GL_DEPTH_TEST);
    }
  }
  renderSceneGraph(*m_rootNode);


  if(CZBF & (1<<3)){
    glDisable( GL_DEPTH_TEST );
    renderArcCircle();
  }
}

//----------------------------------------------------------------------------------------
void A3::renderSceneGraph(const SceneNode & root) {

  // Bind the VAO once here, and reuse for all GeometryNode rendering below.
  glBindVertexArray(m_vao_meshData);

  // This is emphatically *not* how you should be drawing the scene graph in
  // your final implementation.  This is a non-hierarchical demonstration
  // in which we assume that there is a list of GeometryNodes living directly
  // underneath the root node, and that we can draw them in a loop.  It's
  // just enough to demonstrate how to get geometry and materials out of
  // a GeometryNode and onto the screen.

  // You'll want to turn this into recursive code that walks over the tree.
  // You can do that by putting a method in SceneNode, overridden in its
  // subclasses, that renders the subtree rooted at every node.  Or you
  // could put a set of mutually recursive functions in this class, which
  // walk down the tree from nodes of different types.
  
  for (const SceneNode * node : root.children) {
    node->draw(m_shader,m_view,m_batchInfoMap);
    /*
    if (node->m_nodeType != NodeType::GeometryNode)
      continue;

    const GeometryNode * geometryNode = static_cast<const GeometryNode *>(node);

    updateShaderUniforms(m_shader, *geometryNode, m_view);


    // Get the BatchInfo corresponding to the GeometryNode's unique MeshId.
    BatchInfo batchInfo = m_batchInfoMap[geometryNode->meshId];

    //-- Now render the mesh:
    m_shader.enable();
    glDrawArrays(GL_TRIANGLES, batchInfo.startIndex, batchInfo.numIndices);
    m_shader.disable();*/
  }

  glBindVertexArray(0);
  CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
// Draw the trackball circle.
void A3::renderArcCircle() {
  glBindVertexArray(m_vao_arcCircle);

  m_shader_arcCircle.enable();
  GLint m_location = m_shader_arcCircle.getUniformLocation( "M" );
  float aspect = float(m_framebufferWidth)/float(m_framebufferHeight);
  glm::mat4 M;
  if( aspect > 1.0 ) {
    M = glm::scale( glm::mat4(), glm::vec3( 0.5/aspect, 0.5, 1.0 ) );
  } else {
    M = glm::scale( glm::mat4(), glm::vec3( 0.5, 0.5*aspect, 1.0 ) );
  }
  glUniformMatrix4fv( m_location, 1, GL_FALSE, value_ptr( M ) );
  glDrawArrays( GL_LINE_LOOP, 0, CIRCLE_PTS );
  m_shader_arcCircle.disable();

  glBindVertexArray(0);
  CHECK_GL_ERRORS;
}

//----------------------------------------------------------------------------------------
/*
 * Called once, after program is signaled to terminate.
 */
void A3::cleanup()
{

}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles cursor entering the window area events.
 */
bool A3::cursorEnterWindowEvent (
		int entered
) {
	bool eventHandled(false);

	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse cursor movement events.
 */
bool A3::mouseMoveEvent (
		double xPos,
		double yPos
) {
	bool eventHandled(false);
	
	if(!ImGui::IsMouseHoveringAnyWindow()){
	  
	  glm::vec3 Direction;
	  if(PJ){
	    if(MouseStatus & (1<<2)){
	      Direction = vec3(0.0f);
	      // dragging the puppet
	      Direction.x = (float)(2*(xPos - LastX)/m_windowWidth);
	      Direction.y = (float)(2*(LastY - yPos)/m_windowHeight);
	      m_rootNode->RTranslate(Direction);
	    }
	    if(MouseStatus & (1<<1)){
	      Direction = vec3(0.0f);
	      Direction.z = (float)(2*(yPos - LastY)/m_windowHeight);
	      m_rootNode->RTranslate(Direction);
	    }
	    if(MouseStatus & 1){
	      // track ball
	      vec3 center = vec3((float)(m_windowWidth / 2),(float)(m_windowHeight / 2),0.0f);
	      vec3 last = vec3((float)(LastX-center.x),(float)(LastY-center.y),0.0f);
	      vec3 current = vec3((float)(xPos-center.x),(float)(yPos-center.y),0.0f);
	      // calculate z value
	      SetZVal(last);
	      SetZVal(current);
	    
	      last = glm::normalize(last);
	      current = glm::normalize(current);
	      float Ang = glm::angle(current,last);
	    
	      vec3 normal = glm::cross(current,last);
	      glm::mat4 RM = glm::rotate(Ang,normal);
	    
	      m_rootNode->RRotate(RM);
	    } 
	  }else{
	    // rotate x
	    JAmount = vec2(0.0f);
	    if(MouseStatus & (1<<1)){
	      JAmount.x = (yPos-LastY)/10.0;
	    }
	    if(MouseStatus & 1){
	      JAmount.y = (xPos - LastX)/10.0;
	    }
	      // rotate
	    if(MouseStatus & (1<<1) || MouseStatus&1){
	      for(JointNode* i: ActiveJoints){
		glm::vec2 Rot = i->InRange(JAmount);
		i->Rotate(Rot);
	      }
	    }
	    
	  }
	}
	LastX = CX;
	LastY = CY;
	CX = xPos;
	CY = yPos;
	// Fill in with event handling code...
	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse button events.
 */
bool A3::mouseButtonInputEvent (
		int button,
		int actions,
		int mods
				)
{
  bool eventHandled(false);
  int Idx;
  if(!ImGui::IsMouseHoveringAnyWindow()){
    switch (actions){
    case GLFW_PRESS:
      if(button == GLFW_MOUSE_BUTTON_LEFT){
	MouseStatus |= 1<<2;
	if(PJ==false){
	  m_rootNode->SetPicking(true);
	  renderSceneGraph(*m_rootNode);
	  float color3[3];
	  glReadPixels((int)CX,m_windowHeight-(int)(CY),1,1,GL_RGB,GL_FLOAT,color3);
	  Idx = (color3[0] == 0.0)? round(color3[1]*50.0): round(color3[0] *50.0);
	  
	  if(Idx >= 0 && Idx <= NodeCount-1 && AllNodes[Idx]->Parent->m_nodeType == NodeType::JointNode){
	    AllNodes[Idx]->isSelected ^= 1;
	    AllNodes[Idx]->Parent->isSelected ^= 1;
	  }
	  m_rootNode->SetPicking(false);
	}
      }
      if(button == GLFW_MOUSE_BUTTON_MIDDLE || button == GLFW_MOUSE_BUTTON_RIGHT){
	if(button == GLFW_MOUSE_BUTTON_MIDDLE) MouseStatus |= 1<<1;
	if(button == GLFW_MOUSE_BUTTON_RIGHT){
	  MouseStatus |= 1;
	}
	// when pressed, record stack for undo
	// record stack
	bool Added = false;
	ActiveJoints = vector<JointNode*>(0);
	Op = vector<vec2>(0);
	for(int i = 0; i<NodeCount;i++){
	  if(AllNodes[i]->m_nodeType == NodeType::JointNode && AllNodes[i]->isSelected)
	    {
	      JointNode* ActiveN = static_cast<JointNode*>(AllNodes[i]);
	      ActiveJoints.emplace_back(ActiveN);
	      Op.emplace_back(glm::vec2((float)(ActiveN->XDeg),(float)(ActiveN->YDeg)));
	      Added = true;
	    }
	}
        
	if(Added){
	  cmdStack.Push(ActiveJoints,Op);
	}
      }
      
      
	
      break;
    case GLFW_RELEASE:
      if(button == GLFW_MOUSE_BUTTON_LEFT){
	MouseStatus &= (255 ^ (1<<2));
      }
      if(button == GLFW_MOUSE_BUTTON_RIGHT){
	MouseStatus &= (255 ^ 1);
      }
      if(button == GLFW_MOUSE_BUTTON_MIDDLE){
	MouseStatus &= (255 ^ (1<<1));
	ActiveJoints = vector<JointNode*>(0);
	Op = vector<vec2>(0);
	if(Neck->isSelected && (MouseStatus & 1)){
	  ActiveJoints.emplace_back(Neck);
	  Op.emplace_back(vec2((float)(Neck->XDeg),(float)(Neck->YDeg)));
	}
      }
      break;
    }
  }
  
    // Fill in with event handling code...
    eventHandled = true;
    return eventHandled;
  }

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles mouse scroll wheel events.
 */
bool A3::mouseScrollEvent (
		double xOffSet,
		double yOffSet
) {
	bool eventHandled(false);
	// Fill in with event handling code...

	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles window resize events.
 */
bool A3::windowResizeEvent (
		int width,
		int height
) {
	bool eventHandled(false);
	initPerspectiveMatrix();
	m_windowWidth = width;
	m_windowHeight = height;
	return eventHandled;
}

//----------------------------------------------------------------------------------------
/*
 * Event handler.  Handles key input events.
 */
bool A3::keyInputEvent (
			int key,
			int action,
			int mods
			) {
  bool eventHandled(false);
  GLint UniformLocation;
  bool Undoable;
  if( action == GLFW_PRESS ) {
    switch (key){
    case GLFW_KEY_Q:
      glfwSetWindowShouldClose(m_window, GL_TRUE);
      break;
    case GLFW_KEY_M:
      show_gui = !show_gui;
      eventHandled = true;
      break;
    case GLFW_KEY_C:
      CZBF ^= (1<<3);
      eventHandled = true;
      break;
    case GLFW_KEY_Z:
      CZBF ^= (1<<2);
      if(CZBF & (1<<2)){
	CZBF &= (255 - 3);
      }
      eventHandled = true;
      break;
    case GLFW_KEY_B:
      CZBF ^= (1<<1);
      if(CZBF & 2){
	CZBF &= (255 ^ (1<<2));
      }
      eventHandled = true;
      break;
    case GLFW_KEY_F:
      CZBF ^= 1;
      if(CZBF & 1){
	CZBF &= (255 ^ (1<<2));
      }
      eventHandled = true;
      break;
    case GLFW_KEY_P:
      PJ = true;
      //m_rootNode->SetPicking(false);
      eventHandled = true;
      m_rootNode->SetJMode(false);
      /*
      m_shader.enable();
      UniformLocation= m_shader.getUniformLocation("Picking");
      glUniform1i(UniformLocation,1);
      m_shader.disable();
      */
      break;
    case GLFW_KEY_J:
      PJ = false;
      m_rootNode->SetJMode(true);
      //m_rootNode->SetPicking(true);
      eventHandled = true;
      /*
      m_shader.enable();
      UniformLocation = m_shader.getUniformLocation("Picking");
      glUniform1i(UniformLocation,0);
      m_shader.disable();
      */
      break;
    case GLFW_KEY_I:
      // try to reset
      m_rootNode->RTranslate(inverse(m_rootNode->TMatrix));
      m_rootNode->TMatrix = mat4();
      eventHandled = true;
      break;
    case GLFW_KEY_O:
      // try to reset orientation
      m_rootNode->RRotate(inverse(m_rootNode->RMatrix));
      m_rootNode->TMatrix = mat4();
      eventHandled = true;
      break;
    case GLFW_KEY_U:
      ActiveJoints = vector<JointNode*>(0);
      Op = vector<vec2>(0);
     
      if(cmdStack.Back == cmdStack.Cur){

	for(int i = 0;i<NodeCount;i++){
	  // first record for all redo
	  if(AllNodes[i]->m_nodeType == NodeType::JointNode &&AllNodes[i]->isSelected){
	    JointNode *a = static_cast<JointNode*>(AllNodes[i]);
	    ActiveJoints.emplace_back(a);
	    Op.emplace_back(vec2((float)(a->XDeg),(float)(a->YDeg)));
	  }
	}       
	cmdStack.Push(ActiveJoints,Op);
      }

      // then undo
      YELLOW();
      Undoable = cmdStack.Undo();

      DEFAULT();
      if(Undoable){
	ActiveJoints = cmdStack.Stack[cmdStack.Cur-1];
	Op = cmdStack.Ops[cmdStack.Cur-1];
	int j = 0;
	vec2 Rot;
	for(JointNode* i:ActiveJoints){
	  Rot = Op[j] - vec2((float)(i->XDeg),(float)(i->YDeg));
	  i->Rotate(Rot);
	  j+=1;
	}
      }
      break;
    case GLFW_KEY_R:
      Undoable = cmdStack.Redo();
       if(Undoable){
	 ActiveJoints = cmdStack.Stack[cmdStack.Cur];
	 Op = cmdStack.Ops[cmdStack.Cur];
	 int j = 0;
	 vec2 Rot;
	 for(JointNode* i:ActiveJoints){
	   Rot = Op[j] - vec2((float)(i->XDeg),(float)(i->YDeg));
	   i->Rotate(Rot);
	   j+=1;
	 }
	 cmdStack.Pop();
       }
      break;
    case GLFW_KEY_A:
      // reset all
      PJ = true;
      m_rootNode->FullReset();
      cmdStack.FullReset();
      m_rootNode->ApplyRootTranslate(m_rootNode->trans);
      break;
    }
  }
  // Fill in with event handling code...

  return eventHandled;
}


void A3::SetZVal(glm::vec3& V){
  double dist2 = (double)(GetRadius()/2);
  dist2 = dist2 * dist2;
  double vdist2 = V.x*V.x + V.y*V.y;
  if(vdist2 > dist2){
    V.z = 0.0f;
  }else{
    V.z = glm::sqrt((float)(dist2-vdist2));
  } 
}

int A3::GetRadius(){
  return ((m_windowWidth/2 + m_windowHeight/2)-(m_windowWidth/2-m_windowHeight/2))/2;
}


void CMD::Push(vector<JointNode*>&J,vector<vec2>&O){
  if(Back == Cur){
    if(Stack.size() == Back){
      Stack.emplace_back(J);
      Ops.emplace_back(O);
    }else{
      Stack[Back] = J;
      Ops[Back] = O;
    }
    Back += 1;
  }else{
    Cur-=1;
    Stack[Cur] = J;
    Ops[Cur] = O;
    Back=Cur+1;
  }
  Cur += 1;
}

bool CMD::Undo(){
  if(Cur>1){
    Cur -= 1;
    return true;
  }
  return false;
}

bool CMD::Redo(){
  if(Cur<Back){
    return true;
  }
  return false;
}

void CMD::Pop(){
  Cur+=1;
  if(Back==Cur){
    Back = Cur = Cur-1;
    Stack.pop_back();
    Ops.pop_back();
  }
}

void CMD::FullReset(){
  Cur=Back=0;
  Stack=vector<vector<JointNode*>>(0);
  Ops=vector<vector<vec2>>(0);
}
