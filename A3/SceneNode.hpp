// Winter 2019

#pragma once

#include "Material.hpp"
#include "cs488-framework/ShaderProgram.hpp"
#include "cs488-framework/MeshConsolidator.hpp"

#include <glm/glm.hpp>

#include <list>
#include <string>
#include <iostream>

enum class NodeType {
  SceneNode,
    GeometryNode,
    JointNode
    };

class SceneNode {
public:
  SceneNode(const std::string & name);

  SceneNode(const SceneNode & other);

  virtual ~SceneNode();
    
  int totalSceneNodes() const;
    
  const glm::mat4& get_transform() const;
  const glm::mat4& get_inverse() const;
    
  void set_transform(const glm::mat4& m);
    
  void add_child(SceneNode* child);
    
  void remove_child(SceneNode* child);

  //-- Transformations:
  void rotate(char axis, float angle);
  void scale(const glm::vec3& amount);
  virtual void translate(const glm::vec3& amount);

  virtual void draw(const ShaderProgram& S,const glm::mat4& V,BatchInfoMap& BM) const;

  void ApplyRootTranslate(glm::mat4& T);
  void SetNodeArray(SceneNode** Arr);
  //virtual void setParentAndInitTrans(SceneNode* P,ShaderProgram* Pick);
  virtual int setParentAndInitTrans(SceneNode* P);
  virtual void InitJoint();
  virtual void RotateAllChild(const glm::mat4& M);
  virtual void FullReset();
  void RTranslate(const glm::vec3& amount);
  void RTranslate(const glm::mat4& TMat);
  void RRotate(const glm::mat4& RMat);
  
  void SetPicking(bool Picking);
  void SetJMode(bool J);

  friend std::ostream & operator << (std::ostream & os, const SceneNode & node);

  bool isSelected;
  bool Pick;
  bool JMode;
    
  // Transformations
  glm::mat4 trans;
  glm::mat4 invtrans;

  glm::mat4 initTrans;
  glm::mat4 initInv;

  glm::mat4 TMatrix;
  glm::mat4 RMatrix;
    
  std::list<SceneNode*> children;

  NodeType m_nodeType;
  std::string m_name;
  unsigned int m_nodeId;

  SceneNode* Parent;
  ShaderProgram* PShader;
  
private:
  // The number of SceneNode instances.
  static unsigned int nodeInstanceCount;

};
