// Winter 2019

#pragma once


#include "SceneNode.hpp"

#include <glm/glm.hpp>

class JointNode : public SceneNode {
  void SetBack();
public:
  JointNode(const std::string & name);
  virtual ~JointNode();

  void set_joint_x(double min, double init, double max);
  void set_joint_y(double min, double init, double max);
  void RotateX(double deg);
  void RotateY(double deg);
  void Rotate();
  void Rotate(glm::vec2& R);
  
  void InitJoint() override;
  void FullReset() override;
  void InitRotate();
  glm::vec2 InRange(const glm::vec2& change) const;
  struct JointRange {
    double min, init, max;
  };


  JointRange m_joint_x, m_joint_y;

  double XDeg;
  double YDeg;
  bool DoRotation;
};
