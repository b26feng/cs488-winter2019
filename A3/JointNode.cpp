// Winter 2019

#include "JointNode.hpp"
#include "cs488-framework/MathUtils.hpp"



#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <memory>

static const float ZERO = 0.0002f;
//---------------------------------------------------------------------------------------
JointNode::JointNode(const std::string& name)
  : SceneNode(name),
    DoRotation(false)
{
	m_nodeType = NodeType::JointNode;
}

//---------------------------------------------------------------------------------------
JointNode::~JointNode() {

}
 //---------------------------------------------------------------------------------------
void JointNode::set_joint_x(double min, double init, double max) {
	m_joint_x.min = min;
	m_joint_x.init = init;
	m_joint_x.max = max;
	XDeg = init;
	DoRotation |= (XDeg != 0);
        
}

//---------------------------------------------------------------------------------------
void JointNode::set_joint_y(double min, double init, double max) {
	m_joint_y.min = min;
	m_joint_y.init = init;
	m_joint_y.max = max;
	YDeg = init;
	DoRotation |= (YDeg != 0);
	//std::cout<<m_name<<" set joint y:"<<glm::to_string(trans[3])<<std::endl;
}

void JointNode::InitJoint(){
  if(DoRotation){
    Rotate();
  }
  for(SceneNode* i: children){
    i->InitJoint();
  }
  /*
  if(Parent != nullptr && Parent->Parent != nullptr){
    glm::mat4 Rot;
    JointNode* PJoint = static_cast<JointNode*>(Parent->Parent);
    trans = PJoint->invtrans * trans;
    Rot = glm::rotate(degreesToRadians((float)(PJoint->XDeg)),glm::vec3(0,0,1));
    Rot *= glm::rotate(degreesToRadians((float)(PJoint->YDeg)),glm::vec3(0,1,0));
    trans = Rot * trans;
    trans = PJoint->trans * trans;
  }
  invtrans = glm::inverse(trans);
  //std::cout<<m_name<<" has:";
  for(SceneNode* i: children){
    //std::cout<<i->m_name<<" ";
    i->JointRotation();
  }
  //std::cout<<std::endl;
  */
}

void JointNode::FullReset(){
  SceneNode::FullReset();
  XDeg = m_joint_x.init;
  YDeg = m_joint_y.init;
  DoRotation = true;
  InitJoint();
}
void JointNode::RotateX(double deg){
  
  if(XDeg + deg < m_joint_x.max+ZERO && XDeg + deg > m_joint_x.min-ZERO && deg!=0.0f){
    XDeg += deg;
    DoRotation = true;
  }
}

void JointNode::RotateY(double deg){
  if(YDeg + deg < m_joint_y.max+ZERO && YDeg + deg > m_joint_y.min-ZERO && deg!=0.0f){
    YDeg += deg;
    DoRotation = true;
  }
}


void JointNode::Rotate(){
  if(DoRotation){
    glm::mat4 T = glm::mat4();
    T = invtrans * T;
    glm::mat4 Rot = glm::rotate(degreesToRadians((float)(YDeg)),glm::vec3(0,1,0));
    Rot *= glm::rotate(degreesToRadians((float)(XDeg)),glm::vec3(1,0,0));
    T = Rot * T;
    T = trans * T;
    RotateAllChild(T);
    DoRotation = false;
  }
}

void JointNode::SetBack(){
   glm::mat4 T = glm::mat4();
   T = invtrans * T;
   glm::mat4 Rot = glm::rotate(degreesToRadians((float)(YDeg)),glm::vec3(0,1,0));
   Rot *= glm::rotate(degreesToRadians((float)(XDeg)),glm::vec3(1,0,0));
   Rot = glm::inverse(Rot);
   T = Rot * T;
   T = trans * T;
   RotateAllChild(T);
}

void JointNode::Rotate(glm::vec2& R){
  if(R.x != 0.0f || R.y!=0.0f){
    SetBack();
  }
  RotateX((double)R.x);
  RotateY((double)R.y);
  Rotate();
}

glm::vec2 JointNode::InRange(const glm::vec2& change) const{
  glm::vec2 Result = change;
  if(Result.x + XDeg > m_joint_x.max || Result.x+XDeg < m_joint_x.min){
    Result.x = 0.0f;
  }
  if(m_name != "neck" || Result.y + YDeg >= m_joint_y.max || Result.y + YDeg <= m_joint_y.min ){
    Result.y = 0.0f;
  }
  return Result;
}
